a1 = 300;
d1 = 800;
a2 = 1280;
a3 = 1350;
a4 = 260;
d5 = -246;

theta1 = 0:15:110;
theta2 = 110:-15:0;
theta3 = 0:15:110;
theta_1 = deg2rad(theta1);
theta_2 = deg2rad(theta2);
theta_3 = deg2rad(theta3);

[THETA1,THETA2,THETA3] = meshgrid(theta_1,theta_2,theta_3);

x = (a3.*(cos(THETA1)).*(cos(THETA2+THETA3))) + (a2.*(cos(THETA1)).*(cos(THETA2))) + (a1.*(cos(THETA1))) + (a4.*(cos(THETA1)));
y = (sind(THETA1).*cosd(THETA2+THETA3).*a3)  + (sind(THETA1).*cosd(THETA2).*a2) + (sind(THETA1).*a1) + (sind(THETA1).*a4);
z = (sind(THETA2+THETA3).*a3) + (sind(THETA2).*a2) + d1 + d5;

data2 = [x(:) y(:) z(:) THETA1(:)];

disp( data2 );
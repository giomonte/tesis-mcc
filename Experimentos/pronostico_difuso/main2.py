# Experimentos de t+1
import csv
import matplotlib.pyplot as plt
import numpy as np
import math
import helperFuzzy2

directorio = 'datosExperimentosMuestras/'

numeroMuestra =  3000

t = 1

datosArchivos = np.array([

                        ['wind_aristeomercado_10m_muestra']
                    ])

# Posicion del archivo a simular
x = 0

# n + 1 Ejemplo si el numero es 3 la ventana seria de 2
ventana = 5

nb_archivo = datosArchivos[x][0]

rutaArchivo = directorio + nb_archivo+'.csv'

archivo = helperFuzzy2.obtenerArchivo( rutaArchivo, numeroMuestra)

arrayVentana = []
arrayMSE = []

for l in xrange(4, ventana):

	ventana = l
	# Se obtienen las ventanas de los datos de entrada y su targen, del entranamiento y de la validacion
	inpEntrenamiento, tarEntrenamiento, inpValidacion, tarValidacion = helperFuzzy2.obtenerVentanaDatos(archivo, numeroMuestra, ventana + 1 )

	arrayOutputs = []
	arrayTargets = []

	for i in xrange(0, len(inpEntrenamiento)):

		# Generamos el array de los valores que tendra cada entrada
		arrayValueInputs = []
		
		# Se guardan las entradas de los datos, el ultimo valor viene siendo el target ( la salida real ) para ser comparada con el output resultante
		for j in xrange(0, len(inpEntrenamiento[i]) - 1 ):
			arrayValueInputs.append(inpEntrenamiento[i][j])

		target = inpEntrenamiento[i][len(inpEntrenamiento[i]) - 1]

		# obtenemos el rango de valores del universo del discuros para el numero de entradas de acuerdo al tamanio de ventana
		arrayInputs, x_output = helperFuzzy2.getInputs( ventana )

		# Generate fuzzy membership functions - SE DEFINEN LOS CONJUNTOS DIFUSOS
		conjuntosDifusos = 11

		# matrixFuncMembershipsInputs, matrixFuncMembershipsOutput = helperFuzzy2.generateFuzzyMembershipFunctions(arrayInputs, x_output, conjuntosDifusos)
		matrixFuncMembershipsInputs, matrixFuncMembershipsOutput = helperFuzzy2.generateFuzzyMembershipFunctions2(arrayInputs, x_output, conjuntosDifusos)

		# We need the activation of our fuzzy membership functions at these values.
		matrixLevelsInputs = helperFuzzy2.getDegreeMemberships(arrayInputs, matrixFuncMembershipsInputs, arrayValueInputs)

		# Valiodacion de las Reglas Difusas
		numRules = 11
		arrayLevelsActivation = helperFuzzy2.validationRules(matrixLevelsInputs, matrixFuncMembershipsOutput, numRules)

		output = np.zeros_like(x_output)
		# helperFuzzy.graphFuzzySet1(x_output, output, arrayLevelsActivation, matrixFuncMembershipsOutput)

		aggregated = helperFuzzy2.getAggregate(arrayLevelsActivation)
		
		# Se obtiene el valor de salida esperado ESTE ES EL OUTPUT BUENO
		outputFinal = helperFuzzy2.defuzzy(x_output, aggregated)

		arrayTargets.append(target)
		arrayOutputs.append(outputFinal)

	targets = np.array(arrayTargets)
	outputs = np.array(arrayOutputs)

	mse = helperFuzzy2.calculateMSE(targets, outputs)

	print "Tamanio de ventana: ", ventana
	print "MSE: ", mse
	print ''

	arrayVentana.append(ventana)
	arrayMSE.append(mse)

	tar = targets.reshape(len(targets), 1)

	out = outputs.reshape(len(outputs), 1)

	archivoCSV = "pronostico/pronostico_"+str(nb_archivo)+"_ventana_"+str(ventana)+".csv"

	helperFuzzy2.exportToCSV(archivoCSV, tar, out)

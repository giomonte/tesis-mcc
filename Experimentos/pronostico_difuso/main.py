# Pronostico con logica difusa

import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt
import helper
import helperFuzzy

directorio = 'ventanaDatos/'

numeroMuestra =  3000

t = 1

datosArchivos = np.array([

                        ['ventanaTraining_wind_aristeomercado_10m_muestra',2],
                        ['ventanaValidation_wind_aristeomercado_10m_muestra',2]
                    ])

ventana = int ( datosArchivos[0][1] )
nb_archivo = datosArchivos[0][0]

# Sin reconstruccion
archivo = helper.obtenerArchivo( directorio + nb_archivo+'.csv' )

# print archivo[0]

arrayOutputs = []
arrayTargets = []

for i in xrange(0, 2 ):

	input1 = archivo[i][0]
	input2 = archivo[i][1]
	target = archivo[i][2]

	# obtenemos el rango de valores del universo del discuros para el numero de entradas de acuerdo al tamanio de ventana
	arrayInputs, x_output = helperFuzzy.getInputs( archivo, ventana )

	x_input1 = arrayInputs[0]
	x_input2 = arrayInputs[1]

	# Generate fuzzy membership functions
	matrixFuncMembershipsInputs, matrixFuncMembershipsOutput = helperFuzzy.generateFuzzyMembershipFunctions(arrayInputs, x_output)

	# We need the activation of our fuzzy membership functions at these values.
	valueInput1 = input1
	valueInput2 = input2

	arrayValueInputs = [valueInput1, valueInput2]

	matrixLevelsInputs = helperFuzzy.getDegreeMemberships(arrayInputs, matrixFuncMembershipsInputs, arrayValueInputs)

	arrayLevelsActivation = helperFuzzy.validationRules(matrixLevelsInputs, matrixFuncMembershipsOutput)

	output = np.zeros_like(x_output)
	helperFuzzy.graphFuzzySet1(x_output, output, arrayLevelsActivation, matrixFuncMembershipsOutput)

	aggregated = helperFuzzy.getAggregate(arrayLevelsActivation)
	
	# Se obtiene el valor de salida esperado ESTE ES EL OUTPUT BUENO
	outputFinal = helperFuzzy.defuzzy(x_output, aggregated)

	arrayTargets.append(target)
	arrayOutputs.append(outputFinal)

	# # Grado de membresia del output
	output_activation = fuzz.interp_membership(x_output, aggregated, outputFinal) # for plot
	# print 'output_activation: ', output_activation
	helperFuzzy.graphFuzzySetwithCentroid(x_output, output, outputFinal, output_activation, aggregated, matrixFuncMembershipsOutput)

# targets = np.array(arrayTargets)
# outputs = np.array(arrayOutputs)

# mse = helperFuzzy.calculateMSE(targets, outputs)

# print "Tamanio de ventana: ", ventana
# print "MSE: ", mse

# tar = targets.reshape(len(targets), 1)

# out = outputs.reshape(len(outputs), 1)

# archivoCSV = "pronostico/pronostico_"+str(nb_archivo)+"_ventana_"+str(ventana)+".csv"

# helperFuzzy.exportToCSV(archivoCSV, tar, out)
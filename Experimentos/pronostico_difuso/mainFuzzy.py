import fuzzy
import helperFuzzy3
import csv
import numpy as np
import matplotlib.pyplot as plt

directorio = 'datosExperimentosMuestras/'

numeroMuestra =  3000

datosArchivos = np.array([
                        # Archivo, Ventana, CD
                        ['wind_aristeomercado_10m_muestra', 5, 5],
                        ['wind_aristeomercado_10m_muestra', 5, 10],
                        ['wind_aristeomercado_10m_muestra', 5, 15],
                        ['wind_aristeomercado_10m_muestra', 5, 20],
                        ['wind_aristeomercado_10m_muestra', 10, 5],
                        ['wind_aristeomercado_10m_muestra', 10, 10],
                        ['wind_aristeomercado_10m_muestra', 10, 15],
                        ['wind_aristeomercado_10m_muestra', 10, 20],
                        ['wind_aristeomercado_10m_muestra', 15, 5],
                        ['wind_aristeomercado_10m_muestra', 15, 10],
                        ['wind_aristeomercado_10m_muestra', 15, 15],
                        ['wind_aristeomercado_10m_muestra', 15, 20],
                        ['wind_aristeomercado_10m_muestra', 20, 5],
                        ['wind_aristeomercado_10m_muestra', 20, 10],
                        ['wind_aristeomercado_10m_muestra', 20, 15],
                        ['wind_aristeomercado_10m_muestra', 20, 20]
                    ])

# Posicion del archivo a simular
x = 0

for x in xrange(0, len(datosArchivos) ):

    nb_archivo = datosArchivos[x][0]

    rutaArchivo = directorio + nb_archivo+'.csv'

    archivo = helperFuzzy3.obtenerArchivo( rutaArchivo, numeroMuestra)

    numeroValidacion = int(( len(archivo) * 0.20 ))

    numeroTraining = numeroMuestra - numeroValidacion

    ventana = int(datosArchivos[x][1] )
    conjuntosDifusos = int( datosArchivos[x][2] )
    k = 1

    error = fuzzy.forecastingFuzzy(archivo, numeroMuestra, ventana, conjuntosDifusos, k)
    print "-----------------------------------------------------------"

# tar = targets.reshape(len(targets), 1)

# out = outputs.reshape(len(outputs), 1)

# archivoCSV = "pronosticoDifuso/pronostico_"+str(nb_archivo)+"_ventana_"+str(V)+".csv"

# helperFuzzy3.exportToCSV(archivoCSV, tar, out)
__author__ = 'igor'

import fuzzy
import helperFuzzy3
import csv
import numpy as np

from random import random
from random import uniform

class Solution(object):
    """
    A solution for the given problem, it is composed of a binary value and its fitness value
    """
    def __init__(self, value):
        self.value = value
        self.fitness = 0

    def calculate_fitness(self, fitness_function):
        self.fitness = fitness_function(self.value)

def generate_candidate(vector):
    """
    Generates a new candidate solution based on the probability vector
    """
    arregloBinario = []

    for p in vector:
        arregloBinario.append( 1 if random() < p else 0 )

    return Solution(arregloBinario)

def generate_vector(size):
    """
    Initializes a probability vector with given size
    """
    return [0.5] * size


def compete(a, b):
    """
    Returns a tuple with the winner solution
    """
    if a.fitness > b.fitness:
        return a, b
    else:
        return b, a


def update_vector(vector, winner, loser, population_size):
    for i in xrange(len(vector)):
        if winner[i] != loser[i]:
            if winner[i] == '1':
                vector[i] += 1.0 / float(population_size)
            else:
                vector[i] -= 1.0 / float(population_size)

def run(generations, size, population_size, fitness_function):
    # this is the probability for each solution bit be 1
    vector = generate_vector(size)
    best = None

    # I stop by the number of generations but you can define any stop param
    for i in xrange(generations):
        # generate two candidate solutions, it is like the selection on a conventional GA
        s1 = generate_candidate(vector)
        s2 = generate_candidate(vector)

        # calculate fitness for each
        s1.calculate_fitness(fitness_function)
        s2.calculate_fitness(fitness_function)

        # let them compete, so we can know who is the best of the pair
        winner, loser = compete(s1, s2)


        if best:
            if winner.fitness > best.fitness:
                best = winner
        else:
            best = winner

        #print "Best:", best
        # updates the probability vector based on the success of each bit
        update_vector(vector, winner.value, loser.value, population_size)

        print "generation: %d best value: %s best fitness: %f" % (i + 1, best.value, float(best.fitness))

def convertirABinario(x):
    if x>=0.5:
        return 1
    return 0
def convertirBinarioADecimal(arreglo):
    res=0
    aux=0
    for i in range(len(arreglo)-1,-1,-1):
        if arreglo[i]>0:
            res=res+2**aux
        aux+=1
    return res

def TipoFuncionEntrenamiento(num):

    rango = int( num * 12 + 1 )

    print "Numero de entrenamiento: ", num
    print "Rango:", rango

    return num

def functionEvaluationI(x):
    global mejorConf
    global mejorE
    # global mejorRules
    global mejorC
    global numeroTraining
    global numeroMuestra

    #Tamanio de Ventana
    V = 0
    #Numero conjuntos difusos
    CF = 0

    #Ne = map(convertirABinario,x[:4])
    V = x[:4]
    V = convertirBinarioADecimal(V) + 1 #para evitar el valor 0 entradas

    #Nn = map(convertirABinario, x[4:8])
    CF = x[4:8]
    CF = convertirBinarioADecimal(CF) + 1 #para evitar el valor 0 entradas

    # Numero de K
    K = x[8:12]
    K = convertirBinarioADecimal(K) + 1 #para evitar el valor 0 entradas

    if( V > 2 and CF > 1):
        error = fuzzy.forecastingFuzzy(archivo, numeroMuestra, V, CF, K)
        errorMin = error
    else:
        errorMin = 10000

    if errorMin < mejorE:
            mejorConf = "Ventana:", V, " - Conjutos Difusos:", CF, " - K:", K, " - MSE:", errorMin
            mejorE = errorMin
            mejorC = x
            # mejorRules = numRules
            print mejorConf

    return (errorMin * -1)

if __name__ == '__main__':
    global mejorConf
    global mejorE
    global mejorC
    # global mejorRules
    global archivo
    global numeroTraining
    global numeroMuestra

    directorio = 'datosExperimentosMuestras/'

    numeroMuestra =  3000

    datosArchivos = np.array([

                            ['wind_aristeomercado_10m_muestra']
                        ])

    # Posicion del archivo a simular
    x = 0
    nb_archivo = datosArchivos[x][0]

    rutaArchivo = directorio + nb_archivo+'.csv'

    archivo = helperFuzzy3.obtenerArchivo( rutaArchivo, numeroMuestra)

    numeroValidacion = int(( len(archivo) * 0.20 ))

    numeroTraining = numeroMuestra - numeroValidacion

    mejorE = 100000

    # def run(generations, size, population_size, fitness_function):
    f = lambda x: functionEvaluationI(x)

    run(300, 12, 500, f)

    print "Archivo: ", nb_archivo
    print "Topologia:"
    print "Mejor resultado: ", mejorE
    print "Mejor individiuo: ", mejorC
    # print "Numero de Reglas: ", mejorRules
    print mejorConf
# Pronostico con logica difusa

import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt
import helper
import helperFuzzy

directorio = 'ventanaDatos/'

numeroMuestra =  3000

t = 1

datosArchivos = np.array([

                        ['ventanaTraining_wind_aristeomercado_10m_muestra_ventana_3',3]
                    ])

ventana = int ( datosArchivos[0][1] )
nb_archivo = datosArchivos[0][0]

# Sin reconstruccion
archivo = helper.obtenerArchivo( directorio + nb_archivo+'.csv' )

print archivo[0]

input1 = archivo[0][0]
input2 = archivo[0][1]
input3 = archivo[0][2]

x_input1 = np.arange(0.0, 1.01, 0.01)
x_input2 = np.arange(0.0, 1.01, 0.01)
x_input3 = np.arange(0.0, 1.01, 0.01)
x_output = np.arange(0.0, 1.01, 0.01)

# Generate fuzzy membership functions
input1_1 = fuzz.trimf(x_input1, [0.0, 0.0, 0.1])
input1_2 = fuzz.trimf(x_input1, [0.0, 0.1, 0.2])
input1_3 = fuzz.trimf(x_input1, [0.1, 0.2, 0.3])
input1_4 = fuzz.trimf(x_input1, [0.2, 0.3, 0.4])
input1_5 = fuzz.trimf(x_input1, [0.3, 0.4, 0.5])
input1_6 = fuzz.trimf(x_input1, [0.4, 0.5, 0.6])
input1_7 = fuzz.trimf(x_input1, [0.5, 0.6, 0.7])
input1_8 = fuzz.trimf(x_input1, [0.6, 0.7, 0.8])
input1_9 = fuzz.trimf(x_input1, [0.7, 0.8, 0.9])
input1_10 = fuzz.trimf(x_input1, [0.8, 0.9, 1.0])
input1_11 = fuzz.trimf(x_input1, [0.9, 1.0, 1.0])

input2_1 = fuzz.trimf(x_input2, [0.0, 0.0, 0.1])
input2_2 = fuzz.trimf(x_input2, [0.0, 0.1, 0.2])
input2_3 = fuzz.trimf(x_input2, [0.1, 0.2, 0.3])
input2_4 = fuzz.trimf(x_input2, [0.2, 0.3, 0.4])
input2_5 = fuzz.trimf(x_input2, [0.3, 0.4, 0.5])
input2_6 = fuzz.trimf(x_input2, [0.4, 0.5, 0.6])
input2_7 = fuzz.trimf(x_input2, [0.5, 0.6, 0.7])
input2_8 = fuzz.trimf(x_input2, [0.6, 0.7, 0.8])
input2_9 = fuzz.trimf(x_input2, [0.7, 0.8, 0.9])
input2_10 = fuzz.trimf(x_input2, [0.8, 0.9, 1.0])
input2_11 = fuzz.trimf(x_input2, [0.9, 1.0, 1.0])

input3_1 = fuzz.trimf(x_input3, [0.0, 0.0, 0.1])
input3_2 = fuzz.trimf(x_input3, [0.0, 0.1, 0.2])
input3_3 = fuzz.trimf(x_input3, [0.1, 0.2, 0.3])
input3_4 = fuzz.trimf(x_input3, [0.2, 0.3, 0.4])
input3_5 = fuzz.trimf(x_input3, [0.3, 0.4, 0.5])
input3_6 = fuzz.trimf(x_input3, [0.4, 0.5, 0.6])
input3_7 = fuzz.trimf(x_input3, [0.5, 0.6, 0.7])
input3_8 = fuzz.trimf(x_input3, [0.6, 0.7, 0.8])
input3_9 = fuzz.trimf(x_input3, [0.7, 0.8, 0.9])
input3_10 = fuzz.trimf(x_input3, [0.8, 0.9, 1.0])
input3_11 = fuzz.trimf(x_input3, [0.9, 1.0, 1.0])

output_1 = fuzz.trimf(x_output, [0.0, 0.0, 0.1])
output_2 = fuzz.trimf(x_output, [0.0, 0.1, 0.2])
output_3 = fuzz.trimf(x_output, [0.1, 0.2, 0.3])
output_4 = fuzz.trimf(x_output, [0.2, 0.3, 0.4])
output_5 = fuzz.trimf(x_output, [0.3, 0.4, 0.5])
output_6 = fuzz.trimf(x_output, [0.4, 0.5, 0.6])
output_7 = fuzz.trimf(x_output, [0.5, 0.6, 0.7])
output_8 = fuzz.trimf(x_output, [0.6, 0.7, 0.8])
output_9 = fuzz.trimf(x_output, [0.7, 0.8, 0.9])
output_10 = fuzz.trimf(x_output, [0.8, 0.9, 1.0])
output_11 = fuzz.trimf(x_output, [0.9, 1.0, 1.0])

# Visualize these universes and membership functions
fig, (ax0, ax1, ax2, ax3) = plt.subplots(nrows=4, figsize=(15, 9))

ax0.plot(x_input1, input1_1, 'b', linewidth=1.5, label='input1')
ax0.plot(x_input1, input1_2, 'g', linewidth=1.5, label='input2')
ax0.plot(x_input1, input1_3, 'r', linewidth=1.5, label='input3')
ax0.plot(x_input1, input1_4, 'b', linewidth=1.5, label='input4')
ax0.plot(x_input1, input1_5, 'g', linewidth=1.5, label='input5')
ax0.plot(x_input1, input1_6, 'r', linewidth=1.5, label='input6')
ax0.plot(x_input1, input1_7, 'b', linewidth=1.5, label='input7')
ax0.plot(x_input1, input1_8, 'g', linewidth=1.5, label='input8')
ax0.plot(x_input1, input1_9, 'r', linewidth=1.5, label='input9')
ax0.plot(x_input1, input1_10, 'b', linewidth=1.5, label='input10')
ax0.plot(x_input1, input1_11, 'g', linewidth=1.5, label='input11')
ax0.set_title('Input 1')
ax0.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
# ax0.legend()

ax1.plot(x_input2, input2_1, 'b', linewidth=1.5)
ax1.plot(x_input2, input2_2, 'g', linewidth=1.5)
ax1.plot(x_input2, input2_3, 'r', linewidth=1.5)
ax1.plot(x_input2, input2_4, 'b', linewidth=1.5)
ax1.plot(x_input2, input2_5, 'g', linewidth=1.5)
ax1.plot(x_input2, input2_6, 'r', linewidth=1.5)
ax1.plot(x_input2, input2_7, 'b', linewidth=1.5)
ax1.plot(x_input2, input2_8, 'g', linewidth=1.5)
ax1.plot(x_input2, input2_9, 'r', linewidth=1.5)
ax1.plot(x_input2, input2_10, 'b', linewidth=1.5)
ax1.plot(x_input2, input2_11, 'g', linewidth=1.5)
ax1.set_title('Input 2')
ax1.legend()

ax2.plot(x_input3, input3_1, 'b', linewidth=1.5)
ax2.plot(x_input3, input3_2, 'g', linewidth=1.5)
ax2.plot(x_input3, input3_3, 'r', linewidth=1.5)
ax2.plot(x_input3, input3_4, 'b', linewidth=1.5)
ax2.plot(x_input3, input3_5, 'g', linewidth=1.5)
ax2.plot(x_input3, input3_6, 'r', linewidth=1.5)
ax2.plot(x_input3, input3_7, 'b', linewidth=1.5)
ax2.plot(x_input3, input3_8, 'g', linewidth=1.5)
ax2.plot(x_input3, input3_9, 'r', linewidth=1.5)
ax2.plot(x_input3, input3_10, 'b', linewidth=1.5)
ax2.plot(x_input3, input3_11, 'g', linewidth=1.5)
ax2.set_title('Input 3')
ax2.legend()

ax3.plot(x_output, output_1, 'b', linewidth=1.5)
ax3.plot(x_output, output_2, 'g', linewidth=1.5)
ax3.plot(x_output, output_3, 'r', linewidth=1.5)
ax3.plot(x_output, output_4, 'b', linewidth=1.5)
ax3.plot(x_output, output_5, 'g', linewidth=1.5)
ax3.plot(x_output, output_6, 'r', linewidth=1.5)
ax3.plot(x_output, output_7, 'b', linewidth=1.5)
ax3.plot(x_output, output_8, 'g', linewidth=1.5)
ax3.plot(x_output, output_9, 'r', linewidth=1.5)
ax3.plot(x_output, output_10, 'b', linewidth=1.5)
ax3.plot(x_output, output_11, 'g', linewidth=1.5)
ax3.set_title('Output')
ax3.legend()

# Turn off top/right axes
for ax in (ax0, ax1, ax2, ax3):
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.get_xaxis().tick_bottom()
	ax.get_yaxis().tick_left()

plt.show()

valueInput1 = input1

input1_level_1 = fuzz.interp_membership(x_input1, input1_1, valueInput1)
input1_level_2 = fuzz.interp_membership(x_input1, input1_2, valueInput1)
input1_level_3 = fuzz.interp_membership(x_input1, input1_3, valueInput1)
input1_level_4 = fuzz.interp_membership(x_input1, input1_4, valueInput1)
input1_level_5 = fuzz.interp_membership(x_input1, input1_5, valueInput1)
input1_level_6 = fuzz.interp_membership(x_input1, input1_6, valueInput1)
input1_level_7 = fuzz.interp_membership(x_input1, input1_7, valueInput1)
input1_level_8 = fuzz.interp_membership(x_input1, input1_8, valueInput1)
input1_level_9 = fuzz.interp_membership(x_input1, input1_9, valueInput1)
input1_level_10 = fuzz.interp_membership(x_input1, input1_10, valueInput1)
input1_level_11 = fuzz.interp_membership(x_input1, input1_11, valueInput1)

valueInput2 = input2

input2_level_1 = fuzz.interp_membership(x_input2, input2_1, valueInput2)
input2_level_2 = fuzz.interp_membership(x_input2, input2_2, valueInput2)
input2_level_3 = fuzz.interp_membership(x_input2, input2_3, valueInput2)
input2_level_4 = fuzz.interp_membership(x_input2, input2_4, valueInput2)
input2_level_5 = fuzz.interp_membership(x_input2, input2_5, valueInput2)
input2_level_6 = fuzz.interp_membership(x_input2, input2_6, valueInput2)
input2_level_7 = fuzz.interp_membership(x_input2, input2_7, valueInput2)
input2_level_8 = fuzz.interp_membership(x_input2, input2_8, valueInput2)
input2_level_9 = fuzz.interp_membership(x_input2, input2_9, valueInput2)
input2_level_10 = fuzz.interp_membership(x_input2, input2_10, valueInput2)
input2_level_11 = fuzz.interp_membership(x_input2, input2_11, valueInput2)

valueInput3 = input3

input3_level_1 = fuzz.interp_membership(x_input3, input3_1, valueInput3)
input3_level_2 = fuzz.interp_membership(x_input3, input3_2, valueInput3)
input3_level_3 = fuzz.interp_membership(x_input3, input3_3, valueInput3)
input3_level_4 = fuzz.interp_membership(x_input3, input3_4, valueInput3)
input3_level_5 = fuzz.interp_membership(x_input3, input3_5, valueInput3)
input3_level_6 = fuzz.interp_membership(x_input3, input3_6, valueInput3)
input3_level_7 = fuzz.interp_membership(x_input3, input3_7, valueInput3)
input3_level_8 = fuzz.interp_membership(x_input3, input3_8, valueInput3)
input3_level_9 = fuzz.interp_membership(x_input3, input3_9, valueInput3)
input3_level_10 = fuzz.interp_membership(x_input3, input3_10, valueInput3)
input3_level_11 = fuzz.interp_membership(x_input3, input3_11, valueInput3)

# VALIDACION DE REGLAS DIFUSAS
# Rule 1 concerns if input1 is in level 1 OR input2 is in level 1 OR input3 is in level 1 .
active_rule1 = np.fmax(input1_level_1, np.fmax( input2_level_1, input3_level_1))
level_activation_1 = np.fmin(active_rule1, output_1)

# Rule 2 concerns if input1 is in level 2 OR input2 is in level 2 OR input3 is in level 2.
active_rule2 = np.fmax(input1_level_2, np.fmax( input2_level_2, input3_level_2))
level_activation_2 = np.fmin(active_rule2, output_2)

# Rule 3 concerns if input1 is in level 3 OR input2 is in level 3 OR input3 is in level 3.
active_rule3 = np.fmax(input1_level_3, np.fmax( input2_level_3, input3_level_3))
level_activation_3 = np.fmin(active_rule3, output_3)

# Rule 4 concerns if input1 is in level 4 OR input2 is in level 4 OR input3 is in level 4.
active_rule4 = np.fmax(input1_level_4, np.fmax( input2_level_4, input3_level_4))
level_activation_4 = np.fmin(active_rule4, output_4)

# Rule 5 concerns if input1 is in level 5 OR input2 is in level 5 OR input3 is in level 5.
active_rule5 = np.fmax(input1_level_5, np.fmax( input2_level_5, input3_level_5))
level_activation_5 = np.fmin(active_rule5, output_5)

# Rule 6 concerns if input1 is in level 6 OR input2 is in level 6 OR input3 is in level 6.
active_rule6 = np.fmax(input1_level_6, np.fmax( input2_level_6, input3_level_6))
level_activation_6 = np.fmin(active_rule6, output_6)

# Rule 7 concerns if input1 is in level 7 OR input2 is in level 7 OR input3 is in level 7.
active_rule7 = np.fmax(input1_level_7, np.fmax( input2_level_7, input3_level_7))
level_activation_7 = np.fmin(active_rule7, output_7)

# Rule 8 concerns if input1 is in level 8 OR input2 is in level 8 OR input3 is in level 8.
active_rule8 = np.fmax(input1_level_8, np.fmax( input2_level_8, input3_level_8))
level_activation_8 = np.fmin(active_rule8, output_8)

# Rule 9 concerns if input1 is in level 9 OR input2 is in level 9 OR input3 is in level 9.
active_rule9 = np.fmax(input1_level_9, np.fmax( input2_level_9, input3_level_9))
level_activation_9 = np.fmin(active_rule9, output_9)

# Rule 10 concerns if input1 is in level 10 OR input2 is in level 11 OR input3 is in level 10.
active_rule10 = np.fmax(input1_level_10, np.fmax( input2_level_10, input3_level_10))
level_activation_10 = np.fmin(active_rule10, output_10)

# Rule 11 concerns if input1 is in level 11 OR input2 is in level 11 OR input3 is in level 11.
active_rule11 = np.fmax(input1_level_11, np.fmax( input2_level_10, input3_level_11))
level_activation_11 = np.fmin(active_rule11, output_11)

output = np.zeros_like(x_output)

print 'output: ', output

# Visualize this - Se coloria los campos donde entra las reglas definidas
fig, ax0 = plt.subplots(figsize=(15, 7))

ax0.fill_between(x_output, output, level_activation_1, facecolor='b', alpha=0.7)

ax0.plot(x_output, output_1, 'b', linewidth=0.5, linestyle='--', )

ax0.fill_between(x_output, output, level_activation_2, facecolor='g', alpha=0.7)

ax0.plot(x_output, output_2, 'g', linewidth=0.5, linestyle='--')

ax0.fill_between(x_output, output, level_activation_3, facecolor='r', alpha=0.7)

ax0.plot(x_output, output_3, 'r', linewidth=0.5, linestyle='--')

ax0.fill_between(x_output, output, level_activation_4, facecolor='b', alpha=0.7)

ax0.plot(x_output, output_4, 'b', linewidth=0.5, linestyle='--')

ax0.fill_between(x_output, output, level_activation_5, facecolor='g', alpha=0.7)

ax0.plot(x_output, output_5, 'g', linewidth=0.5, linestyle='--')

ax0.fill_between(x_output, output, level_activation_6, facecolor='r', alpha=0.7)

ax0.plot(x_output, output_6, 'r', linewidth=0.5, linestyle='--')

ax0.fill_between(x_output, output, level_activation_7, facecolor='b', alpha=0.7)

ax0.plot(x_output, output_7, 'b', linewidth=0.5, linestyle='--')

ax0.fill_between(x_output, output, level_activation_8, facecolor='g', alpha=0.7)

ax0.plot(x_output, output_8, 'g', linewidth=0.5, linestyle='--')

ax0.fill_between(x_output, output, level_activation_9, facecolor='r', alpha=0.7)

ax0.plot(x_output, output_9, 'r', linewidth=0.5, linestyle='--')

ax0.fill_between(x_output, output, level_activation_10, facecolor='b', alpha=0.7)

ax0.plot(x_output, output_10, 'b', linewidth=0.5, linestyle='--')

ax0.fill_between(x_output, output, level_activation_11, facecolor='g', alpha=0.7)

ax0.plot(x_output, output_11, 'g', linewidth=0.5, linestyle='--')

ax0.set_title('Actividad de la salidad de membresia')

# Turn off top/right axes
for ax in (ax0,):
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.get_xaxis().tick_bottom()
	ax.get_yaxis().tick_left()

plt.show()

# Aggregate all three output membership functions together - Se colorea el espacio difuso
aggregated = np.fmax(level_activation_1,
np.fmax(level_activation_2, 
	np.fmax( level_activation_3, 
		np.fmax( level_activation_4 , 
			np.fmax( level_activation_5 , 
				np.fmax( level_activation_6, 
					np.fmax( level_activation_7, 
						np.fmax(level_activation_8, 
							np.fmax(level_activation_9, 
								np.fmax(level_activation_10, level_activation_11)) ) ) ) )))  ))

print 'aggregated: ', aggregated

# Calculate defuzzified result
outputFinal = fuzz.defuzz(x_output, aggregated, 'centroid')

print 'outputFinal: ', outputFinal

output_activation = fuzz.interp_membership(x_output, aggregated, outputFinal) # for plot

# Grado de membresia del output
print 'output_activation: ', output_activation

# Visualize this
fig, ax0 = plt.subplots(figsize=(15, 7))
ax0.plot(x_output, output_1, 'b', linewidth=0.5, linestyle='--', )
ax0.plot(x_output, output_2, 'g', linewidth=0.5, linestyle='--')
ax0.plot(x_output, output_3, 'r', linewidth=0.5, linestyle='--')
ax0.plot(x_output, output_4, 'b', linewidth=0.5, linestyle='--')
ax0.plot(x_output, output_5, 'g', linewidth=0.5, linestyle='--')
ax0.plot(x_output, output_6, 'r', linewidth=0.5, linestyle='--')
ax0.plot(x_output, output_7, 'b', linewidth=0.5, linestyle='--')
ax0.plot(x_output, output_8, 'g', linewidth=0.5, linestyle='--')
ax0.plot(x_output, output_9, 'r', linewidth=0.5, linestyle='--')
ax0.plot(x_output, output_10, 'b', linewidth=0.5, linestyle='--')
ax0.plot(x_output, output_11, 'g', linewidth=0.5, linestyle='--')

ax0.fill_between(x_output, output, aggregated, facecolor='Orange', alpha=0.7)
ax0.plot([outputFinal, outputFinal], [0, output_activation], 'k', linewidth=1.5, alpha=0.9)
ax0.set_title('Aggregated membership and result (line)')

# Turn off top/right axes
for ax in (ax0,):
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.get_xaxis().tick_bottom()
	ax.get_yaxis().tick_left()

plt.show()
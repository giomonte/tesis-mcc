import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt
import csv
import copy

def obtenerArchivo(archivo, numeroDatos):

    return funcionLeerArchivo( archivo, numeroDatos)

def funcionLeerArchivo(archivo, numeroDatos):
    valorIncorrecto = -1
    datosCrudos = np.genfromtxt(archivo ,delimiter=',')

    #Tamano de datos lectura n datos
    datosCrudos =  datosCrudos[len(datosCrudos) - numeroDatos:]

    return normalizaDatos(datosCrudos, valorIncorrecto)
    
def normalizaDatos(datosCrudos, valorIncorrecto):

    datosCrudosSinValorIncorrecto = copy.copy(datosCrudos)

    datosCrudosSinValorIncorrecto = filter( lambda x: x != -1, datosCrudosSinValorIncorrecto )

    numeroMaximo = np.amax( datosCrudosSinValorIncorrecto )
    numeroMinimo = np.amin( datosCrudosSinValorIncorrecto )

    return map(lambda x : "NA" if x == -1 else (( x - numeroMinimo ) / ( numeroMaximo - numeroMinimo)), datosCrudos)

def formatInputTarget(datos, ventana):
    inputArray = []
    targetArray = []

    for i in range (0, ( len(datos) - ventana - 1)):

        input = datos[ i : i + ventana ]
        target = datos[ i + ventana ]

        if "NA" not in input and target != "NA":
            inputArray.append(input)
            targetArray.append(target)

    input = np.array(inputArray)
    inp = input.reshape(len(input), ventana)

    target = np.array(targetArray)
    lenT = len(target)
    tar = target.reshape(lenT, 1)

    return inp, tar

def obtenerVentanaDatos(archivo, numeroMuestra, ventana):

	numeroValidacion = int(( len(archivo) * 0.20 ))

	numeroTraining = numeroMuestra - numeroValidacion

	datosNormalizados = archivo

	#Del 70 porciento atenrior se toma el 70 porciento para datos de entrenamiento
	datosEntrenamiento = datosNormalizados[:numeroTraining]

	datosValidacion = datosNormalizados[numeroTraining:]

	inpEntrenamiento, tarEntrenamiento = formatInputTarget(datosEntrenamiento, ventana)

	inpValidacion, tarValidacion = formatInputTarget(datosValidacion, ventana)

	return inpEntrenamiento, tarEntrenamiento, inpValidacion, tarValidacion

# Generamos el universo de discuros para los valores de entrada y del valor de salida
def getInputs(datos, ventana):

	arrayInputs = []
	x_output = np.arange(0.0, 1.01, 0.01)

	for i in xrange(0,ventana):

		x_input = np.arange(0.0, 1.01, 0.01)

		arrayInputs.append(x_input);

	return arrayInputs, x_output 

# Generamos los conjuntos difusos para cada valor de entrada y del valor de salida
def generateFuzzyMembershipFunctions(arrayInputs, x_output):		

	# Numero de conjutos difusos
	numFuncMemb = 11

	# Se crea una matrix para obtener los conjuntos difusos de cada entrada
	matrixFuncMembershipsInputs = []
	# Se crea una matrix para obtener los conjuntos difusos de la salida
	matrixFuncMembershipsOutput = []
	# Se crea un array para obtener los conjuntos difusos de la salida esperada
	arrayMembershipsFuncionsOutput = []

	# Se recorre el array de entradas
	for i in xrange(0, len(arrayInputs) ):

		# Creamos un arrray para guardar los conjuntos para cada entrada
		arrayMembershipsFuncionsInput = []

		# Se generan valores de los rangos que tendra cada conjunto difuso de los datos de entrada
		value1 = 0.0
		value2 = 0.0
		value3 = 0.1

		for j in xrange(0, numFuncMemb):

			funcMember = fuzz.trimf(arrayInputs[i], [value1, value2, value3])

			arrayMembershipsFuncionsInput.append(funcMember)

			value1 = value2
			value2 = value3
			if( j < ( numFuncMemb - 2) ):
				value3 = value3 + 0.1
			else:
				value3 = 1.01

		matrixFuncMembershipsInputs.append(arrayMembershipsFuncionsInput)	
	
	# Se generan los valores de los rangos que tranda cada conjunto difuso de la salida
	value1 = 0.0
	value2 = 0.0
	value3 = 0.1

	# se recorre el array de la salida
	for i in xrange(0, numFuncMemb ):

		funcMember = fuzz.trimf(x_output, [value1, value2, value3])
		arrayMembershipsFuncionsOutput.append(funcMember)

		value1 = value2
		value2 = value3
		if( i < ( numFuncMemb - 2) ):
			value3 = value3 + 0.1
		else:
			value3 = 1.01

    # Se guardan los conjuntos difusos de la salida en una matriz
	matrixFuncMembershipsOutput.append(arrayMembershipsFuncionsOutput)

	return 	matrixFuncMembershipsInputs, matrixFuncMembershipsOutput


# Obtener el grado de membresia para cada valor de entrada en cada conjunto difuso
def getDegreeMemberships(arrayInputs, matrixFuncMembershipsInputs, arrayValueInputs):

	# Se crea una matrix para obtener el grado de membresia de los valores de entrada en cada conjunto difuso
	matrixLevelsInputs = []

	# Recorremos los el universo de discurso se cada valor de entrada
	for i in xrange(0, len(arrayInputs) ):

		arrayLevels = []

		# Recorremos cada conjunto difuso para evaluar el grado de membresia de los valores de entrada
		for j in xrange(0, len(matrixFuncMembershipsInputs[i]) ):

			# Se genera el grado de membresia que pertenese cada valor de entrada en cada conjunto difuso
			level = fuzz.interp_membership(arrayInputs[i], matrixFuncMembershipsInputs[i][j], arrayValueInputs[i])

			arrayLevels.append(level)

		matrixLevelsInputs.append(arrayLevels)
		
	return matrixLevelsInputs

# Se realiza la validacion de las reglas difusas
def validationRules(matrixLevelsInputs, matrixFuncMembershipsOutput):


	arrayLevelsActivation = []

	numRules =  11

	for i in xrange(0, numRules):
		
		active_rule = np.fmax(matrixLevelsInputs[0][i], matrixLevelsInputs[1][i])
		level_activation = np.fmin(active_rule, matrixFuncMembershipsOutput[0][i])
		arrayLevelsActivation.append(level_activation)


	return arrayLevelsActivation



# Aggregate all three output membership functions together - Se colorea el espacio difuso
def getAggregate(arrayLevelsActivation):

	aggregated = np.fmax(arrayLevelsActivation[0],
	np.fmax(arrayLevelsActivation[1], 
		np.fmax( arrayLevelsActivation[2], 
			np.fmax( arrayLevelsActivation[3] , 
				np.fmax( arrayLevelsActivation[4] , 
					np.fmax( arrayLevelsActivation[5], 
						np.fmax( arrayLevelsActivation[6], 
							np.fmax(arrayLevelsActivation[7], 
								np.fmax(arrayLevelsActivation[8], 
									np.fmax(arrayLevelsActivation[9], arrayLevelsActivation[10])) ) ) ) )))  ))

	return aggregated

# # Calculate defuzzified result
def defuzzy(x_output, aggregated):

	outputFinal = fuzz.defuzz(x_output, aggregated, 'centroid')

	return outputFinal

# Graficamos los conjuntos difusos que intervienen en los valores de entrada
def graphFuzzySet1(x_output, output, arrayLevelsActivation, matrixFuncMembershipsOutput):
	# Visualize this - Se coloria los campos donde entra las reglas definidas
	fig, ax0 = plt.subplots(figsize=(15, 7))
	ax0.fill_between(x_output, output, arrayLevelsActivation[0], facecolor='b', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][0], 'b', linewidth=0.5, linestyle='--', )
	ax0.fill_between(x_output, output, arrayLevelsActivation[1], facecolor='g', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][1], 'g', linewidth=0.5, linestyle='--')
	ax0.fill_between(x_output, output, arrayLevelsActivation[2], facecolor='r', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][2], 'r', linewidth=0.5, linestyle='--')
	ax0.fill_between(x_output, output, arrayLevelsActivation[3], facecolor='b', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][3], 'b', linewidth=0.5, linestyle='--')
	ax0.fill_between(x_output, output, arrayLevelsActivation[4], facecolor='g', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][4], 'g', linewidth=0.5, linestyle='--')
	ax0.fill_between(x_output, output, arrayLevelsActivation[5], facecolor='r', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][5], 'r', linewidth=0.5, linestyle='--')
	ax0.fill_between(x_output, output, arrayLevelsActivation[6], facecolor='b', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][6], 'b', linewidth=0.5, linestyle='--')
	ax0.fill_between(x_output, output, arrayLevelsActivation[7], facecolor='g', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][7], 'g', linewidth=0.5, linestyle='--')
	ax0.fill_between(x_output, output, arrayLevelsActivation[8], facecolor='r', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][8], 'r', linewidth=0.5, linestyle='--')
	ax0.fill_between(x_output, output, arrayLevelsActivation[9], facecolor='b', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][9], 'b', linewidth=0.5, linestyle='--')
	ax0.fill_between(x_output, output, arrayLevelsActivation[10], facecolor='g', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][10], 'g', linewidth=0.5, linestyle='--')
	ax0.set_title('Actividad de la salidad de membresia')

	# Turn off top/right axes
	for ax in (ax0,):
		ax.spines['top'].set_visible(False)
		ax.spines['right'].set_visible(False)
		ax.get_xaxis().tick_bottom()
		ax.get_yaxis().tick_left()

	plt.show()

# Graficamos el resultado de todas las reglas difusas activadas utlizando el metodo del centroide
def graphFuzzySetwithCentroid(x_output, output, outputFinal, output_activation, aggregated, matrixFuncMembershipsOutput):
	# Visualize this
	fig, ax0 = plt.subplots(figsize=(15, 7))
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][0], 'b', linewidth=0.5, linestyle='--', )
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][1], 'g', linewidth=0.5, linestyle='--')
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][2], 'r', linewidth=0.5, linestyle='--')
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][3], 'b', linewidth=0.5, linestyle='--')
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][4], 'g', linewidth=0.5, linestyle='--')
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][5], 'r', linewidth=0.5, linestyle='--')
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][6], 'b', linewidth=0.5, linestyle='--')
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][7], 'g', linewidth=0.5, linestyle='--')
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][8], 'r', linewidth=0.5, linestyle='--')
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][9], 'b', linewidth=0.5, linestyle='--')
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][10], 'g', linewidth=0.5, linestyle='--')

	ax0.fill_between(x_output, output, aggregated, facecolor='Orange', alpha=0.7)
	ax0.plot([outputFinal, outputFinal], [0, output_activation], 'k', linewidth=1.5, alpha=0.9)
	ax0.set_title('Aggregated membership and result (line)')

	# Turn off top/right axes
	for ax in (ax0,):
		ax.spines['top'].set_visible(False)
		ax.spines['right'].set_visible(False)
		ax.get_xaxis().tick_bottom()
		ax.get_yaxis().tick_left()

	plt.show()

# Genera un archivo CSV
def exportToCSV(archivoCSV, tar, out):

	with open(archivoCSV, "wb") as ArchivoNuevoCSV:

	    writer = csv.writer(ArchivoNuevoCSV, lineterminator='\n')

	    writer.writerow(['Target', 'Output'])

	    for i in range(0, len(tar)):

	      target = str(tar[i])
	      output = str(out[i])
	      
	      aux1 = target.replace("[", "")
	      tarAux1 = aux1.replace("]", "")

	      aux2 = output.replace("[", "")
	      outAux2 = aux2.replace("]", "")

	      writer.writerow( (tarAux1, outAux2) )

def calculateMSE(targets, outputs):

    sum = 0

    for index, elemet in enumerate(targets):
        forecasting = outputs[index]
        sum = sum + ((forecasting - elemet)**2)

    return sum / len(targets)
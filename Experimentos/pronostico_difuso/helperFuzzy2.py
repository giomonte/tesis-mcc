import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt
import csv
import copy

def obtenerArchivo(archivo, numeroDatos):

    return funcionLeerArchivo( archivo, numeroDatos)

def funcionLeerArchivo(archivo, numeroDatos):
    valorIncorrecto = -1
    datosCrudos = np.genfromtxt(archivo ,delimiter=',')

    #Tamano de datos lectura n datos
    datosCrudos =  datosCrudos[len(datosCrudos) - numeroDatos:]

    return normalizaDatos(datosCrudos, valorIncorrecto)
    
def normalizaDatos(datosCrudos, valorIncorrecto):

    datosCrudosSinValorIncorrecto = copy.copy(datosCrudos)

    datosCrudosSinValorIncorrecto = filter( lambda x: x != -1, datosCrudosSinValorIncorrecto )

    numeroMaximo = np.amax( datosCrudosSinValorIncorrecto )
    numeroMinimo = np.amin( datosCrudosSinValorIncorrecto )

    return map(lambda x : "NA" if x == -1 else (( x - numeroMinimo ) / ( numeroMaximo - numeroMinimo)), datosCrudos)

def formatInputTarget(datos, ventana):
    inputArray = []
    targetArray = []

    for i in range (0, ( len(datos) - ventana - 1)):

        input = datos[ i : i + ventana ]
        target = datos[ i + ventana ]

        if "NA" not in input and target != "NA":
            inputArray.append(input)
            targetArray.append(target)

    input = np.array(inputArray)
    inp = input.reshape(len(input), ventana)

    target = np.array(targetArray)
    lenT = len(target)
    tar = target.reshape(lenT, 1)

    return inp, tar

def obtenerVentanaDatos(archivo, numeroMuestra, ventana):

	numeroValidacion = int(( len(archivo) * 0.20 ))

	numeroTraining = numeroMuestra - numeroValidacion

	datosNormalizados = archivo

	#Del 70 porciento atenrior se toma el 70 porciento para datos de entrenamiento
	datosEntrenamiento = datosNormalizados[:numeroTraining]

	datosValidacion = datosNormalizados[numeroTraining:]

	inpEntrenamiento, tarEntrenamiento = formatInputTarget(datosEntrenamiento, ventana)

	inpValidacion, tarValidacion = formatInputTarget(datosValidacion, ventana)

	return inpEntrenamiento, tarEntrenamiento, inpValidacion, tarValidacion

# Generamos el universo de discuros para los valores de entrada y del valor de salida
def getInputs(ventana):

	arrayInputs = []
	x_output = np.arange(0.0, 1.01, 0.01)

	for i in xrange(0, ventana):

		x_input = np.arange(0.0, 1.01, 0.01)

		arrayInputs.append(x_input);

	return arrayInputs, x_output 

def generateFuzzyMembershipFunctions2(arrayInputs, x_output, numFuncMemb):

	# Se crea una matrix para obtener los conjuntos difusos de cada entrada
	matrixFuncMembershipsInputs = []
	# Se crea una matrix para obtener los conjuntos difusos de la salida
	matrixFuncMembershipsOutput = []
	# Se crea un array para obtener los conjuntos difusos de la salida esperada
	arrayMembershipsFuncionsOutput = []

	# Se define el tamanio de cada conjunto difuso
	inicio =  1.0 / ( numFuncMemb - 1 ) 

	# Se recorre el array de entradas
	for i in xrange(0, len(arrayInputs) ):

		# Creamos un arrray para guardar los conjuntos para cada entrada
		arrayMembershipsFuncionsInput = []

		# Se generan valores de los rangos que tendra cada conjunto difuso de los datos de entrada
		value1 = 0.0
		value2 = 0.0
		value3 = inicio

		for j in xrange(0, numFuncMemb):

			funcMember = fuzz.trimf(arrayInputs[i], [value1, value2, value3])

			arrayMembershipsFuncionsInput.append(funcMember)

			value1 = value2
			value2 = value3
			if( j < ( numFuncMemb - 2) ):
				value3 = value3 + inicio
			else:
				value3 = 1.01

		matrixFuncMembershipsInputs.append(arrayMembershipsFuncionsInput)	

	# Se generan los valores de los rangos que tranda cada conjunto difuso de la salida
	value1 = 0.0
	value2 = 0.0
	value3 = inicio

	# se recorre el array de la salida
	for i in xrange(0, numFuncMemb ):

		funcMember = fuzz.trimf(x_output, [value1, value2, value3])
		arrayMembershipsFuncionsOutput.append(funcMember)

		value1 = value2
		value2 = value3
		if( i < ( numFuncMemb - 2) ):
			value3 = value3 + inicio
		else:
			value3 = 1.01

    # Se guardan los conjuntos difusos de la salida en una matriz
	matrixFuncMembershipsOutput.append(arrayMembershipsFuncionsOutput)

	return 	matrixFuncMembershipsInputs, matrixFuncMembershipsOutput

# Generamos los conjuntos difusos para cada valor de entrada y del valor de salida
def generateFuzzyMembershipFunctions(arrayInputs, x_output, numFuncMemb):		

	# Se crea una matrix para obtener los conjuntos difusos de cada entrada
	matrixFuncMembershipsInputs = []
	# Se crea una matrix para obtener los conjuntos difusos de la salida
	matrixFuncMembershipsOutput = []
	# Se crea un array para obtener los conjuntos difusos de la salida esperada
	arrayMembershipsFuncionsOutput = []

	# Se recorre el array de entradas
	for i in xrange(0, len(arrayInputs) ):

		# Creamos un arrray para guardar los conjuntos para cada entrada
		arrayMembershipsFuncionsInput = []

		# Se generan valores de los rangos que tendra cada conjunto difuso de los datos de entrada
		value1 = 0.0
		value2 = 0.0
		value3 = 0.1

		for j in xrange(0, numFuncMemb):

			funcMember = fuzz.trimf(arrayInputs[i], [value1, value2, value3])

			arrayMembershipsFuncionsInput.append(funcMember)

			value1 = value2
			value2 = value3
			if( j < ( numFuncMemb - 2) ):
				value3 = value3 + 0.1
			else:
				value3 = 1.01

		matrixFuncMembershipsInputs.append(arrayMembershipsFuncionsInput)	
	
	# Se generan los valores de los rangos que tranda cada conjunto difuso de la salida
	value1 = 0.0
	value2 = 0.0
	value3 = 0.1

	# se recorre el array de la salida
	for i in xrange(0, numFuncMemb ):

		funcMember = fuzz.trimf(x_output, [value1, value2, value3])
		arrayMembershipsFuncionsOutput.append(funcMember)

		value1 = value2
		value2 = value3
		if( i < ( numFuncMemb - 2) ):
			value3 = value3 + 0.1
		else:
			value3 = 1.01

    # Se guardan los conjuntos difusos de la salida en una matriz
	matrixFuncMembershipsOutput.append(arrayMembershipsFuncionsOutput)

	return 	matrixFuncMembershipsInputs, matrixFuncMembershipsOutput


# Obtener el grado de membresia para cada valor de entrada en cada conjunto difuso
def getDegreeMemberships(arrayInputs, matrixFuncMembershipsInputs, arrayValueInputs):

	# Se crea una matrix para obtener el grado de membresia de los valores de entrada en cada conjunto difuso
	matrixLevelsInputs = []

	# Recorremos los el universo de discurso se cada valor de entrada
	for i in xrange(0, len(arrayInputs) ):

		arrayLevels = []
		print "arrayValueInputs: ", i, " : ", arrayValueInputs[i]

		# Recorremos cada conjunto difuso para evaluar el grado de membresia de los valores de entrada
		for j in xrange(0, len(matrixFuncMembershipsInputs[i]) ):

			# Se genera el grado de membresia que pertenese cada valor de entrada en cada conjunto difuso
			level = fuzz.interp_membership(arrayInputs[i], matrixFuncMembershipsInputs[i][j], arrayValueInputs[i])

			print "level (conjuntoDifuso): ", (j), " : " , level

			arrayLevels.append(level)

		print ""

		matrixLevelsInputs.append(arrayLevels)
	
	print "matrixLevelsInputs: ",  matrixLevelsInputs

	return matrixLevelsInputs

# Definicion de reglas difusas
def validationRules2(matrixLevelsInputs, matrixFuncMembershipsOutput):

	arrayLevelsActivation = []

	# # print "matrixFuncMembershipsOutput: ", len(matrixFuncMembershipsOutput[0])
	print "matrixLevelsInputs: ", matrixLevelsInputs

	for i in xrange(0, len(matrixLevelsInputs)):

		for j in xrange(0 ,len(matrixLevelsInputs[i])):
		# if (  matrixLevelsInputs[i][j] == 1.0 ):
			print matrixLevelsInputs[i][j]
		print ""

	# print matrixLevelsInputs[3][1]
	# print matrixLevelsInputs[2][1]
	# print matrixLevelsInputs[1][4]
	# print matrixLevelsInputs[0][2]
	# print ""
	# print matrixFuncMembershipsOutput[0][0]
	# print ""

	# # Rule 1 - 
	# # If Xn is LT1 and Xn-1 is LT1 and Xn-2 is LT4 and Xn-3 is LT2 
	# # then Xn+1 is LT0
	# rule1 = np.fmin( matrixLevelsInputs[3][1], np.fmin( matrixLevelsInputs[2][1], np.fmin( matrixLevelsInputs[1][4], matrixLevelsInputs[0][2])))
	# activation_rule1 = np.fmin(rule1, matrixFuncMembershipsOutput[0][0])
	# # print "rule1: ", rule1
	# arrayLevelsActivation.append(activation_rule1)

	# # Rule 2 - 
	# # If Xn is LT1 and Xn-1 is LT1 and Xn-2 is LT4 and Xn-3 is LT0 
	# # then Xn+1 is LT3
	# rule2 = np.fmin( matrixLevelsInputs[3][1], np.fmin( matrixLevelsInputs[2][1], np.fmin( matrixLevelsInputs[1][4], matrixLevelsInputs[0][0])))
	# activation_rule2 = np.fmin(rule2, matrixFuncMembershipsOutput[0][3])
	# # print "rule2: ", rule2
	# arrayLevelsActivation.append(activation_rule2)

	# # Rule 3 - 
	# # If Xn is LT1 and Xn-1 is LT1 and Xn-2 is LT4 and Xn-3 is LT0 
	# # then Xn+1 is LT2
	# rule3 = np.fmin( matrixLevelsInputs[3][1], np.fmin( matrixLevelsInputs[2][1], np.fmin( matrixLevelsInputs[1][4], matrixLevelsInputs[0][0])))
	# activation_rule3 = np.fmin(rule3, matrixFuncMembershipsOutput[0][2])
	# # print "rule3: ", rule3
	# arrayLevelsActivation.append(activation_rule3)

	# # Rule 4 - 
	# # If Xn is LT1 and Xn-1 is LT1 and Xn-2 is LT1 and Xn-3 is LT1 
	# # then Xn+1 is LT1
	# rule4 = np.fmin( matrixLevelsInputs[3][1], np.fmin( matrixLevelsInputs[2][1], np.fmin( matrixLevelsInputs[1][1], matrixLevelsInputs[0][1])))
	# activation_rule4 = np.fmin(rule4, matrixFuncMembershipsOutput[0][1])
	# # print "rule4: ", rule4
	# arrayLevelsActivation.append(activation_rule4)

	# # Rule 5 - 
	# # If Xn is LT2 and Xn-1 is LT1 and Xn-2 is LT2 and Xn-3 is LT2 
	# # then Xn+1 is LT0
	# rule5 = np.fmin( matrixLevelsInputs[3][2], np.fmin( matrixLevelsInputs[2][1], np.fmin( matrixLevelsInputs[1][2], matrixLevelsInputs[0][2])))
	# activation_rule5 = np.fmin(rule5, matrixFuncMembershipsOutput[0][0])
	# # print "rule5: ", rule5
	# arrayLevelsActivation.append(activation_rule5)

	# # Rule 6 - 
	# # If Xn is LT0 and Xn-1 is LT2 and Xn-2 is LT1 and Xn-3 is LT2 
	# # then Xn+1 is LT0
	# rule6 = np.fmin( matrixLevelsInputs[3][0], np.fmin( matrixLevelsInputs[2][2], np.fmin( matrixLevelsInputs[1][1], matrixLevelsInputs[0][2])))
	# activation_rule6 = np.fmin(rule6, matrixFuncMembershipsOutput[0][0])
	# # print "rule6: ", rule6
	# arrayLevelsActivation.append(activation_rule6)

	# # Rule 7 - 
	# # If Xn is LT3 and Xn-1 is LT2 and Xn-2 is LT1 and Xn-3 is LT1 
	# # then Xn+1 is LT4
	# rule7 = np.fmin( matrixLevelsInputs[3][3], np.fmin( matrixLevelsInputs[2][2], np.fmin( matrixLevelsInputs[1][1], matrixLevelsInputs[0][1])))
	# activation_rule7 = np.fmin(rule7, matrixFuncMembershipsOutput[0][4])
	# # print "rule7: ", rule7
	# arrayLevelsActivation.append(activation_rule7)

	# # Rule 8 - 
	# # If Xn is LT3 and Xn-1 is LT3 and Xn-2 is LT3 and Xn-3 is LT2 
	# # then Xn+1 is LT2
	# rule8 = np.fmin( matrixLevelsInputs[3][3], np.fmin( matrixLevelsInputs[2][3], np.fmin( matrixLevelsInputs[1][3], matrixLevelsInputs[0][2])))
	# activation_rule8 = np.fmin(rule8, matrixFuncMembershipsOutput[0][2])
	# # print "rule8: ", rule8
	# arrayLevelsActivation.append(activation_rule8)

	# # Rule 9 - 
	# # If Xn is LT1 and Xn-1 is LT2 and Xn-2 is LT3 and Xn-3 is LT3 
	# # then Xn+1 is LT1
	# rule9 = np.fmin( matrixLevelsInputs[3][1], np.fmin( matrixLevelsInputs[2][2], np.fmin( matrixLevelsInputs[1][3], matrixLevelsInputs[0][3])))
	# activation_rule9 = np.fmin(rule9, matrixFuncMembershipsOutput[0][1])
	# # print "rule9: ", rule9
	# arrayLevelsActivation.append(activation_rule9)

	# # Rule 10 - 
	# # If Xn is LT1 and Xn-1 is LT1 and Xn-2 is LT2 and Xn-3 is LT3 
	# # then Xn+1 is LT1
	# rule10 = np.fmin( matrixLevelsInputs[3][1], np.fmin( matrixLevelsInputs[2][1], np.fmin( matrixLevelsInputs[1][3], matrixLevelsInputs[0][3])))
	# activation_rule10 = np.fmin(rule10, matrixFuncMembershipsOutput[0][1])
	# # print "rule10: ", rule10
	# arrayLevelsActivation.append(activation_rule10)

	# # Rule 11 - 
	# # If Xn is LT4 and Xn-1 is LT3 and Xn-2 is LT3 and Xn-3 is LT2 
	# # then Xn+1 is LT3
	# rule11 = np.fmin( matrixLevelsInputs[3][4], np.fmin( matrixLevelsInputs[2][3], np.fmin( matrixLevelsInputs[1][3], matrixLevelsInputs[0][2])))
	# activation_rule11 = np.fmin(rule11, matrixFuncMembershipsOutput[0][3])
	# # print "rule11: ", rule11
	# arrayLevelsActivation.append(activation_rule11)

	# Rule 12 - 
	# If Xn is LT0 and Xn-1 is LT0 and Xn-2 is LT0 and Xn-3 is LT0 
	# then Xn+1 is LT0
	rule12 = np.fmin( matrixLevelsInputs[3][0], np.fmin( matrixLevelsInputs[2][0], np.fmin( matrixLevelsInputs[1][0], matrixLevelsInputs[0][0])))
	activation_rule12 = np.fmin(rule12, matrixFuncMembershipsOutput[0][0])
	# print "rule12: ", rule12
	arrayLevelsActivation.append(activation_rule12)

	# Rule 13 - 
	# If Xn is LT1 and Xn-1 is LT1 and Xn-2 is LT1 and Xn-3 is LT1 
	# then Xn+1 is LT1
	rule13 = np.fmin( matrixLevelsInputs[3][1], np.fmin( matrixLevelsInputs[2][1], np.fmin( matrixLevelsInputs[1][1], matrixLevelsInputs[0][1])))
	activation_rule13 = np.fmin(rule13, matrixFuncMembershipsOutput[0][1])
	# print "rule13: ", rule13
	arrayLevelsActivation.append(activation_rule13)

	# Rule 14 - 
	# If Xn is LT2 and Xn-1 is LT2 and Xn-2 is LT2 and Xn-3 is LT2 
	# then Xn+1 is LT2
	rule14 = np.fmin( matrixLevelsInputs[3][2], np.fmin( matrixLevelsInputs[2][2], np.fmin( matrixLevelsInputs[1][2], matrixLevelsInputs[0][2])))
	activation_rule14 = np.fmin(rule14, matrixFuncMembershipsOutput[0][2])
	# print "rule14: ", rule14
	arrayLevelsActivation.append(activation_rule14)

	# Rule 15 - 
	# If Xn is LT3 and Xn-1 is LT3 and Xn-2 is LT3 and Xn-3 is LT3 
	# then Xn+1 is LT3
	rule15 = np.fmin( matrixLevelsInputs[3][3], np.fmin( matrixLevelsInputs[2][3], np.fmin( matrixLevelsInputs[1][3], matrixLevelsInputs[0][3])))
	activation_rule15 = np.fmin(rule15, matrixFuncMembershipsOutput[0][3])
	# print "rule15: ", rule15
	arrayLevelsActivation.append(activation_rule15)

	# Rule 16 - 
	# If Xn is LT4 and Xn-1 is LT4 and Xn-2 is LT4 and Xn-3 is LT4 
	# then Xn+1 is LT4
	rule16 = np.fmin( matrixLevelsInputs[3][4], np.fmin( matrixLevelsInputs[2][4], np.fmin( matrixLevelsInputs[1][4], matrixLevelsInputs[0][4])))
	activation_rule16 = np.fmin(rule16, matrixFuncMembershipsOutput[0][4])
	# print "rule16: ", rule16
	arrayLevelsActivation.append(activation_rule16)

	# # Rule 17 - 
	# # If Xn is LT2 and Xn-1 is LT2 and Xn-2 is LT1 and Xn-3 is LT1 
	# # then Xn+1 is LT2
	# rule17 = np.fmin( matrixLevelsInputs[3][2], np.fmin( matrixLevelsInputs[2][2], np.fmin( matrixLevelsInputs[1][1], matrixLevelsInputs[0][1])))
	# activation_rule17 = np.fmin(rule17, matrixFuncMembershipsOutput[0][2])
	# # print "rule17: ", rule17
	# arrayLevelsActivation.append(activation_rule17)

	# # Rule 18 - 
	# # If Xn is LT2 and Xn-1 is LT2 and Xn-2 is LT2 and Xn-3 is LT1 
	# # then Xn+1 is LT1
	# rule18 = np.fmin( matrixLevelsInputs[3][2], np.fmin( matrixLevelsInputs[2][2], np.fmin( matrixLevelsInputs[1][2], matrixLevelsInputs[0][1])))
	# activation_rule18 = np.fmin(rule18, matrixFuncMembershipsOutput[0][1])
	# # print "rule18: ", rule18
	# arrayLevelsActivation.append(activation_rule18)

	# # Rule 19 - 
	# # If Xn is LT0 and Xn-1 is LT1 and Xn-2 is LT2 and Xn-3 is LT1 
	# # then Xn+1 is LT1
	# rule19 = np.fmin( matrixLevelsInputs[3][0], np.fmin( matrixLevelsInputs[2][1], np.fmin( matrixLevelsInputs[1][2], matrixLevelsInputs[0][1])))
	# activation_rule19 = np.fmin(rule19, matrixFuncMembershipsOutput[0][1])
	# # print "rule19: ", rule19
	# arrayLevelsActivation.append(activation_rule19)

	# # Rule 20 - 
	# # If Xn is LT3 and Xn-1 is LT1 and Xn-2 is LT0 and Xn-3 is LT1 
	# # then Xn+1 is LT3
	# rule20 = np.fmin( matrixLevelsInputs[3][3], np.fmin( matrixLevelsInputs[2][1], np.fmin( matrixLevelsInputs[1][0], matrixLevelsInputs[0][1])))
	# activation_rule20 = np.fmin(rule20, matrixFuncMembershipsOutput[0][3])
	# # print "rule20: ", rule20
	# arrayLevelsActivation.append(activation_rule20)

	# # Rule 21 - 
	# # If Xn is LT1 and Xn-1 is LT0 and Xn-2 is LT1 and Xn-3 is LT2 
	# # then Xn+1 is LT0
	# rule21 = np.fmin( matrixLevelsInputs[3][1], np.fmin( matrixLevelsInputs[2][0], np.fmin( matrixLevelsInputs[1][1], matrixLevelsInputs[0][2])))
	# activation_rule21 = np.fmin(rule21, matrixFuncMembershipsOutput[0][0])
	# # print "rule21: ", rule21
	# arrayLevelsActivation.append(activation_rule21)

	# # Rule 22 - 
	# # If Xn is LT0 and Xn-1 is LT1 and Xn-2 is LT2 and Xn-3 is LT3 
	# # then Xn+1 is LT4
	# rule22 = np.fmin( matrixLevelsInputs[3][0], np.fmin( matrixLevelsInputs[2][1], np.fmin( matrixLevelsInputs[1][2], matrixLevelsInputs[0][3])))
	# activation_rule22 = np.fmin(rule22, matrixFuncMembershipsOutput[0][4])
	# # print "rule22: ", rule22
	# arrayLevelsActivation.append(activation_rule22)

	# # Rule 23 - 
	# # If Xn is LT4 and Xn-1 is LT3 and Xn-2 is LT2 and Xn-3 is LT1 
	# # then Xn+1 is LT0
	# rule23 = np.fmin( matrixLevelsInputs[3][4], np.fmin( matrixLevelsInputs[2][3], np.fmin( matrixLevelsInputs[1][2], matrixLevelsInputs[0][1])))
	# activation_rule23 = np.fmin(rule23, matrixFuncMembershipsOutput[0][0])
	# # print "rule23: ", rule23
	# arrayLevelsActivation.append(activation_rule23)

	# # Rule 24 - 
	# # If Xn is LT and Xn-1 is LT3 and Xn-2 is LT1 and Xn-3 is LT0 
	# # then Xn+1 is LT0
	# rule24 = np.fmin( matrixLevelsInputs[3][1], np.fmin( matrixLevelsInputs[2][3], np.fmin( matrixLevelsInputs[1][1], matrixLevelsInputs[0][0])))
	# activation_rule24 = np.fmin(rule24, matrixFuncMembershipsOutput[0][0])
	# # print "rule24: ", rule24
	# arrayLevelsActivation.append(activation_rule24)

	return arrayLevelsActivation


# Se realiza la validacion de las reglas difusas
def validationRules(matrixLevelsInputs, matrixFuncMembershipsOutput, numRules):

	arrayLevelsActivation = []

	# numRules =  11

	for i in xrange(0, numRules):

		# Se toma el primer valor para ser comparado con el siguiente en el ciclo
		active_ruleAux = matrixLevelsInputs[0][i]

		# El ciclo inicia de la posicion 1 para no tomar en cuenta el de la posicion 0 ya que se tomo en la instruccion anterior
		for j in xrange(1, len(matrixLevelsInputs) ):
			# Se otiene el valor max compradando las posiciones actuales
			active_ruleAux2 = np.fmax(active_ruleAux, matrixLevelsInputs[j][i])	
			# print active_ruleAux2
			active_ruleAux = active_ruleAux2

		active_rule = active_ruleAux

		level_activation = np.fmin(active_rule, matrixFuncMembershipsOutput[0][i])

		arrayLevelsActivation.append(level_activation)

	return arrayLevelsActivation

# Aggregate all three output membership functions together - Se colorea el espacio difuso
def getAggregate(arrayLevelsActivation):

	active_ruleAux = arrayLevelsActivation[0]

	for i in xrange(1, len(arrayLevelsActivation)):		
		# Se otiene el valor max compradando las posiciones actuales
		active_ruleAux2 = np.fmax(active_ruleAux, arrayLevelsActivation[i])	
		active_ruleAux = active_ruleAux2

	aggregated = active_ruleAux

	# aggregated = np.fmax(arrayLevelsActivation[0],
	# np.fmax(arrayLevelsActivation[1], 
	# 	np.fmax( arrayLevelsActivation[2], 
	# 		np.fmax( arrayLevelsActivation[3] , 
	# 			np.fmax( arrayLevelsActivation[4] , 
	# 				np.fmax( arrayLevelsActivation[5], 
	# 					np.fmax( arrayLevelsActivation[6], 
	# 						np.fmax(arrayLevelsActivation[7], 
	# 							np.fmax(arrayLevelsActivation[8], 
	# 								np.fmax(arrayLevelsActivation[9], 
	# 									np.fmax(arrayLevelsActivation[10], arrayLevelsActivation[11])  )) ) ) ) )))  ))

	return aggregated

# # Calculate defuzzified result
def defuzzy(x_output, aggregated):

	outputFinal = fuzz.defuzz(x_output, aggregated, 'centroid')

	return outputFinal

# Graficamos los conjuntos difusos que intervienen en los valores de entrada
def graphFuzzySet1(x_output, output, arrayLevelsActivation, matrixFuncMembershipsOutput):
	# Visualize this - Se coloria los campos donde entra las reglas definidas
	fig, ax0 = plt.subplots(figsize=(15, 7))
	ax0.fill_between(x_output, output, arrayLevelsActivation[0], facecolor='b', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][0], 'b', linewidth=0.5, linestyle='--', )
	ax0.fill_between(x_output, output, arrayLevelsActivation[1], facecolor='g', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][1], 'g', linewidth=0.5, linestyle='--')
	ax0.fill_between(x_output, output, arrayLevelsActivation[2], facecolor='r', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][2], 'r', linewidth=0.5, linestyle='--')
	ax0.fill_between(x_output, output, arrayLevelsActivation[3], facecolor='b', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][3], 'b', linewidth=0.5, linestyle='--')
	ax0.fill_between(x_output, output, arrayLevelsActivation[4], facecolor='g', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][4], 'g', linewidth=0.5, linestyle='--')
	ax0.fill_between(x_output, output, arrayLevelsActivation[5], facecolor='r', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][5], 'r', linewidth=0.5, linestyle='--')
	ax0.fill_between(x_output, output, arrayLevelsActivation[6], facecolor='b', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][6], 'b', linewidth=0.5, linestyle='--')
	ax0.fill_between(x_output, output, arrayLevelsActivation[7], facecolor='g', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][7], 'g', linewidth=0.5, linestyle='--')
	ax0.fill_between(x_output, output, arrayLevelsActivation[8], facecolor='r', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][8], 'r', linewidth=0.5, linestyle='--')
	ax0.fill_between(x_output, output, arrayLevelsActivation[9], facecolor='b', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][9], 'b', linewidth=0.5, linestyle='--')
	ax0.fill_between(x_output, output, arrayLevelsActivation[10], facecolor='g', alpha=0.7)
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][10], 'g', linewidth=0.5, linestyle='--')
	ax0.set_title('Actividad de la salidad de membresia')

	# Turn off top/right axes
	for ax in (ax0,):
		ax.spines['top'].set_visible(False)
		ax.spines['right'].set_visible(False)
		ax.get_xaxis().tick_bottom()
		ax.get_yaxis().tick_left()

	plt.show()

# Graficamos el resultado de todas las reglas difusas activadas utlizando el metodo del centroide
def graphFuzzySetwithCentroid(x_output, output, outputFinal, output_activation, aggregated, matrixFuncMembershipsOutput):
	# Visualize this
	fig, ax0 = plt.subplots(figsize=(15, 7))
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][0], 'b', linewidth=0.5, linestyle='--', )
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][1], 'g', linewidth=0.5, linestyle='--')
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][2], 'r', linewidth=0.5, linestyle='--')
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][3], 'b', linewidth=0.5, linestyle='--')
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][4], 'g', linewidth=0.5, linestyle='--')
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][5], 'r', linewidth=0.5, linestyle='--')
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][6], 'b', linewidth=0.5, linestyle='--')
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][7], 'g', linewidth=0.5, linestyle='--')
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][8], 'r', linewidth=0.5, linestyle='--')
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][9], 'b', linewidth=0.5, linestyle='--')
	ax0.plot(x_output, matrixFuncMembershipsOutput[0][10], 'g', linewidth=0.5, linestyle='--')

	ax0.fill_between(x_output, output, aggregated, facecolor='Orange', alpha=0.7)
	ax0.plot([outputFinal, outputFinal], [0, output_activation], 'k', linewidth=1.5, alpha=0.9)
	ax0.set_title('Aggregated membership and result (line)')

	# Turn off top/right axes
	for ax in (ax0,):
		ax.spines['top'].set_visible(False)
		ax.spines['right'].set_visible(False)
		ax.get_xaxis().tick_bottom()
		ax.get_yaxis().tick_left()

	plt.show()

# Genera un archivo CSV
def exportToCSV(archivoCSV, tar, out):

	with open(archivoCSV, "wb") as ArchivoNuevoCSV:

	    writer = csv.writer(ArchivoNuevoCSV, lineterminator='\n')

	    writer.writerow(['Target', 'Output'])

	    for i in range(0, len(tar)):

	      target = str(tar[i])
	      output = str(out[i])
	      
	      aux1 = target.replace("[", "")
	      tarAux1 = aux1.replace("]", "")

	      aux2 = output.replace("[", "")
	      outAux2 = aux2.replace("]", "")

	      writer.writerow( (tarAux1, outAux2) )

def calculateMSE(targets, outputs):

    sum = 0

    for index, elemet in enumerate(targets):
        forecasting = outputs[index]
        sum = sum + ((forecasting - elemet)**2)

    return sum / len(targets)
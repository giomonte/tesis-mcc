import numpy as np
import copy
import math
import matplotlib.pyplot as plt
import csv

def obtenerArchivo(archivo):

    return funcionLeerArchivo( archivo )

def funcionLeerArchivo(archivo):

    datosCrudos = np.genfromtxt(("\t".join(i) for i in csv.reader(open(archivo))), delimiter="\t")

    return datosCrudos
# Experimentos de t+1
import simulacionRN
import csv
import matplotlib.pyplot as plt
import numpy as np
np.set_printoptions(threshold=np.inf)
import Helper2
import helperFuzzy3
import math

directorio = 'datosExperimentosMuestras/'

numeroMuestra =  3000

t = 1

datosArchivos = np.array([

                        ['wind_aristeomercado_10m_muestra',23,5,7]
                    ])

# Posicion del archivo a simular
x = 0

neuronasCentrada = int ( datosArchivos[x][1] )
neuronasCOculta = int  ( datosArchivos[x][2] )
algoritmoEntrenamiento = int ( datosArchivos[x][3] )

nb_archivo = datosArchivos[x][0]

rutaArchivo = directorio + nb_archivo+'.csv'

print "Generando pronostico para la serie : ", rutaArchivo

# Sin reconstruccion
archivo = Helper2.obtenerArchivo( rutaArchivo, numeroMuestra)

# Reconstruccion
# archivo = Helper2.funcionLeerArchivo2( rutaArchivo )

numeroValidacion = int(( len(archivo) * 0.20 ))

numeroTraining = numeroMuestra - numeroValidacion

archivo = archivo[numeroTraining :]

out, tar = simulacionRN.run( neuronasCentrada, neuronasCOculta, algoritmoEntrenamiento, nb_archivo, archivo )

mse = helperFuzzy3.calculateMSE(tar, out)

print "MSE: ", mse

archivoCSV = "pronosticoDifuso/pronosticoN_"+str(nb_archivo)+".csv"

helperFuzzy3.exportToCSV(archivoCSV, tar, out)
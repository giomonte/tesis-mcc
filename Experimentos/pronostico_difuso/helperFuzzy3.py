# helperFuzzy3
import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt
import csv
import copy

def obtenerArchivo(archivo, numeroDatos):

    return funcionLeerArchivo( archivo, numeroDatos)

def funcionLeerArchivo(archivo, numeroDatos):
    valorIncorrecto = -1
    datosCrudos = np.genfromtxt(archivo ,delimiter=',')

    #Tamano de datos lectura n datos
    datosCrudos =  datosCrudos[len(datosCrudos) - numeroDatos:]

    return normalizaDatos(datosCrudos, valorIncorrecto)
    
def normalizaDatos(datosCrudos, valorIncorrecto):

    datosCrudosSinValorIncorrecto = copy.copy(datosCrudos)

    datosCrudosSinValorIncorrecto = filter( lambda x: x != -1, datosCrudosSinValorIncorrecto )

    numeroMaximo = np.amax( datosCrudosSinValorIncorrecto )
    numeroMinimo = np.amin( datosCrudosSinValorIncorrecto )

    return map(lambda x : "NA" if x == -1 else (( x - numeroMinimo ) / ( numeroMaximo - numeroMinimo)), datosCrudos)

def formatInputTarget(datos, ventana):
    inputArray = []
    targetArray = []

    for i in range (0, ( len(datos) - ventana - 1)):

        input = datos[ i : i + ventana ]
        target = datos[ i + ventana ]

        if "NA" not in input and target != "NA":
            inputArray.append(input)
            targetArray.append(target)

    input = np.array(inputArray)
    inp = input.reshape(len(input), ventana)

    target = np.array(targetArray)
    lenT = len(target)
    tar = target.reshape(lenT, 1)

    return inp, tar

def obtenerVentanaDatos(archivo, numeroMuestra, ventana):

	numeroValidacion = int(( len(archivo) * 0.20 ))

	numeroTraining = numeroMuestra - numeroValidacion

	datosNormalizados = archivo

	#Del 70 porciento atenrior se toma el 70 porciento para datos de entrenamiento
	datosEntrenamiento = datosNormalizados[:numeroTraining]

	datosValidacion = datosNormalizados[numeroTraining:]

	inpEntrenamiento, tarEntrenamiento = formatInputTarget(datosEntrenamiento, ventana)

	inpValidacion, tarValidacion = formatInputTarget(datosValidacion, ventana)

	return inpEntrenamiento, tarEntrenamiento, inpValidacion, tarValidacion

# Generamos el universo de discuros para los valores de entrada y del valor de salida
def getInputs(ventana):

	arrayInputs = []
	x_output = np.arange(0.0, 1.01, 0.01)

	for i in xrange(0, ventana):

		x_input = np.arange(0.0, 1.01, 0.01)

		arrayInputs.append(x_input);

	return arrayInputs, x_output 

# Generamos los conjuntos difusos para cada valor de entrada y del valor de salida
def generateFuzzyMembershipFunctions(arrayInputs, x_output, numFuncMemb):

	# Se crea una matrix para obtener los conjuntos difusos de cada entrada
	matrixFuncMembershipsInputs = []
	# Se crea una matrix para obtener los conjuntos difusos de la salida
	matrixFuncMembershipsOutput = []
	# Se crea un array para obtener los conjuntos difusos de la salida esperada
	arrayMembershipsFuncionsOutput = []

	# Se define el tamanio de cada conjunto difuso
	inicio =  1.0 / ( numFuncMemb - 1 ) 

	# Se recorre el array de entradas
	for i in xrange(0, len(arrayInputs) ):

		# Creamos un arrray para guardar los conjuntos para cada entrada
		arrayMembershipsFuncionsInput = []

		# Se generan valores de los rangos que tendra cada conjunto difuso de los datos de entrada
		value1 = 0.0
		value2 = 0.0
		value3 = inicio

		for j in xrange(0, numFuncMemb):

			funcMember = fuzz.trimf(arrayInputs[i], [value1, value2, value3])

			arrayMembershipsFuncionsInput.append(funcMember)

			value1 = value2
			value2 = value3
			if( j < ( numFuncMemb - 2) ):
				value3 = value3 + inicio
			else:
				value3 = 1.01

		matrixFuncMembershipsInputs.append(arrayMembershipsFuncionsInput)	

	# Se generan los valores de los rangos que tranda cada conjunto difuso de la salida
	value1 = 0.0
	value2 = 0.0
	value3 = inicio

	# se recorre el array de la salida
	for i in xrange(0, numFuncMemb ):

		funcMember = fuzz.trimf(x_output, [value1, value2, value3])
		arrayMembershipsFuncionsOutput.append(funcMember)

		value1 = value2
		value2 = value3
		if( i < ( numFuncMemb - 2) ):
			value3 = value3 + inicio
		else:
			value3 = 1.01

    # Se guardan los conjuntos difusos de la salida en una matriz
	matrixFuncMembershipsOutput.append(arrayMembershipsFuncionsOutput)

	return 	matrixFuncMembershipsInputs, matrixFuncMembershipsOutput

# Obtener el grado de membresia para cada valor de ENTRADA en cada conjunto difuso
def getDegreeMemberships(arrayInputs, matrixFuncMembershipsInputs, arrayValueInputs):

	# Se crea una matrix para obtener el grado de membresia de los valores de entrada en cada conjunto difuso
	matrixLevelsInputs = []

	# Recorremos los el universo de discurso se cada valor de entrada
	for i in xrange(0, len(arrayInputs) ):

		arrayLevels = []

		# Recorremos cada conjunto difuso para evaluar el grado de membresia de los valores de entrada
		for j in xrange(0, len(matrixFuncMembershipsInputs[i]) ):

			# Se genera el grado de membresia que pertenese cada valor de entrada en cada conjunto difuso
			level = fuzz.interp_membership(arrayInputs[i], matrixFuncMembershipsInputs[i][j], arrayValueInputs[i])
			arrayLevels.append(level)

		matrixLevelsInputs.append(arrayLevels)

	return matrixLevelsInputs

# Obtener el grado de membresia para el valor de SALIDA en cada conjunto difuso
def getDegreeMembershipsOutput(x_output, matrixFuncMembershipsOutput, target):

	# Se crea una matrix para obtener el grado de membresia de los valores de entrada en cada conjunto difuso
	matrixLevelsOutput = []

	# Recorremos los el universo de discurso se cada valor de entrada
	arrayLevels = []

	# Recorremos cada conjunto difuso para evaluar el grado de membresia de los valores de entrada
	for i in xrange(0, len(matrixFuncMembershipsOutput[0]) ):

	# 	# Se genera el grado de membresia que pertenese cada valor de entrada en cada conjunto difuso
		level = fuzz.interp_membership(x_output, matrixFuncMembershipsOutput[0][i], target)
		arrayLevels.append(level)

	matrixLevelsOutput.append(arrayLevels)

	return matrixLevelsOutput

# Definicion de reglas difusas
def creationRules(conjuntosDifusos, matrixLevelsInputs, matrixLevelsOutput, Rules):

	# for i in xrange(0,len(matrixLevelsInputs)):
	# 	print matrixLevelsInputs[i]

	arrayLevelsActivation = []

	# Array de posiciones de los conjuntos difusos que son activados
	LTS = []

	# LTSMAX = []
	# LTSMIN = []

	for i in xrange(0, len(matrixLevelsInputs)):

		# Obtenermos el conjuto difuso donde impacta mas el valor
		LT = np.argmax(matrixLevelsInputs[i])

		# Obtenermos el conjuto difuso donde impacta menos el valor
		# LTMIN = 0
		# for j in xrange(0,len(matrixLevelsInputs[i])):
		# 	if( matrixLevelsInputs[i][j] > 0.0 and j != LTMAX ):
		# 		LTMIN = j
		
		# Agregamor el conjuto de mayor y de menor impcato a cada array
		LTS.append(LT)
		# LTSMIN.append(LTMIN)

	# LTS = LTSMAX

	# Obtenermos el conjunto difuso donde impacta mas el valor de salida
	if( len(matrixLevelsOutput) > 0 ):
		LT = np.argmax(matrixLevelsOutput[0])
		LTS.append(LT)
	# ------------------------------------------------------------------

	conjuntoIgual = False

	for i in xrange(0, len(Rules)):

		if( np.array_equal(Rules[i], LTS ) ):
			conjuntoIgual = True
			break

	if(not conjuntoIgual):
		Rules.append(LTS)

	# print LTS

	# LTS = LTSMIN

	# print LTS

	# conjuntoIgual = False

	# for i in xrange(0, len(Rules)):

	# 	if( np.array_equal(Rules[i], LTS ) ):
	# 		conjuntoIgual = True
	# 		break

	# if(not conjuntoIgual):
	# 	Rules.append(LTS)

	return Rules

# Impresion de las reglas difusas
def descriptionRules(numInputs, Rules):

	for i in xrange(0, len(Rules)):

		texto = "Regla: If "

		if( numInputs == 1 ):
			n = 1
		else:
			n = numInputs
		
		for j in xrange(0, ( len(Rules[i]) - 1 ) ):
		
			texto = texto +  "n-" +str(n) + " is LT" + str(Rules[i][j])

			if( len(Rules[i]) > 1 and j < ( len(Rules[i]) - 2 ) ):
				texto = texto + " and "

			n = n - 1

		texto = texto + " than n is LT"+str(Rules[i][( len(Rules[i]) - 1 )])

		print texto
		print ""

# Busqueda de la regla mas cercana
def kNNFuzzy(Rules, RulesValidation, k=5):

	# print ""
	# print "Function kNNFuzzy"

	RuleValidation = RulesValidation[0]

	# print "RuleValidation: ", RuleValidation

	# Sera una matriz donde se guardara el alcance de en cada regla y su posicion
	RuleskNN = []

	# print "Rules: ", Rules

	for i in xrange(0,len(Rules)):

		alcance = 0
		RulekNN = []

		for j in xrange(0, ( len(Rules[i]) - 1 ) ):
			
			# print Rules[i][j], " - ", RuleValidation[j]

			if( Rules[i][j] == RuleValidation[j] ):
				alcance = alcance + 1
		# print alcance
		# print ""

		# Guardamos la comparacion de cada parte de la regla y la posicion
		RulekNN.append(alcance)
		RulekNN.append(i)

		RuleskNN.append(RulekNN)

	# print "RuleskNN: ", RuleskNN

	# Se ordena de mayor a menor el array de las mejores k reglas mas parecidas
	RuleskNN.sort(reverse=True)

	RuleskNN = RuleskNN[:k]

	# ------------------------------------------------------------------
	# Como es un solo array de una posicion tomamos la posicion 2 de la posicion 0 para tomar la posicion en el banco de reglas
	Rule = Rules[RuleskNN[0][1]]
	# Asignamos el conjuto difuso del esperado
	CD = Rule[ ( len(Rule) - 1 )]
	# Lo agregamos a nuestra regla generada
	RuleValidation.append(CD)

	RulesGood = []
	RulesGood.append(RuleValidation)
	# -------------------------------------------------------------------

	# # print "RuleskNN: ", RuleskNN

	# # Obtenermos el numero de conjuntos que mas se repite 
	# Moda, arrayReglaskNN = calcularModaKNN(RuleskNN)

	# # print "MODA: ", Moda


	# # print ""
	# # print ""
	# # print ""
	# # print "-----------------------------------------"
	# # print "ArrayReglaskNN: ", arrayReglaskNN

	# RulesGood = []

	# for i in xrange(0,len(arrayReglaskNN)):
	# 	RuleGood = Rules[Moda]
	# 	RulesGood.append(RuleGood)

	# # print "RULESGOOD: ", RulesGood

	# RuleGood = Rules[Moda]

	# # print "RuleGood: ", RuleGood

	# CFs = []

	# for i in xrange(0,len(RulesGood)):
		
	# 	CF = RulesGood[i][len(RuleGood)-1]
	# 	# print "CF: ", CF
	# 	CFs.append(CF)

	# # print "CFs: ", CFs	


	# RulesGoodValidations = []

	# for i in xrange(0, len(RulesGood)):
	# 	RulesGoodValidations.append(RuleValidation)

	# # print "RulesGoodValidations: ", RulesGoodValidations

	# # 	RuleGoodValidation = RuleValidation
	# 	# RuleGoodValidation.append(CFs[i])

	# 	# RulesGoodValidations.append(RuleGoodValidation)

	# # print "RulesGoodValidations: ", RulesGoodValidations

	# # print "-----------------------------------------"
	# # print ""
	# # print ""
	# # print ""

	# # Obtener el conjunto difuso del tarjet (la clase)
	# CF = RuleGood[len(RuleGood)-1]

	# # print "CF: ", CF

	# # se agrega en la regla generada sin el tarjet para agregar el tarjet pronosticado
	# RulesValidation[0].append(CF)

	# # for i in xrange(0,len(RuleskNN)):
		
	# # 	RulesGood.append(Rules[RuleskNN[i][1]])

	return RulesGood

def calcularModaKNN(array):   

	# print "Function calcularModaKNN"	

	# print "1 - Array: ", array, ' - Tam: ', len(array)                                               
                                                                                           
	# moda    
	# Array auxiliar para guardar el numero de conjuntos que se repiten
	arrayAux = []
	for i in xrange(0,len(array)):
		arrayAux.append(array[i][0])

	# print "2 - ArrayAux: ", arrayAux, ' - Tam: ', len(arrayAux)     

	repeticiones = 0                                                                         
	for i in arrayAux:                                                                              
	    apariciones = arrayAux.count(i)                                                             
	    if apariciones > repeticiones:                                                       
	        repeticiones = apariciones                                                       
	                                                                                         
	modas = []                                                                               
	for i in arrayAux:                                                                              
	    apariciones = arrayAux.count(i)                                                             
	    if apariciones == repeticiones and i not in modas:                                   
	        modas.append(i)                                                                  
	                              
	# Si hay mas de una moda, tomamos la mediana de esas moda
	# print "Modas: ", modas, " - tam: ", len(modas)

	# if(len(modas) > 1):
	# 	mediana = calcularMediana(modas)
	# 	print "MEDIANA: ", mediana
	# else:
	
	# mediana = modas[0]

	moda = modas[0]

	# print "MODA: ", moda

	# print "array: ", array

	arrayAux2 = []
	for i in xrange(0,len(array)):

		# print moda, " - ", array[i][0]
		if(moda == array[i][0]):
			arrayAux2.append(array[i])

	# # Array auxiliar 3 para guardar la mediana de la posicion de la regla en la BD

	# print "arrayAux2: ", arrayAux2
	arrayAux3 = []
	for i in xrange(0,len(arrayAux2)):
		arrayAux3.append(arrayAux2[i][1])

	# Obtenemos la mediana de las reglas de la moda
	# print "arrayAux3: ", arrayAux3 

	arrayReglaskNN = arrayAux3
	# mediana = calcularMediana(arrayAux3)

	# print "MEDIANA: ", mediana

	# regresamos la posicion de la regla que tomaremos
	return moda, arrayReglaskNN                                                                     

def calcularMediana(array):  
  
	# print "Function calcularMediana"     
	# print array                                        
                                                                                                                                                                                                                                              
	# mediana                                                                                
	array.sort()  

	# print "2 - Array: ", array, " - Tam: ", len(array)                                                                                        
	                                                                                         
	if len(array) % 2 == 0:                                                                      
	    n = len(array)     


	    print (array[n/2-1]+ array[n/2] )
	    
	    mediana = (array[n/2-1]+ array[n/2] )/2   

	    # print "MMMEDIANA: ", mediana                                                   
	else:                                                                                    
	    mediana =array[len(array)/2]
	    # print "MMMEDIANA: ", mediana                                                                 
	                         
	return mediana

# Se realiza la validacion de las reglas difusas
def validationRules(matrixLevelsInputs, matrixFuncMembershipsOutput, Rules, numDatosEntrada):

	# print "Rules: ", Rules
	# print ""
	# print "matrixLevelsInputs: ", matrixLevelsInputs
	# print ""
	# print "matrixFuncMembershipsOutput: ", matrixFuncMembershipsOutput
	# print ""

	arrayLevelsActivation = []

	for i in xrange(0, len(Rules)):

		# print "Rules: ", Rules[i][0]
		# print ""

		active_ruleAux = matrixLevelsInputs[0][(Rules[i][0])]

		# print "posicion: ", 0, " - ", Rules[i][0]

		# print "active_ruleAux: ", active_ruleAux
		# print ""

		posicion = 1

		for j in xrange(1, ( len( Rules[i]) - 1) ):

			# print "j: ", j

			# print "posicion: ", posicion, " - ", Rules[i][j]

			active_ruleAux2 = np.fmin(active_ruleAux, matrixLevelsInputs[posicion][(Rules[i][j])])	

			# print "active_ruleAux2: ", active_ruleAux2

			active_ruleAux = active_ruleAux2

			# print "active_ruleAux: ", active_ruleAux

			posicion = posicion + 1

			# print "posicion: ", posicion

		rule = active_ruleAux

		# print "rule: ", rule

		# print "posicion: ", 0, " - ", (Rules[i][ ( len(Rules[i]) - 1) ])

		activation_rule = np.fmin(rule, matrixFuncMembershipsOutput[0][(Rules[i][ ( len(Rules[i]) - 1) ])])

		# print "activation_rule: ", activation_rule
		arrayLevelsActivation.append(activation_rule)

	return arrayLevelsActivation

# Aggregate all three output membership functions together - Se colorea el espacio difuso
def getAggregate(arrayLevelsActivation):

	active_ruleAux = arrayLevelsActivation[0]

	for i in xrange(1, len(arrayLevelsActivation)):		
		# Se otiene el valor max compradando las posiciones actuales
		active_ruleAux2 = np.fmax(active_ruleAux, arrayLevelsActivation[i])	
		active_ruleAux = active_ruleAux2

	aggregated = active_ruleAux

	return aggregated

# # Calculate defuzzified result
def defuzzy(x_output, aggregated):

	outputFinal = fuzz.defuzz(x_output, aggregated, 'centroid')

	return outputFinal

# Genera un archivo CSV
def exportToCSV(archivoCSV, tar, out):

	with open(archivoCSV, "wb") as ArchivoNuevoCSV:

	    writer = csv.writer(ArchivoNuevoCSV, lineterminator='\n')

	    writer.writerow(['Target', 'Output'])

	    for i in range(0, len(tar)):

	      target = str(tar[i])
	      output = str(out[i])
	      
	      aux1 = target.replace("[", "")
	      tarAux1 = aux1.replace("]", "")

	      aux2 = output.replace("[", "")
	      outAux2 = aux2.replace("]", "")

	      writer.writerow( (tarAux1, outAux2) )

def calculateMSE(targets, outputs):

    sum = 0

    for index, elemet in enumerate(targets):
        forecasting = outputs[index]
        sum = sum + ((forecasting - elemet)**2)

    return sum / len(targets)
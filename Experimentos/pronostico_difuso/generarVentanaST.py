# Experimentos de t+1
import csv
import matplotlib.pyplot as plt
import numpy as np
import math
import Helper2

print "T1"

directorio = 'datosExperimentosMuestras/'

numeroMuestra =  3000

t = 1

datosArchivos = np.array([

                        ['wind_aristeomercado_10m_muestra',5]
                    ])

# Posicion del archivo a simular
x = 0

ventana = int ( datosArchivos[x][1] )

nb_archivo = datosArchivos[x][0]

rutaArchivo = directorio + nb_archivo+'.csv'

# Sin reconstruccion
archivo = Helper2.obtenerArchivo( rutaArchivo, numeroMuestra)

numeroValidacion = int(( len(archivo) * 0.20 ))

numeroTraining = numeroMuestra - numeroValidacion

datosNormalizados = archivo

#Del 70 porciento atenrior se toma el 70 porciento para datos de entrenamiento
datosEntrenamiento = datosNormalizados[:numeroTraining]

# print datosEntrenamiento

datosValidacion = datosNormalizados[numeroTraining:]

inpEntrenamiento, tarEntrenamiento = Helper2.formatInputTarget(datosEntrenamiento, ventana)

inpValidacion, tarValidacion = Helper2.formatInputTarget(datosValidacion, ventana)

print 'inpEntrenamiento: ', inpEntrenamiento
print 'tarEntrenamiento: ', tarEntrenamiento

# Crear archivo .csv
archivoCSV = "ventanaDatos/ventanaTraining_"+str(nb_archivo)+"_ventana_"+str(ventana - 1)+".csv"

with open(archivoCSV, "wb") as ArchivoNuevoCSV:

    writer = csv.writer(ArchivoNuevoCSV, lineterminator='\n')

    # writer.writerow(['Target', 'Output'])

    for i in range(0, len(inpEntrenamiento)):

      vent1 = str(inpEntrenamiento[i][0])
      vent2 = str(inpEntrenamiento[i][1])
      vent3 = str(inpEntrenamiento[i][2])
      vent4 = str(inpEntrenamiento[i][3])
      vent5 = str(inpEntrenamiento[i][4])
      # vent6 = str(inpEntrenamiento[i][5])
      
      aux1 = vent1.replace("[", "")
      ventAux1 = aux1.replace("]", "")

      aux2 = vent2.replace("[", "")
      ventAux2 = aux2.replace("]", "")

      aux3 = vent3.replace("[", "")
      ventAux3 = aux3.replace("]", "")

      aux4 = vent4.replace("[", "")
      ventAux4 = aux4.replace("]", "")

      aux5 = vent5.replace("[", "")
      ventAux5 = aux5.replace("]", "")

      # aux6 = vent6.replace("[", "")
      # ventAux6 = aux6.replace("]", "")

      writer.writerow( (ventAux1, ventAux2, ventAux3, ventAux4, ventAux5) )

# Crear archivo .csv
archivoCSV = "ventanaDatos/ventanaValidation_"+str(nb_archivo)+"_ventana_"+str(ventana - 1 )+".csv"

with open(archivoCSV, "wb") as ArchivoNuevoCSV:

    writer = csv.writer(ArchivoNuevoCSV, lineterminator='\n')

    # writer.writerow(['Target', 'Output'])

    for i in range(0, len(inpValidacion)):

      vent1 = str(inpValidacion[i][0])
      vent2 = str(inpValidacion[i][1])
      vent3 = str(inpValidacion[i][2])
      vent4 = str(inpValidacion[i][3])
      vent5 = str(inpValidacion[i][4])
      # vent6 = str(inpValidacion[i][5])

      aux1 = vent1.replace("[", "")
      ventAux1 = aux1.replace("]", "")

      aux2 = vent2.replace("[", "")
      ventAux2 = aux2.replace("]", "")

      aux3 = vent3.replace("[", "")
      ventAux3 = aux3.replace("]", "")

      aux4 = vent4.replace("[", "")
      ventAux4 = aux4.replace("]", "")

      # aux5 = vent5.replace("[", "")
      # ventAux5 = aux5.replace("]", "")

      # aux6 = vent6.replace("[", "")
      # ventAux6 = aux6.replace("]", "")

      writer.writerow( (ventAux1, ventAux2, ventAux3, ventAux4, ventAux5) )
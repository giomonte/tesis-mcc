import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt

# Generate universe variables
# * Quality and service on subjective ranges [0, 10]
# * Tip has a range of [0, 25] in units of percentage points

x_qual = np.arange(0, 11, 1)
x_serv = np.arange(0, 11, 1)
x_tip = np.arange(0, 26, 1)

# Generate fuzzy membership functions
qual_vlo = fuzz.trimf(x_qual, [0, 0, 2]) #muy malo
qual_lo = fuzz.trimf(x_qual, [0, 2, 4]) #malo
qual_md = fuzz.trimf(x_qual, [2, 4, 6]) #regular
qual_hi = fuzz.trimf(x_qual, [4, 6, 8]) #bueno
qual_vhi = fuzz.trimf(x_qual, [6, 8, 10]) #muy bueno
qual_vvhi = fuzz.trimf(x_qual, [8, 10, 10]) #excelente

serv_vlo = fuzz.trimf(x_serv, [0, 0, 2]) #muy malo
serv_lo = fuzz.trimf(x_serv, [0, 2, 4]) #malo
serv_md = fuzz.trimf(x_serv, [2, 4, 6]) #regular
serv_hi = fuzz.trimf(x_serv, [4, 6, 8]) #bueno
serv_vhi = fuzz.trimf(x_serv, [6, 8, 10]) #muy bueno
serv_vvhi = fuzz.trimf(x_serv, [8, 10, 10]) #excelente

tip_vlo = fuzz.trimf(x_tip, [0, 0, 5])
tip_lo = fuzz.trimf(x_tip, [0, 5, 10])
tip_md = fuzz.trimf(x_tip, [5, 10, 15])
tip_hi = fuzz.trimf(x_tip, [10, 15, 20])
tip_vhi = fuzz.trimf(x_tip, [15, 20, 25])
tip_vvhi = fuzz.trimf(x_tip, [20, 25, 25])

# Visualize these universes and membership functions
fig, (ax0, ax1, ax2) = plt.subplots(nrows=3, figsize=(15, 9))

ax0.plot(x_qual, qual_vlo, 'b', linewidth=1.5, label='Muy Malo')
ax0.plot(x_qual, qual_lo, 'g', linewidth=1.5, label='Malo')
ax0.plot(x_qual, qual_md, 'r', linewidth=1.5, label='Regular')
ax0.plot(x_qual, qual_hi, 'k', linewidth=1.5, label='Bueno')
ax0.plot(x_qual, qual_vhi, 'm', linewidth=1.5, label='Muy Bueno')
ax0.plot(x_qual, qual_vvhi, 'b', linewidth=1.5, label='Excelente')
ax0.set_title('Calidad de la Comida')
ax0.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)


ax1.plot(x_serv, serv_vlo, 'b', linewidth=1.5, label='Muy Malo')
ax1.plot(x_serv, serv_lo, 'g', linewidth=1.5, label='Malo')
ax1.plot(x_serv, serv_md, 'r', linewidth=1.5, label='Regular')
ax1.plot(x_serv, serv_hi, 'k', linewidth=1.5, label='Bueno')
ax1.plot(x_serv, serv_vhi, 'm', linewidth=1.5, label='Muy Bueno')
ax1.plot(x_serv, serv_vvhi, 'b', linewidth=1.5, label='Excelente')
ax1.set_title('Calidad del Servicio')
ax1.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)


ax2.plot(x_tip, tip_vlo, 'b', linewidth=1.5, label='Muy Baja')
ax2.plot(x_tip, tip_lo, 'g', linewidth=1.5, label='Baja')
ax2.plot(x_tip, tip_md, 'r', linewidth=1.5, label='Regular')
ax2.plot(x_tip, tip_hi, 'k', linewidth=1.5, label='Alta')
ax2.plot(x_tip, tip_vhi, 'm', linewidth=1.5, label='Muy Alta')
ax2.plot(x_tip, tip_vvhi, 'b', linewidth=1.5, label='Muy Muy Alta')
ax2.set_title('Porcentaje de Propina')
ax2.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)

# Turn off top/right axes
for ax in (ax0, ax1, ax2):
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.get_xaxis().tick_bottom()
	ax.get_yaxis().tick_left()

plt.show()

# We need the activation of our fuzzy membership functions at these values.
# The exact values 6.5 and 9.8 do not exist on our universes...
# This is what fuzz.interp_membership exists for!

valueFood = 3.5
valueService = 9.5

qual_level_vlo = fuzz.interp_membership(x_qual, qual_vlo, valueFood)
qual_level_lo = fuzz.interp_membership(x_qual, qual_lo, valueFood)
qual_level_md = fuzz.interp_membership(x_qual, qual_md, valueFood)
qual_level_hi = fuzz.interp_membership(x_qual, qual_hi, valueFood)
qual_level_vhi = fuzz.interp_membership(x_qual, qual_vhi, valueFood)
qual_level_vvhi = fuzz.interp_membership(x_qual, qual_vvhi, valueFood)

serv_level_vlo = fuzz.interp_membership(x_serv, serv_vlo, valueService)
serv_level_lo = fuzz.interp_membership(x_serv, serv_lo, valueService)
serv_level_md = fuzz.interp_membership(x_serv, serv_md, valueService)
serv_level_hi = fuzz.interp_membership(x_serv, serv_hi, valueService)
serv_level_vhi = fuzz.interp_membership(x_serv, serv_vhi, valueService)
serv_level_vvhi = fuzz.interp_membership(x_serv, serv_vvhi, valueService)


# REGLAS DIFUSAS

# Now we take our rules and apply them. 
# Rule 1 concerns bad food OR service.
# Rule 1 if the food is bad OR the service is bad
# The OR operator means we take the maximum of these two.
active_rule1 = np.fmax(qual_level_lo, serv_level_lo)
# Now we apply this by clipping the top off the corresponding output
# membership function with `np.fmin`
tip_activation_lo = np.fmin(active_rule1, tip_lo) # removed entirely to 0
# For rule 2 we connect acceptable service to medium tipping
# Rule 2 if the services is medium OR the food is medium
# active_rule2 = np.fmax(qual_level_md, serv_level_md)
# print 'active_rule2: ', active_rule2
tip_activation_md = np.fmin(serv_level_md, tip_md)
# For rule 3 we connect high service OR high food with high tipping
# Rule 3 if the service is high OR the food is high then high tipping
active_rule3 = np.fmax(qual_level_hi, serv_level_hi)
tip_activation_hi = np.fmin(active_rule3, tip_hi)

# # if service is excellent THEN tip is very very high
tip_activation_4 = np.fmin(serv_level_vvhi, tip_vvhi) 

# if the food is very bad AND the service is very bad
active_rule5 = np.fmin(qual_level_vlo, serv_level_vlo)
tip_activation_5 = np.fmin(active_rule5, tip_vlo)

# if the service is very high THEN tip is very high
tip_activation_6 = np.fmin(serv_level_vhi, tip_vhi)


# if the food is medioum AND the service is very very high THEN tip is high
active_rule7 = np.fmin(qual_level_md, serv_level_vvhi)
tip_activation_7 = np.fmin(active_rule5, tip_hi)

tip0 = np.zeros_like(x_tip)

# Visualize this
fig, ax0 = plt.subplots(figsize=(8, 3))

ax0.fill_between(x_tip, tip0, tip_activation_lo, facecolor='g', alpha=0.7)

ax0.fill_between(x_tip, tip0, tip_activation_md, facecolor='r', alpha=0.7)

ax0.fill_between(x_tip, tip0, tip_activation_hi, facecolor='r', alpha=0.7)
ax0.fill_between(x_tip, tip0, tip_activation_4, facecolor='r', alpha=0.7)
ax0.fill_between(x_tip, tip0, tip_activation_5, facecolor='r', alpha=0.7)
ax0.fill_between(x_tip, tip0, tip_activation_6, facecolor='r', alpha=0.7)
ax0.fill_between(x_tip, tip0, tip_activation_7, facecolor='r', alpha=0.7)


ax0.plot(x_tip, tip_vlo, 'b', linewidth=0.5, linestyle='--', )
ax0.plot(x_tip, tip_lo, 'g', linewidth=0.5, linestyle='--')
ax0.plot(x_tip, tip_md, 'r', linewidth=0.5, linestyle='--')
ax0.plot(x_tip, tip_hi, 'k', linewidth=0.5, linestyle='--')
ax0.plot(x_tip, tip_vhi, 'm', linewidth=0.5, linestyle='--')
ax0.plot(x_tip, tip_vvhi, 'b', linewidth=0.5, linestyle='--')
ax0.set_title('Actividad de la salidad de membresia')

# Turn off top/right axes
for ax in (ax0,):
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.get_xaxis().tick_bottom()
	ax.get_yaxis().tick_left()

plt.show()

# Aggregate all three output membership functions together - Se colorea el espacio difuso
aggregated = np.fmax(tip_activation_lo,
np.fmax(tip_activation_md, 
np.fmax(tip_activation_hi, 
np.fmax(tip_activation_4, 
np.fmax(tip_activation_5, 
np.fmax(tip_activation_6, tip_activation_7 )) ))))

print 'aggregated: ', aggregated

# Calculate defuzzified result
tip = fuzz.defuzz(x_tip, aggregated, 'centroid')

print 'tip: ', tip

tip_activation = fuzz.interp_membership(x_tip, aggregated, tip) # for plot

print 'tip_activation: ', tip_activation

# Visualize this
fig, ax0 = plt.subplots(figsize=(8, 3))
ax0.plot(x_tip, tip_vlo, 'b', linewidth=0.5, linestyle='--', )
ax0.plot(x_tip, tip_lo, 'g', linewidth=0.5, linestyle='--')
ax0.plot(x_tip, tip_md, 'r', linewidth=0.5, linestyle='--')
ax0.plot(x_tip, tip_hi, 'k', linewidth=0.5, linestyle='--', )
ax0.plot(x_tip, tip_vhi, 'm', linewidth=0.5, linestyle='--')
ax0.plot(x_tip, tip_vvhi, 'b', linewidth=0.5, linestyle='--')


ax0.fill_between(x_tip, tip0, aggregated, facecolor='Orange', alpha=0.7)
ax0.plot([tip, tip], [0, tip_activation], 'k', linewidth=1.5, alpha=0.9)
ax0.set_title('Aggregated membership and result (line)')

# Turn off top/right axes
for ax in (ax0,):
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.get_xaxis().tick_bottom()
	ax.get_yaxis().tick_left()

plt.show()
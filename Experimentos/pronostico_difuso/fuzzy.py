# Experimentos pronostico de serie de tiempo con logica difusa 4.0
import csv
import matplotlib.pyplot as plt
import numpy as np
import math
import helperFuzzy3

def forecastingFuzzy(archivo, numeroMuestra, ventana, conjuntosDifusos, k):

	print "Tamanio de ventana: ", ventana
	print "Conjutos Difusos: ", conjuntosDifusos
	print "Valor de K: ", k
	
	arrayVentana = []
	arrayMSE = []

	numDatosEntrada = ventana

	# Se obtienen las ventanas de los datos de entrada y su targen, del entranamiento y de la validacion
	inpEntrenamiento, tarEntrenamiento, inpValidacion, tarValidacion = helperFuzzy3.obtenerVentanaDatos(archivo, numeroMuestra, ventana )

	# print "inpEntrenamiento: ", inpEntrenamiento
	# print "tamanio inpEntrenamiento: ", len(inpEntrenamiento)
	# print "tarEntrenamiento: ", tarEntrenamiento
	# print "tamanio tarEntrenamiento: ", len(tarEntrenamiento)
	# print "Tamanio de ventana: ", ventana
	# print ""

	arrayOutputs = []
	arrayTargets = []
	Rules = []

	# -------------------------------------- TRAINING ---------------------------------------------
	# Proceso de entrenamiento con el 80% porciento de los datos
	for i in xrange(0, len(inpEntrenamiento)):
	# for i in xrange(0, 10):
		pass
		# Generamos el array de los valores que tendra cada entrada
		arrayValueInputs = []

		# Se guardan las entradas de los datos, el ultimo valor viene siendo el target ( la salida real ) para ser comparada con el output resultante
		for j in xrange(0, len(inpEntrenamiento[i])  ):
			arrayValueInputs.append(inpEntrenamiento[i][j])

		# target = inpEntrenamiento[i][len(inpEntrenamiento[i]) - 1]
		target = tarEntrenamiento[i][0]

		# print ""
		# print "arrayValueInputs: ", arrayValueInputs
		# print "target: ", target
		# # print "target2: ", target2
		# print ""

		# obtenemos el rango de valores del universo del discuros para el numero de entradas de acuerdo al tamanio de ventana
		arrayInputs, x_output = helperFuzzy3.getInputs( ventana )

		# print "arrayInputs: ", arrayInputs
		# print ""
		# print "x_output: ", x_output

		# Generate fuzzy membership functions
		matrixFuncMembershipsInputs, matrixFuncMembershipsOutput = helperFuzzy3.generateFuzzyMembershipFunctions(arrayInputs, x_output, conjuntosDifusos)

		# for i in xrange(0,len(matrixFuncMembershipsInputs)):
		# 	print "matrixFuncMembershipsInputs: ", matrixFuncMembershipsInputs[i]
		# 	print ""
		# print ""
		# print ""
		# print "matrixFuncMembershipsOutput: ", matrixFuncMembershipsOutput
		
		# We need the activation of our fuzzy membership functions at these values.
		# Obtenemos el grado de membresia para cada entrada en cada conjunto difuso
		matrixLevelsInputs = helperFuzzy3.getDegreeMemberships(arrayInputs, matrixFuncMembershipsInputs, arrayValueInputs)

		# for i in xrange(0,len(matrixLevelsInputs)):
		# 	print "matrixLevelsInputs: ", matrixLevelsInputs[i]
		# 	print ""
		# print ""

		# Obtenemos el grado de membresia para la salida en cada conjunto difuso
		matrixLevelsOutput = helperFuzzy3.getDegreeMembershipsOutput(x_output, matrixFuncMembershipsOutput, target)

		# for i in xrange(0,len(matrixLevelsOutput)):
		# 	print "matrixLevelsOutput: ", matrixLevelsOutput[i]
		# 	print ""
		# print ""

		# Validacion de las Reglas Difusas
		Rules = helperFuzzy3.creationRules(conjuntosDifusos, matrixLevelsInputs, matrixLevelsOutput, Rules)

	# Se imprime la descripcion de las reglas difusas creadas
	# print "Reglas creadas por el training:"
	# print ""
	# print "Numero de Reglas: ", len(Rules)
	# print ""
	# print "Reglas: "

	# for x in xrange(0,len(Rules)):
	# 	print Rules[x]
	# print ""
	# helperFuzzy3.descriptionRules( numDatosEntrada, Rules)
	# print "Total de Reglas Difusas Creadas: ", len(Rules)

	if( len(Rules) < k ):
		k = len(Rules)

	# -------------------------------------- VALIDATION -----------------------------------------
	# Proceso de validacion con el 20% de los datos
	for i in xrange(0, len(inpValidacion)):	
	# for i in xrange(0, 1):
		# Generamos el array de los valores que tendra cada entrada
		arrayValueInputs = []
		
		# Se guardan las entradas de los datos, el ultimo valor viene siendo el target ( la salida real ) para ser comparada con el output resultante
		for j in xrange(0, len(inpValidacion[i]) ):
			arrayValueInputs.append(inpValidacion[i][j])

		# target = inpValidacion[i][len(inpValidacion[i]) - 1]
		target = tarValidacion[i][0]

		# print ""
		# print "arrayValueInputs: ", arrayValueInputs
		# print "target: ", target
		# print ""

		# obtenemos el rango de valores del universo del discuros para el numero de entradas de acuerdo al tamanio de ventana
		arrayInputs, x_output = helperFuzzy3.getInputs( ventana )

		# Generate fuzzy membership functions
		matrixFuncMembershipsInputs, matrixFuncMembershipsOutput = helperFuzzy3.generateFuzzyMembershipFunctions(arrayInputs, x_output, conjuntosDifusos)

		# We need the activation of our fuzzy membership functions at these values.
		# Obtenemos el grado de membresia para cada entrada en cada conjunto difuso
		matrixLevelsInputs = helperFuzzy3.getDegreeMemberships(arrayInputs, matrixFuncMembershipsInputs, arrayValueInputs)

		# Obtenemos el grado de membresia para la salida en cada conjunto difuso
		# matrixLevelsOutput = helperFuzzy3.getDegreeMembershipsOutput(x_output, matrixFuncMembershipsOutput, target)
		matrixLevelsOutput = []

		# ------------------------SECCION DONDE SE APLICA EL k-NN ---------------------
		# Validacion de las Reglas Difusas Se crean las reglas para valiar las cercanas
		RulesValidation = []
		RulesValidation = helperFuzzy3.creationRules(conjuntosDifusos, matrixLevelsInputs, matrixLevelsOutput, RulesValidation)

		# print "RulesValidation: "
		# print RulesValidation[0]
		

		# Buscar las reglas mas cercana con respecto a la regla creada con los datos
		RulesGood = helperFuzzy3.kNNFuzzy(Rules, RulesValidation, k)

		# print RulesGood

		# print "Regla formada de la validacion: "
		# helperFuzzy3.descriptionRules( numDatosEntrada, RulesGood)

		# -----------------------------------------------------------------------------

		# Validacion de las reglas difusas
		arrayLevelsActivation = helperFuzzy3.validationRules(matrixLevelsInputs, matrixFuncMembershipsOutput, RulesGood, numDatosEntrada)

		output = np.zeros_like(x_output)
		
		aggregated = helperFuzzy3.getAggregate(arrayLevelsActivation)

		# print aggregated

		if( np.sum(aggregated) == 0.0 ):
			outputFinal = 0.0
		else:
			# Se obtiene el valor de salida esperado ESTE ES EL OUTPUT BUENO
			outputFinal = helperFuzzy3.defuzzy(x_output, aggregated)

		# print "target: ", target
		# print "outputFinal: ", outputFinal
		# print ""

		arrayTargets.append(target)
		arrayOutputs.append(outputFinal)

	# -------------------------------------------------------------------------------------------

	# print "Numero de datos de entrenamiento: ", len(inpEntrenamiento)
	# print "Numero de datos de validacion: ", len(inpValidacion)
	# print "Datos de entrada: ", numDatosEntrada
	# print "Conjutos Difusos: ", conjuntosDifusos

	targets = np.array(arrayTargets)
	outputs = np.array(arrayOutputs)

	mse = helperFuzzy3.calculateMSE(targets, outputs)

	print "MSE: ", mse

	return mse
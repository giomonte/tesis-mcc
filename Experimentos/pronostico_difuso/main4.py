# Experimentos pronostico de serie de tiempo con logica difusa 4.0
import csv
import matplotlib.pyplot as plt
import numpy as np
import math
import helperFuzzy3

directorio = 'datosExperimentosMuestras/'

numeroMuestra =  3000

t = 1

datosArchivos = np.array([

                        ['wind_aristeomercado_10m_muestra']
                    ])

# Posicion del archivo a simular
x = 0

# numero de ventanas a generar desde 2 a 10
ventanas = 4

nb_archivo = datosArchivos[x][0]

rutaArchivo = directorio + nb_archivo+'.csv'

archivo = helperFuzzy3.obtenerArchivo( rutaArchivo, numeroMuestra)

arrayVentana = []
arrayMSE = []

ventana = 4

for l in xrange(3, ventanas):

	numDatosEntrada = ventana 

	# Se obtienen las ventanas de los datos de entrada y su targen, del entranamiento y de la validacion
	inpEntrenamiento, tarEntrenamiento, inpValidacion, tarValidacion = helperFuzzy3.obtenerVentanaDatos(archivo, numeroMuestra, ventana )

	print "inpEntrenamiento: ", inpEntrenamiento
	print "tamanio inpEntrenamiento: ", len(inpEntrenamiento)
	print "tarEntrenamiento: ", tarEntrenamiento
	print "tamanio tarEntrenamiento: ", len(tarEntrenamiento)
	print "Tamanio de ventana: ", ventana

	arrayOutputs = []
	arrayTargets = []
	Rules = []

	# -------------------------------------- TRAINING ---------------------------------------------
	# print "TRAINING: "
	# Proceso de entrenamiento con el 80% porciento de los datos
	for i in xrange(0, len(inpEntrenamiento)):
	# for i in xrange(0, 10):
		# Generamos el array de los valores que tendra cada entrada
		arrayValueInputs = []

		# Se guardan las entradas de los datos, el ultimo valor viene siendo el target ( la salida real ) para ser comparada con el output resultante
		for j in xrange(0, len(inpEntrenamiento[i])  ):
			arrayValueInputs.append(inpEntrenamiento[i][j])

		# target = inpEntrenamiento[i][len(inpEntrenamiento[i]) - 1]

		target = tarEntrenamiento[i][0]

		# arrayValueInputs.append(target)

		# print ""
		# print "arrayValueInputs: ", arrayValueInputs
		# print "target: ", target
		# # print "target2: ", target2
		# print ""

		# obtenemos el rango de valores del universo del discuros para el numero de entradas de acuerdo al tamanio de ventana
		arrayInputs, x_output = helperFuzzy3.getInputs( ventana )

		# print "arrayInputs: ", arrayInputs
		# print ""
		# print "x_output: ", x_output

		# Generate fuzzy membership functions - SE DEFINEN LOS CONJUNTOS DIFUSOS
		conjuntosDifusos = 3
		matrixFuncMembershipsInputs, matrixFuncMembershipsOutput = helperFuzzy3.generateFuzzyMembershipFunctions(arrayInputs, x_output, conjuntosDifusos)

		# for i in xrange(0,len(matrixFuncMembershipsInputs)):
		# 	print "matrixFuncMembershipsInputs: ", matrixFuncMembershipsInputs[i]
		# 	print ""
		# print ""
		# print ""
		# print "matrixFuncMembershipsOutput: ", matrixFuncMembershipsOutput
		# We need the activation of our fuzzy membership functions at these values.
		# Obtenemos el grado de membresia para cada entrada en cada conjunto difuso
		matrixLevelsInputs = helperFuzzy3.getDegreeMemberships(arrayInputs, matrixFuncMembershipsInputs, arrayValueInputs)

		# for i in xrange(0,len(matrixLevelsInputs)):
		# 	print "matrixLevelsInputs: ", matrixLevelsInputs[i]
		# 	print ""
		# print ""

		# Obtenemos el grado de membresia para la salida en cada conjunto difuso
		matrixLevelsOutput = helperFuzzy3.getDegreeMembershipsOutput(x_output, matrixFuncMembershipsOutput, target)

		# for i in xrange(0,len(matrixLevelsOutput)):
		# 	print "matrixLevelsOutput: ", matrixLevelsOutput[i]
		# 	print ""
		# print ""

		# Validacion de las Reglas Difusas
		Rules = helperFuzzy3.creationRules(conjuntosDifusos, matrixLevelsInputs, matrixLevelsOutput, Rules)

	# print ""
	# Se imprime la descripcion de las reglas difusas creadas
	# helperFuzzy3.descriptionRules( numDatosEntrada, Rules)

	k = 3991

	if( len(Rules) < k ):
		k = len(Rules)

	# -------------------------------------- VALIDATION -----------------------------------------
	# print "VALIDATION: "
	# Proceso de validacion con el 20% de los datos
	for i in xrange(0, len(inpValidacion)):	
	# for i in xrange(0, 1):
		# Generamos el array de los valores que tendra cada entrada
		arrayValueInputs = []
		
		# Se guardan las entradas de los datos, el ultimo valor viene siendo el target ( la salida real ) para ser comparada con el output resultante
		for j in xrange(0, len(inpValidacion[i]) ):
			arrayValueInputs.append(inpValidacion[i][j])

		# target = inpValidacion[i][len(inpValidacion[i]) - 1]

		target = tarValidacion[i][0]

		# print ""
		# print "arrayValueInputs: ", arrayValueInputs
		# print "target: ", target
		# print ""

		# obtenemos el rango de valores del universo del discuros para el numero de entradas de acuerdo al tamanio de ventana
		arrayInputs, x_output = helperFuzzy3.getInputs( ventana )

		# Generate fuzzy membership functions - SE DEFINEN LOS CONJUNTOS DIFUSOS
		conjuntosDifusos = 3
		matrixFuncMembershipsInputs, matrixFuncMembershipsOutput = helperFuzzy3.generateFuzzyMembershipFunctions(arrayInputs, x_output, conjuntosDifusos)

		# We need the activation of our fuzzy membership functions at these values.
		# Obtenemos el grado de membresia para cada entrada en cada conjunto difuso
		matrixLevelsInputs = helperFuzzy3.getDegreeMemberships(arrayInputs, matrixFuncMembershipsInputs, arrayValueInputs)

		# Obtenemos el grado de membresia para la salida en cada conjunto difuso
		matrixLevelsOutput = helperFuzzy3.getDegreeMembershipsOutput(x_output, matrixFuncMembershipsOutput, target)

		# for i in xrange(0,len(matrixLevelsInputs)):
		# 	print "matrixLevelsInputs: ", matrixLevelsInputs[i]
		# 	print ""
		# print ""
		# for i in xrange(0,len(matrixLevelsOutput)):
		# 	print "matrixLevelsOutput: ", matrixLevelsOutput[i]
		# 	print ""
		# print ""

		# ------------------------SECCION DONDE SE APLICA EL k-NN ---------------------
		# Validacion de las Reglas Difusas Se crean las reglas para valiar las cercanas
		RulesValidation = []
		RulesValidation = helperFuzzy3.creationRules(conjuntosDifusos, matrixLevelsInputs, matrixFuncMembershipsOutput, RulesValidation)

		# print "RulesValidation: ", RulesValidation
		# print "Regla formada de la validacion: "
		# helperFuzzy3.descriptionRules( numDatosEntrada, RulesValidation)

		# Buscar la regla mas cercana
		RulesGood = helperFuzzy3.kNNFuzzy(Rules, RulesValidation, k)

		# helperFuzzy3.descriptionRules( numDatosEntrada, RulesGood)
		# -----------------------------------------------------------------------------

		# Validacion de las reglas difusas
		arrayLevelsActivation = helperFuzzy3.validationRules(matrixLevelsInputs, matrixFuncMembershipsOutput, RulesGood, numDatosEntrada)

		# print arrayLevelsActivation

		output = np.zeros_like(x_output)
		
		aggregated = helperFuzzy3.getAggregate(arrayLevelsActivation)

		# print aggregated

		if( np.sum(aggregated) == 0.0 ):
			outputFinal = 0.0
		else:
			# Se obtiene el valor de salida esperado ESTE ES EL OUTPUT BUENO
			outputFinal = helperFuzzy3.defuzzy(x_output, aggregated)

		# print "outputFinal: ", outputFinal
		# print ""


		arrayTargets.append(target)
		arrayOutputs.append(outputFinal)

	# # # -------------------------------------------------------------------------------------------

	print "Numero de datos de entrenamiento: ", len(inpEntrenamiento)
	print "Numero de datos de validacion: ", len(inpValidacion)
	print "Datos de entrada: ", numDatosEntrada
	print "Conjutos Difusos: ", conjuntosDifusos
	print "Total de Reglas Difusas Creadas: ", len(Rules)

	targets = np.array(arrayTargets)
	outputs = np.array(arrayOutputs)

	mse = helperFuzzy3.calculateMSE(targets, outputs)

	print "Tamanio de ventana: ", ventana
	print "MSE: ", mse
	print ""

	# ventana = ventana + 1

	# arrayVentana.append(ventana)

	# arrayMSE.append(mse)

	tar = targets.reshape(len(targets), 1)

	out = outputs.reshape(len(outputs), 1)

	archivoCSV = "pronosticoDifuso/pronostico_"+str(nb_archivo)+"_ventana_"+str(ventana)+".csv"

	helperFuzzy3.exportToCSV(archivoCSV, tar, out)
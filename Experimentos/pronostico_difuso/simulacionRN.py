# Experimentos de  T + 1
import neurolab as nl
import numpy as np
import Helper2

def run( neuronasCEntrada, neuronasCOculta, algoritmoEntrenamiento, nb_archivo, archivo ):

    inp, tar = Helper2.formatInputTarget(archivo, neuronasCEntrada)

    net = nl.load("redesNeuronales/MejorN_"+ str(nb_archivo) +".net")

    out = net.sim(inp)

    return out, tar

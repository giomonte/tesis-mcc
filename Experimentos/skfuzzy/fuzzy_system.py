import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt

# Generate universe variables
#   * Quality and service on subjective ranges [0, 10]
#   * Tip has a range of [0, 25] in units of percentage points
x_qual = np.arange(0, 11, 1)
x_serv = np.arange(0, 11, 1)
x_tip  = np.arange(0, 26, 1)

print x_qual
print ''
print x_serv
print ''
print x_tip
print ''

# Generate fuzzy membership functions
qual_lo = fuzz.trimf(x_qual, [0, 0, 5])
qual_md = fuzz.trimf(x_qual, [0, 5, 10])
qual_hi = fuzz.trimf(x_qual, [5, 10, 10])
serv_lo = fuzz.trimf(x_serv, [0, 0, 5])
serv_md = fuzz.trimf(x_serv, [0, 5, 10])
serv_hi = fuzz.trimf(x_serv, [5, 10, 10])
tip_lo = fuzz.trimf(x_tip, [0, 0, 13])
tip_md = fuzz.trimf(x_tip, [0, 13, 25])
tip_hi = fuzz.trimf(x_tip, [13, 25, 25])

print 'Calidad baja'
print qual_lo
print ''
print 'Calidad media'
print qual_md
print ''
print 'Calidad alta'
print qual_hi
print ''
print 'Servicio bajo'
print serv_lo
print ''
print 'Servicio medio'
print serv_md
print ''
print 'Servicio alto'
print serv_hi
print ''
print 'Propina baja'
print tip_lo
print ''
print 'Propina media'
print tip_md
print ''
print 'Propina alta'
print tip_hi
print ''

# Visualize these universes and membership functions
fig, (ax0, ax1, ax2) = plt.subplots(nrows=3, figsize=(8, 9))

print 'Fig:'
print fig

ax0.plot(x_qual, qual_lo, 'b', linewidth=1.5, label='Mala')
ax0.plot(x_qual, qual_md, 'g', linewidth=1.5, label='Decente')
ax0.plot(x_qual, qual_hi, 'r', linewidth=1.5, label='Buena')
ax0.set_title('Calidad de la comida')
ax0.legend()

print 'ax0:'
print ax0
print ''

ax1.plot(x_serv, serv_lo, 'b', linewidth=1.5, label='Pobre')
ax1.plot(x_serv, serv_md, 'g', linewidth=1.5, label='Aceptable')
ax1.plot(x_serv, serv_hi, 'r', linewidth=1.5, label='Excelente')
ax1.set_title('Calidad del Servicio')
ax1.legend()

print 'ax1:'
print ax1
print ''

ax2.plot(x_tip, tip_lo, 'b', linewidth=1.5, label='Baja')
ax2.plot(x_tip, tip_md, 'g', linewidth=1.5, label='Mediana')
ax2.plot(x_tip, tip_hi, 'r', linewidth=1.5, label='Alta')
ax2.set_title('Promedio de Propina')
ax2.legend()

print 'ax2:'
print ax2
print ''

# Turn off top/right axes
for ax in (ax0, ax1, ax2):
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()

# plt.tight_layout()
plt.show()
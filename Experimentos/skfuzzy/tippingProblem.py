import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt

# Generate universe variables
# * Quality and service on subjective ranges [0, 10]
# * Tip has a range of [0, 25] in units of percentage points

x_qual = np.arange(0, 11, 1)
x_serv = np.arange(0, 11, 1)
x_tip = np.arange(0, 26, 1)

# Generate fuzzy membership functions
qual_lo = fuzz.trimf(x_qual, [0, 0, 5])
qual_md = fuzz.trimf(x_qual, [0, 5, 10])
qual_hi = fuzz.trimf(x_qual, [5, 10, 10])

serv_lo = fuzz.trimf(x_serv, [0, 0, 5])
serv_md = fuzz.trimf(x_serv, [0, 5, 10])
serv_hi = fuzz.trimf(x_serv, [5, 10, 10])

tip_lo = fuzz.trimf(x_tip, [0, 0, 13])
tip_md = fuzz.trimf(x_tip, [0, 13, 25])
tip_hi = fuzz.trimf(x_tip, [13, 25, 25])

# Visualize these universes and membership functions
fig, (ax0, ax1, ax2) = plt.subplots(nrows=3, figsize=(8, 9))

ax0.plot(x_qual, qual_lo, 'b', linewidth=1.5, label='Mala')
ax0.plot(x_qual, qual_md, 'g', linewidth=1.5, label='Decente')
ax0.plot(x_qual, qual_hi, 'r', linewidth=1.5, label='Buena')
ax0.set_title('Calidad de la Comida')
ax0.legend()

ax1.plot(x_serv, serv_lo, 'b', linewidth=1.5, label='Pobre')
ax1.plot(x_serv, serv_md, 'g', linewidth=1.5, label='Aceptable')
ax1.plot(x_serv, serv_hi, 'r', linewidth=1.5, label='Excelente')
ax1.set_title('Calidad del Servicio')
ax1.legend()

ax2.plot(x_tip, tip_lo, 'b', linewidth=1.5, label='Baja')
ax2.plot(x_tip, tip_md, 'g', linewidth=1.5, label='Media')
ax2.plot(x_tip, tip_hi, 'r', linewidth=1.5, label='Alta')
ax2.set_title('Porcentaje de Propina')
ax2.legend()

# Turn off top/right axes
for ax in (ax0, ax1, ax2):
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.get_xaxis().tick_bottom()
	ax.get_yaxis().tick_left()

plt.show()

# We need the activation of our fuzzy membership functions at these values.
# The exact values 6.5 and 9.8 do not exist on our universes...
# This is what fuzz.interp_membership exists for!

valueFood = 9.0
valueService = 9.0

qual_level_lo = fuzz.interp_membership(x_qual, qual_lo, valueFood)
qual_level_md = fuzz.interp_membership(x_qual, qual_md, valueFood)
qual_level_hi = fuzz.interp_membership(x_qual, qual_hi, valueFood)

print 'qual_level_lo: ', qual_level_lo
print 'qual_level_md: ', qual_level_md
print 'qual_level_hi: ', qual_level_hi

serv_level_lo = fuzz.interp_membership(x_serv, serv_lo, valueService)
serv_level_md = fuzz.interp_membership(x_serv, serv_md, valueService)
serv_level_hi = fuzz.interp_membership(x_serv, serv_hi, valueService)

print 'serv_level_lo: ', serv_level_lo
print 'serv_level_md: ', serv_level_md
print 'serv_level_hi: ', serv_level_hi

# Now we take our rules and apply them. 
# Rule 1 concerns bad food OR service.
# Rule 1 if the food is bad OR the service is bad
# The OR operator means we take the maximum of these two.
active_rule1 = np.fmax(qual_level_lo, serv_level_lo)

print 'active_rule1: ', active_rule1

# Now we apply this by clipping the top off the corresponding output
# membership function with `np.fmin`
tip_activation_lo = np.fmin(active_rule1, tip_lo) # removed entirely to 0

print 'tip_activation_lo: ', tip_activation_lo

# For rule 2 we connect acceptable service to medium tipping
# Rule 2 if the services is medium OR the food is medium
# active_rule2 = np.fmax(qual_level_md, serv_level_md)
# print 'active_rule2: ', active_rule2

tip_activation_md = np.fmin(serv_level_md, tip_md)

print 'tip_activation_md : ', tip_activation_md 

# For rule 3 we connect high service OR high food with high tipping
# Rule 3 if the service is high OR the food is high then high tipping
active_rule3 = np.fmax(qual_level_hi, serv_level_hi)

print 'active_rule3: ', active_rule3

tip_activation_hi = np.fmin(active_rule3, tip_hi)

print 'tip_activation_hi: ', tip_activation_hi

tip0 = np.zeros_like(x_tip)

print 'tip0: ', tip0

# Visualize this
fig, ax0 = plt.subplots(figsize=(8, 3))
ax0.fill_between(x_tip, tip0, tip_activation_lo, facecolor='b', alpha=0.7)
ax0.plot(x_tip, tip_lo, 'b', linewidth=0.5, linestyle='--', )
ax0.fill_between(x_tip, tip0, tip_activation_md, facecolor='g', alpha=0.7)
ax0.plot(x_tip, tip_md, 'g', linewidth=0.5, linestyle='--')
ax0.fill_between(x_tip, tip0, tip_activation_hi, facecolor='r', alpha=0.7)
ax0.plot(x_tip, tip_hi, 'r', linewidth=0.5, linestyle='--')
ax0.set_title('Actividad de la salidad de membresia')

# Turn off top/right axes
for ax in (ax0,):
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.get_xaxis().tick_bottom()
	ax.get_yaxis().tick_left()

plt.show()

# Aggregate all three output membership functions together - Se colorea el espacio difuso
print 'tip_activation_lo: ',tip_activation_lo
print ''
print 'tip_activation_md: ',tip_activation_md
print ''
print 'tip_activation_hi: ',tip_activation_hi
print ''

aggregated = np.fmax(tip_activation_lo,
np.fmax(tip_activation_md, tip_activation_hi))

print 'aggregated: ', aggregated

# Calculate defuzzified result
tip = fuzz.defuzz(x_tip, aggregated, 'centroid')

print 'tip: ', tip

tipAux = fuzz.dcentroid(x_tip, aggregated, tip)

print "tipAux: ", tipAux

tip_activation = fuzz.interp_membership(x_tip, aggregated, tip) # for plot

print 'tip_activation: ', tip_activation

# Visualize this
fig, ax0 = plt.subplots(figsize=(8, 3))
ax0.plot(x_tip, tip_lo, 'b', linewidth=0.5, linestyle='--', )
ax0.plot(x_tip, tip_md, 'g', linewidth=0.5, linestyle='--')
ax0.plot(x_tip, tip_hi, 'r', linewidth=0.5, linestyle='--')
ax0.fill_between(x_tip, tip0, aggregated, facecolor='Orange', alpha=0.7)
ax0.plot([tip, tip], [0, tip_activation], 'k', linewidth=1.5, alpha=0.9)
ax0.set_title('Aggregated membership and result (line)')

# Turn off top/right axes
for ax in (ax0,):
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.get_xaxis().tick_bottom()
	ax.get_yaxis().tick_left()

plt.show()
import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt

# Generate universe variables
# * Quality and service on subjective ranges [0, 10]
# * Tip has a range of [0, 25] in units of percentage points

x_velocidad 	= np.arange(0, 11, 1)
x_tiempo 		= np.arange(0, 11, 1)
x_definicion 	= np.arange(0, 11, 1)
x_dificultad 	= np.arange(0, 11, 1)

x_calificacion 	= np.arange(0, 11, 1)

# Generate fuzzy membership functions
velocidad_baja 	= fuzz.trimf(x_velocidad, [0, 0, 5])
velocidad_media = fuzz.trimf(x_velocidad, [0, 5, 10])
velocidad_alta 	= fuzz.trimf(x_velocidad, [5, 10, 10])

tiempo_baja 	= fuzz.trimf(x_tiempo, [0, 0, 5])
tiempo_media 	= fuzz.trimf(x_tiempo, [0, 5, 10])
tiempo_alta 	= fuzz.trimf(x_tiempo, [5, 10, 10])

definicion_baja 	= fuzz.trimf(x_definicion, [0, 0, 5])
definicion_media 	= fuzz.trimf(x_definicion, [0, 5, 10])
definicion_alta 	= fuzz.trimf(x_definicion, [5, 10, 10])

dificultad_baja 	= fuzz.trimf(x_dificultad, [0, 0, 5])
dificultad_media 	= fuzz.trimf(x_dificultad, [0, 5, 10])
dificultad_alta 	= fuzz.trimf(x_dificultad, [5, 10, 10])

# --------------------------------------------------------

calificacion_baja 	= fuzz.trimf(x_calificacion, [0, 0, 5])
calificacion_media 	= fuzz.trimf(x_calificacion, [0, 5, 10])
calificacion_alta 	= fuzz.trimf(x_calificacion, [5, 10, 10])

# Visualize these universes and membership functions
fig, (ax0, ax1, ax2, ax3, ax4) = plt.subplots(nrows=5, figsize=(10, 12))

ax0.plot(x_velocidad, velocidad_baja, 'b', linewidth=1.5, label='Baja')
ax0.plot(x_velocidad, velocidad_media, 'g', linewidth=1.5, label='Media')
ax0.plot(x_velocidad, velocidad_alta, 'r', linewidth=1.5, label='Alta')
ax0.set_title('Calificacion de la Velocidad')
ax0.legend()

ax1.plot(x_tiempo, dificultad_baja, 'b', linewidth=1.5, label='Baja')
ax1.plot(x_tiempo, dificultad_media, 'g', linewidth=1.5, label='Media')
ax1.plot(x_tiempo, dificultad_alta, 'r', linewidth=1.5, label='Alta')
ax1.set_title('Calificacion del Tiempo')
ax1.legend()

ax2.plot(x_definicion, definicion_baja, 'b', linewidth=1.5, label='Baja')
ax2.plot(x_definicion, definicion_media, 'g', linewidth=1.5, label='Media')
ax2.plot(x_definicion, definicion_alta, 'r', linewidth=1.5, label='Alta')
ax2.set_title('Calificacion de la Definicion')
ax2.legend()

ax3.plot(x_dificultad, dificultad_baja, 'b', linewidth=1.5, label='Baja')
ax3.plot(x_dificultad, dificultad_media, 'g', linewidth=1.5, label='Media')
ax3.plot(x_dificultad, dificultad_alta, 'r', linewidth=1.5, label='Alta')
ax3.set_title('Calificacion de la Dificultad')
ax3.legend()

ax4.plot(x_calificacion, calificacion_baja, 'b', linewidth=1.5, label='Baja')
ax4.plot(x_calificacion, calificacion_media, 'g', linewidth=1.5, label='Media')
ax4.plot(x_calificacion, calificacion_alta, 'r', linewidth=1.5, label='Alta')
ax4.set_title('Calificacion Final')
ax4.legend()

# Turn off top/right axes
for ax in (ax0, ax1, ax2, ax3, ax4):
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.get_xaxis().tick_bottom()
	ax.get_yaxis().tick_left()

plt.show()

# We need the activation of our fuzzy membership functions at these values.
# The exact values 6.5 and 9.8 do not exist on our universes...
# This is what fuzz.interp_membership exists for!

valueFood = 9.0
valueService = 9.0

gradoMembresiaVelocidadBaja 	= fuzz.interp_membership(x_velocidad, velocidad_baja, valueFood)
gradoMembresiaVelocidadMedia 	= fuzz.interp_membership(x_velocidad, velocidad_media, valueFood)
gradoMembresiaVelocidadAlta 	= fuzz.interp_membership(x_velocidad, velocidad_alta, valueFood)

print 'gradoMembresiaVelocidadBaja: ', gradoMembresiaVelocidadBaja
print 'gradoMembresiaVelocidadMedia: ', gradoMembresiaVelocidadMedia
print 'gradoMembresiaVelocidadAlta: ', gradoMembresiaVelocidadAlta

gradoMembresiaTiempoBaja 	= fuzz.interp_membership(x_tiempo, dificultad_baja, valueService)
gradoMembresiaTiempoMedia 	= fuzz.interp_membership(x_tiempo, dificultad_media, valueService)
gradoMembresiaTiempoAlta 	= fuzz.interp_membership(x_tiempo, dificultad_alta, valueService)

print 'gradoMembresiaTiempoBaja: ', gradoMembresiaTiempoBaja
print 'gradoMembresiaTiempoMedia: ', gradoMembresiaTiempoMedia
print 'gradoMembresiaTiempoAlta: ', gradoMembresiaTiempoAlta

# Now we take our rules and apply them. 
# Rule 1 concerns bad food OR service.
# Rule 1 if the food is bad OR the service is bad
# The OR operator means we take the maximum of these two.
active_rule1 = np.fmax(gradoMembresiaVelocidadBaja, gradoMembresiaTiempoBaja)

print 'active_rule1: ', active_rule1

# Now we apply this by clipping the top off the corresponding output
# membership function with `np.fmin`
tip_activation_lo = np.fmin(active_rule1, calificacion_baja) # removed entirely to 0

print 'tip_activation_lo: ', tip_activation_lo

# For rule 2 we connect acceptable service to medium tipping
# Rule 2 if the services is medium OR the food is medium
# active_rule2 = np.fmax(gradoMembresiaVelocidadMedia, gradoMembresiaTiempoMedia)
# print 'active_rule2: ', active_rule2

tip_activation_md = np.fmin(gradoMembresiaTiempoMedia, calificacion_media)

print 'tip_activation_md : ', tip_activation_md 

# For rule 3 we connect high service OR high food with high tipping
# Rule 3 if the service is high OR the food is high then high tipping
active_rule3 = np.fmax(gradoMembresiaVelocidadAlta, gradoMembresiaTiempoAlta)

print 'active_rule3: ', active_rule3

tip_activation_hi = np.fmin(active_rule3, calificacion_alta)

print 'tip_activation_hi: ', tip_activation_hi

tip0 = np.zeros_like(x_calificacion)

print 'tip0: ', tip0

# Visualize this
fig, ax0 = plt.subplots(figsize=(8, 3))
ax0.fill_between(x_calificacion, tip0, tip_activation_lo, facecolor='b', alpha=0.7)
ax0.plot(x_calificacion, calificacion_baja, 'b', linewidth=0.5, linestyle='--', )
ax0.fill_between(x_calificacion, tip0, tip_activation_md, facecolor='g', alpha=0.7)
ax0.plot(x_calificacion, calificacion_media, 'g', linewidth=0.5, linestyle='--')
ax0.fill_between(x_calificacion, tip0, tip_activation_hi, facecolor='r', alpha=0.7)
ax0.plot(x_calificacion, calificacion_alta, 'r', linewidth=0.5, linestyle='--')
ax0.set_title('Actividad de la salidad de membresia')

# Turn off top/right axes
for ax in (ax0,):
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.get_xaxis().tick_bottom()
	ax.get_yaxis().tick_left()

plt.show()

# Aggregate all three output membership functions together - Se colorea el espacio difuso
print 'tip_activation_lo: ',tip_activation_lo
print ''
print 'tip_activation_md: ',tip_activation_md
print ''
print 'tip_activation_hi: ',tip_activation_hi
print ''

aggregated = np.fmax(tip_activation_lo,
np.fmax(tip_activation_md, tip_activation_hi))

print 'aggregated: ', aggregated

# Calculate defuzzified result
tip = fuzz.defuzz(x_calificacion, aggregated, 'centroid')

print 'tip: ', tip

tipAux = fuzz.dcentroid(x_calificacion, aggregated, tip)

print "tipAux: ", tipAux

tip_activation = fuzz.interp_membership(x_calificacion, aggregated, tip) # for plot

print 'tip_activation: ', tip_activation

# Visualize this
fig, ax0 = plt.subplots(figsize=(8, 3))
ax0.plot(x_calificacion, calificacion_baja, 'b', linewidth=0.5, linestyle='--', )
ax0.plot(x_calificacion, calificacion_media, 'g', linewidth=0.5, linestyle='--')
ax0.plot(x_calificacion, calificacion_alta, 'r', linewidth=0.5, linestyle='--')
ax0.fill_between(x_calificacion, tip0, aggregated, facecolor='Orange', alpha=0.7)
ax0.plot([tip, tip], [0, tip_activation], 'k', linewidth=1.5, alpha=0.9)
ax0.set_title('Aggregated membership and result (line)')

# Turn off top/right axes
for ax in (ax0,):
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.get_xaxis().tick_bottom()
	ax.get_yaxis().tick_left()

plt.show()
# Experimentos de  T + 1
import neurolab as nl
import numpy as np
import Helper

def run( neuronasCEntrada, neuronasCOculta, algoritmoEntrenamiento, nb_archivo, archivo ):

    inp, tar = Helper.formatInputTarget(archivo, neuronasCEntrada)

    net = nl.load("redes/wind_malpais/MejorN_"+ str(nb_archivo) +".net")

    out = net.sim(inp)

    return out, tar

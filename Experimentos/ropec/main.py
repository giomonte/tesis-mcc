# Proceso de entrenamiento de red neuronal entreando 3 veces obteniendo el mejor error
import Helper
import redNeuronal
import neurolab as nl
import numpy as np

# Sin reconstruccion
# directorio = 'seriesTiempoConHuecos/'

# Con reconstruccion
directorio = 'seriesTiempo/wind_malpais/'

numeroMuestra =  3000

# [nombreArchivo, nE, nO, aE]
datosArchivos = np.array([
                        # ['serieTiempoConHuecos_wind_corrales_10m_muestra',43,28,6],
                        # ['serieTiempoConHuecos_wind_corrales_10m_muestra_reconstruidoANN', 18,2,5],
                        # ['serieTiempoConHuecos_wind_corrales_10m_muestra_reconstruidoKNN', 32,4,4]
                        # ['serieTiempoConHuecos_wind_elfresno_10m_muestra', 30,3,6],
                        # ['serieTiempoConHuecos_wind_elfresno_10m_muestra_reconstruidoANN', 46,8,7]
                        # ['serieTiempoConHuecos_wind_elfresno_10m_muestra_reconstruidoKNN', 42,14,7]
                         # ['serieTiempoConHuecos_wind_malpais_10m_muestra', 29,8,6]
                        # ['serieTiempoConHuecos_wind_malpais_10m_muestra_reconstruidoKNN', 8,4,7]
                        ['serieTiempoConHuecos_wind_malpais_10m_muestra_reconstruidoANN', 55,4,6]
                    ])

for i in range (0, len(datosArchivos)):

    nb_archivo = datosArchivos[i][0]
    rutaArchivo = directorio + nb_archivo+'.csv'

    print "Generando la mejor Red para la serie : "+ rutaArchivo

    # Sin reconstruccion
    # archivo = Helper.obtenerArchivo( rutaArchivo, numeroMuestra)

    # Reconstruccion
    archivo = Helper.funcionLeerArchivo2( rutaArchivo )

    # print archivo

    numeroValidacion = int(( len(archivo) * 0.20 ))

    numeroTraining = numeroMuestra - numeroValidacion

    arrayError  = []
    arrayNet    = []

    for j in range(0,3):

        error, net = redNeuronal.run( int( datosArchivos[i][1] ) , int ( datosArchivos[i][2]), int(datosArchivos[i][3]), 6000, archivo, numeroTraining)

        print "Error ", (j+1)," : ", error

        arrayError.append(error)
        arrayNet.append(net)

    mejorError = min(arrayError)

    print "Mejor Error: ", mejorError

    posicionMejorNet = np.argmin(arrayError)
    mejorNet =  arrayNet[posicionMejorNet]

    mejorNet.save(('redes/wind_malpais/MejorN_'+ str(nb_archivo) +'.net'))

    # print "Red neuronal generada : " + nombreRedNeuronal

    # # Se simula el t + 1
    # ejecutaSimulacionRedNeuronalT1.run(datosArchivos[i][1], datosArchivos[i][2], datosArchivos[i][3], nb_archivo, archivo)

    # # Se simula el t + 24
    # ejecutaSimulacionRedNeuronal.run(datosArchivos[i][1], datosArchivos[i][2], datosArchivos[i][3], nb_archivo, archivo, numeroTraining)

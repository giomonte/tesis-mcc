# Experimentos de t+1
import simulacionRedNeuronalT1
import csv
import matplotlib.pyplot as plt
import numpy as np
np.set_printoptions(threshold=np.inf)
import Helper
import math

print "T1"

# directorio = 'seriesTiempoConHuecos/'
rutaSerieArchivo = 'seriesTiempo/wind_malpais/'

# Con reconstruccion
directorio = 'seriesTiempo/wind_malpais/'

numeroMuestra =  3000

t = 1

datosArchivos = np.array([

                        # ['serieTiempoConHuecos_wind_corrales_10m_muestra',43,28,6],
                        # ['serieTiempoConHuecos_wind_corrales_10m_muestra_reconstruidoANN', 18,2,5]
                        # ['serieTiempoConHuecos_wind_corrales_10m_muestra_reconstruidoKNN', 32,4,4]
                        # ['serieTiempoConHuecos_wind_elfresno_10m_muestra', 30,3,6]
                        # ['serieTiempoConHuecos_wind_elfresno_10m_muestra_reconstruidoANN', 46,8,7]
                        # ['serieTiempoConHuecos_wind_elfresno_10m_muestra_reconstruidoKNN', 42,14,7]
                        # ['serieTiempoConHuecos_wind_malpais_10m_muestra', 29,8,6]
                        # ['serieTiempoConHuecos_wind_malpais_10m_muestra_reconstruidoKNN', 8,4,7]
                        ['serieTiempoConHuecos_wind_malpais_10m_muestra_reconstruidoANN', 55,4,6]
                    ])

# Posicion del archivo a simular
x = 0

neuronasCentrada = int ( datosArchivos[x][1] )
neuronasCOculta = int  ( datosArchivos[x][2] )
algoritmoEntrenamiento = int ( datosArchivos[x][3] )

nb_archivo = datosArchivos[x][0]

rutaArchivo = directorio + nb_archivo+'.csv'

print "Generando pronostico para la serie : ", rutaArchivo

# Sin reconstruccion
# archivo = Helper.obtenerArchivo( rutaArchivo, numeroMuestra)

# Reconstruccion
archivo = Helper.funcionLeerArchivo2( rutaArchivo )

numeroValidacion = int(( len(archivo) * 0.20 ))

numeroTraining = numeroMuestra - numeroValidacion

archivo = archivo[numeroTraining :]

out, tar = simulacionRedNeuronalT1.run( neuronasCentrada, neuronasCOculta, algoritmoEntrenamiento, nb_archivo, archivo )

# print "out : ", out
# print "tar : ", tar

nombreArchivoPronostico = rutaSerieArchivo + nb_archivo + "_pronosticoANN_CR_ANN.csv"
print "Archivo con pronostico ANN : ", nombreArchivoPronostico
Helper.guardarExcel(nombreArchivoPronostico, tar, out, "Target", "Output")
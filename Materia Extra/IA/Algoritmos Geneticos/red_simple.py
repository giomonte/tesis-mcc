from sknn.mlp import Regressor, Layer

import numpy as np

# 1 significa verdadero, 0 significa falso

Input_entrenamiento= np.array([[1,1],[1,0],[0,1],[0,0]])

Output_entrenamiento= np.array([1,0,0,1])

nn = Regressor(

layers=[

Layer("Rectifier", units=10),

Layer("Linear")],

learning_rate=0.2,

n_iter=500)

nn.fit(Input_entrenamiento, Output_entrenamiento)

nn.get_parameters()

Input_test= np.array([[0,0]])

Output_test = nn.predict(Input_test)

Output_test
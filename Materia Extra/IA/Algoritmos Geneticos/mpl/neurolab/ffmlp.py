# -*- coding: utf-8 -*-
"""
Example of use multi-layer perceptron
=====================================

Task: Approximation function: 1/2 * sin(x)

"""

import neurolab as nl
import numpy as np

# Create train samples
#20 = tamaño de las entradas de entrenamiento
x = np.linspace(-50, 50, 10)
y = np.sin(x) * 0.5
#y = np.power(x, 2)

size = len(x)

inp = x.reshape(size,1)
tar = y.reshape(size,1)

# Create network with 2 layers and random initialized
#20 = numero de neuronas en la capa oculta
# 1 = neurona de salida
net = nl.net.newff([[-50, 50]],[10, 1])

print net

# Train network
error = net.train(inp, tar, epochs=500, show=50, goal=0.02)
#print error

#x2 = np.linspace(-50, 50, 100)
#size2 = len(x2)
#inp2 = x2.reshape(size2,1)
# Simulate network
out = net.sim(inp)
#print out
# create network with 2 inputs and 10 neurons
#net2 = nl.net.newc([[-1, 1], [-1, 1]], 10)
#print net2
#error2 = net2.train(inp, epochs=500, show=100, goal=0.01)
#print error2

# Plot result
import pylab as pl
pl.subplot(211)
pl.plot(error)
pl.xlabel('Epoch number')
pl.ylabel('error (default SSE)')

#150 = numero de puntos de entrada
x2 = np.linspace(-20.0,20.0,150)
y2 = net.sim(x2.reshape(x2.size,1)).reshape(x2.size)

y3 = out.reshape(size)

pl.subplot(212)
pl.plot(x2, y2, '-',x , y, '.', x, y3, 'p')
pl.legend(['train target', 'net output'])
pl.show()

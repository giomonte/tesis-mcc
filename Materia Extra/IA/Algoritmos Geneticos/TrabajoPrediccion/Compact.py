__author__ = 'igor'

from random import random
from random import uniform

class Solution(object):
    """
    A solution for the given problem, it is composed of a binary value and its fitness value
    """
    def __init__(self, value):
        self.value = value
        self.fitness = 0

    def calculate_fitness(self, fitness_function):
        #print "Self", self
        #print "Fitness_Function", fitness_function
        self.fitness = fitness_function(self.value)


def generate_candidate(vector):
    """
    Generates a new candidate solution based on the probability vector
    """
    value = ""
    #guardar datos en un arreglo
    arregloFloat = []  
    
    for p in vector:
        value += "1" if random() < p else "0"  
        arregloFloat.append( uniform(0.5, 1) if random() < p else uniform(0, 0.5) )
    
    #print "Vector", vector
    #print "Valor:",value
    #print "Arreglo Float:", arregloFloat
    
    #return Solution(value)
    return Solution(arregloFloat)

def generate_vector(size):
    """
    Initializes a probability vector with given size
    """
    return [0.5] * size


def compete(a, b):
    """
    Returns a tuple with the winner solution
    """
    #print "Valor a.fitness: ", a.fitness
    #print "Valor b.fitness: ", b.fitness
    if a.fitness > b.fitness:
        return a, b
    else:
        return b, a


def update_vector(vector, winner, loser, population_size):
    for i in xrange(len(vector)):
        if winner[i] != loser[i]:
            if winner[i] == '1':
                vector[i] += 1.0 / float(population_size)
            else:
                vector[i] -= 1.0 / float(population_size)
def run(generations, size, population_size, fitness_function):
    # this is the probability for each solution bit be 1
    vector = generate_vector(size)
    #print "Se genera el vector" , vector
    best = None

    # I stop by the number of generations but you can define any stop param
    for i in xrange(generations):
        # generate two candidate solutions, it is like the selection on a conventional GA
        s1 = generate_candidate(vector)
        #print "Se genera candidato:", s1
        s2 = generate_candidate(vector)

        # calculate fitness for each
        s1.calculate_fitness(fitness_function)
        s2.calculate_fitness(fitness_function)

        # let them compete, so we can know who is the best of the pair
        winner, loser = compete(s1, s2)
       

        if best:
            if winner.fitness > best.fitness:
                best = winner
        else:
            best = winner
    
        #print "Best:", best
        # updates the probability vector based on the success of each bit
        update_vector(vector, winner.value, loser.value, population_size)

        print "generation: %d best value: %s best fitness: %f" % (i + 1, best.value, float(best.fitness))
def functionEvaluationFlotante(x):
    
    global mejor
    
    anterior =  x[0]
    #print "Posicion 0:", anterior
    
    #print "Random:", uniform(0.5, 1)
    
    #if( anterior == 1 ):
        #anterior = 0
        
    if( anterior <= 1 and anterior >= 0.5 ):
        anterior = uniform(0, 0.5)
    
    suma = 0
    #print "Arreglo", x
    for i in x:  
        
        #valor = int ( i )
        valor = float (i)
        #print valor
        
        #if(  valor == 0 and anterior == 1  or  valor == 1 and anterior == 0  ):
            #suma += 1
            
        if(  ( valor >= 0 and valor <= 0.5 ) and (anterior <= 1 and anterior >= 0.5)  or  ( valor >= 0.5 and valor <= 1 ) and (anterior <= 0.5 and anterior >= 0)  ):
            suma += 1
            
        anterior = valor
    #print(x)
    if( suma > mejor ):
        print( "Cromosoma: ", x , "Valor: ", suma )
        mejor = suma
        
    return suma

if __name__ == '__main__':
    global mejor
    mejor = 0
    
    #def run(generations, size, population_size, fitness_function):
    f = lambda x: functionEvaluationFlotante(x)
    run(1000, 50, 500, f)
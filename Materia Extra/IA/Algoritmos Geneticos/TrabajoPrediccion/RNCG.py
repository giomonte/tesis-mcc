__author__ = 'igor'

import redNeuronal

from random import random
from random import uniform

class Solution(object):
    """
    A solution for the given problem, it is composed of a binary value and its fitness value
    """
    def __init__(self, value):
        self.value = value
        self.fitness = 0

    def calculate_fitness(self, fitness_function):
        self.fitness = fitness_function(self.value)

def generate_candidate(vector):
    """
    Generates a new candidate solution based on the probability vector
    """
    arregloBinario = []  
    
    for p in vector:
        arregloBinario.append( 1 if random() < p else 0 )

    return Solution(arregloBinario)

def generate_vector(size):
    """
    Initializes a probability vector with given size
    """
    return [0.5] * size


def compete(a, b):
    """
    Returns a tuple with the winner solution
    """
    if a.fitness > b.fitness:
        return a, b
    else:
        return b, a


def update_vector(vector, winner, loser, population_size):
    for i in xrange(len(vector)):
        if winner[i] != loser[i]:
            if winner[i] == '1':
                vector[i] += 1.0 / float(population_size)
            else:
                vector[i] -= 1.0 / float(population_size)
                          
def run(generations, size, population_size, fitness_function):
    # this is the probability for each solution bit be 1
    vector = generate_vector(size)
    best = None

    # I stop by the number of generations but you can define any stop param
    for i in xrange(generations):
        # generate two candidate solutions, it is like the selection on a conventional GA
        s1 = generate_candidate(vector)
        s2 = generate_candidate(vector)

        # calculate fitness for each
        s1.calculate_fitness(fitness_function)
        s2.calculate_fitness(fitness_function)

        # let them compete, so we can know who is the best of the pair
        winner, loser = compete(s1, s2)
       

        if best:
            if winner.fitness > best.fitness:
                best = winner
        else:
            best = winner
    
        #print "Best:", best
        # updates the probability vector based on the success of each bit
        update_vector(vector, winner.value, loser.value, population_size)

        print "generation: %d best value: %s best fitness: %f" % (i + 1, best.value, float(best.fitness))

def convertirABinario(x):
    if x>=0.5:
        return 1
    return 0
def convertirBinarioADecimal(arreglo):
    res=0
    aux=0
    for i in range(len(arreglo)-1,-1,-1):
        if arreglo[i]>0:
            res=res+2**aux
        aux+=1
    return res 

def TipoFuncionEntrenamiento(num):
    
    rango = int( num * 12 + 1 )
    
    print "Numero de entrenamiento: ", num 
    print "Rango:", rango
    
    return num

def functionEvaluationI(x):
    print x
    #METODO DE PRUEBA
    global mejorE
    global mejorC
    
    #Numero de Neuronas de entrada
    Ne = 0
    #Numero de Neuronas en la capa oculta
    Nn = 0
    
    #Ne = map(convertirABinario,x[:4])
    Ne = x[:6]
    print Ne
    Ne = convertirBinarioADecimal(Ne) + 1#para evitar el valor 0 entradas
    
    #Nn = map(convertirABinario, x[4:8])    
    Nn = x[6:12]
    print Nn
    Nn = convertirBinarioADecimal(Nn) + 1#para evitar el valor 0 entradas
    
    print "Neuronas de entrada:", Ne
    print "Neuronas ocultaas", Nn

    vector = [1,1,1,1,1,1]

    print "vector: ", vector
    print convertirBinarioADecimal(vector) + 1
    
    #NumFunc = TipoFuncionEntrenamiento(x[8:12])
    NumFunc = 0
    NumFunc = x[12:15]
    print NumFunc
    NumFunc = convertirBinarioADecimal(NumFunc) 
    print "Numero de funcion", NumFunc
    
    if( Ne > 0 and Nn > 0 and ( NumFunc > 0 and NumFunc <=12 ) and ( NumFunc != 6 and NumFunc !=7 ) ):
        
        error = redNeuronal.run(Ne, Nn, NumFunc, 70)
        #error = redNeuronal.run( 16, 13, 9, 70 )
        errorMin = error

    else:
        #penalizacion
        errorMin = 10000
    
    print "Error minimo: ", errorMin
    
    if errorMin < mejorE:
            mejorE = errorMin
            mejorC = x
        
    return (errorMin) * -1

if __name__ == '__main__': 
    global mejorE
    global mejorC
    mejorE = 100000
    
    #def run(generations, size, population_size, fitness_function):
    f = lambda x: functionEvaluationI(x)
    run(1, 15, 50, f)
    
    print "Mejor resultado", mejorE
    print "Mejor individiuo", mejorC
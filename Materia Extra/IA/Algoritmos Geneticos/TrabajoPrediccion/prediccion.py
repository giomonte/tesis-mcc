__author__ = 'igor'

import redNeuronal

from random import random
from random import uniform

class Solution(object):
    """
    A solution for the given problem, it is composed of a binary value and its fitness value
    """
    def __init__(self, value):
        self.value = value
        self.fitness = 0

    def calculate_fitness(self, fitness_function):
        #print "Self", self
        #print "Fitness_Function", fitness_function
        self.fitness = fitness_function(self.value)


def generate_candidate(vector):
    """
    Generates a new candidate solution based on the probability vector
    """
    value = ""
    #guardar datos en un arreglo
    arregloFloat = []  
    
    for p in vector:
        value += "1" if random() < p else "0"  
        arregloFloat.append( uniform(0.5, 1) if random() < p else uniform(0, 0.5) )
    
    #print "Vector", vector
    #print "Valor:",value
    #print "Arreglo Float:", arregloFloat
    
    #return Solution(value)
    return Solution(arregloFloat)

def generate_vector(size):
    """
    Initializes a probability vector with given size
    """
    return [0.5] * size


def compete(a, b):
    """
    Returns a tuple with the winner solution
    """
    #print "Valor a.fitness: ", a.fitness
    #print "Valor b.fitness: ", b.fitness
    if a.fitness > b.fitness:
        return a, b
    else:
        return b, a


def update_vector(vector, winner, loser, population_size):
    for i in xrange(len(vector)):
        if winner[i] != loser[i]:
            if winner[i] == '1':
                vector[i] += 1.0 / float(population_size)
            else:
                vector[i] -= 1.0 / float(population_size)
def run(generations, size, population_size, fitness_function):
    # this is the probability for each solution bit be 1
    vector = generate_vector(size)
    #print "Se genera el vector" , vector
    best = None

    # I stop by the number of generations but you can define any stop param
    for i in xrange(generations):
        # generate two candidate solutions, it is like the selection on a conventional GA
        s1 = generate_candidate(vector)
        #print "Se genera candidato:", s1
        s2 = generate_candidate(vector)

        # calculate fitness for each
        s1.calculate_fitness(fitness_function)
        s2.calculate_fitness(fitness_function)

        # let them compete, so we can know who is the best of the pair
        winner, loser = compete(s1, s2)
       

        if best:
            if winner.fitness > best.fitness:
                best = winner
        else:
            best = winner
    
        #print "Best:", best
        # updates the probability vector based on the success of each bit
        update_vector(vector, winner.value, loser.value, population_size)

        print "generation: %d best value: %s best fitness: %f" % (i + 1, best.value, float(best.fitness))
def convertirABinario(x):
    if x>=0.5:
        return 1
    return 0
def convertirBinarioADecimal(arreglo):
    res=0
    aux=0
    for i in range(len(arreglo)-1,-1,-1):
        if arreglo[i]>0:
            res=res+2**aux
        aux+=1
    return res 
    
def functionEvaluationFlotante(x):
    #METODO DE PRUEBA
    global mejor
    suma = 0
    anterior =  x[0]
    
    #Numero de Neuronas de entrada
    Ne = 0
    #Numero de Neuronas en la capa oculta
    Nn = 0
    
    Ne = map(convertirABinario,x[:4])
    Ne = convertirBinarioADecimal(Ne) + 1#para evitar el valor 0 entradas
    
    Nn = map(convertirABinario, x[4:8])    
    Nn = convertirBinarioADecimal(Nn) + 1#para evitar el valor 0 entradas
    
    error = redNeuronal.run( Ne, Nn, 40 )
    
    print "Tamaño del arreglo del error", len(error)
            
    if( anterior <= 1 and anterior >= 0.5 ):
        anterior = uniform(0, 0.5)
    
    
    for i in x:  
        
        valor = float (i)
            
        if(  ( valor >= 0 and valor <= 0.5 ) and (anterior <= 1 and anterior >= 0.5)  or  ( valor >= 0.5 and valor <= 1 ) and (anterior <= 0.5 and anterior >= 0)  ):
            suma += 1
            
        anterior = valor

    if( suma > mejor ):
        print( "Cromosoma: ", x , "Valor: ", suma )
        mejor = suma
        
    return suma
def functionEvaluationIndividuo(x):
    #PENDIENTE
    global mejorE
    global mejorC
    
    #Numero de Neuronas de entrada
    Ne = 0
    #Numero de Neuronas en la capa oculta
    Nn = 0
    
    Ne = map(convertirABinario,x[:4])
    Ne = convertirBinarioADecimal(Ne) + 1#para evitar el valor 0 entradas
    
    Nn = map(convertirABinario, x[4:8])    
    Nn = convertirBinarioADecimal(Nn) + 1#para evitar el valor 0 entradas
    
    error = redNeuronal.run( Ne, Nn, 1000 )
    
    errorMin = obtenerMenorError(error)
    
    print errorMin
    
    for i in error:
        if( i < mejorE ):
            mejorE = i
            
            
    '''
        if errorMin < mejorE
            mejorE = errorMin
            mejorC = x
        
        return (errorMin) * -1
    '''
            
    return (errorMin) * -1
if __name__ == '__main__':
    global mejorE
    global mejorC
    mejorE = 100000
    
    #def run(generations, size, population_size, fitness_function):
    f = lambda x: functionEvaluationFlotante(x)
    run(1, 8, 50, f)
    
    #print "Mejor resultado", mejor
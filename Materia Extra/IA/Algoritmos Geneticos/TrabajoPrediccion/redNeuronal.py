import neurolab as nl
import numpy as np

def run( neuronasCEntrada, nuronasCOculta, algoritmoEntrenamiento, epocas ):
    data = np.genfromtxt('IMECA_1eros_MIL_DATOS.csv',delimiter=',')
    numeroMaximo = np.amax(data)
    numeroMinimo = np.amin(data);

    datos=map( lambda x: (( x - numeroMinimo ) / ( numeroMaximo - numeroMinimo)), data)
    continuar = True
    index = 0
    auxIndex = 1
    datosEntrenamiento = datos[:700]

    inputArrayAux = []
    inputArray = []
    targetArray = []

    while index < len( datosEntrenamiento ) - 1:
        inputArrayAux.append( datosEntrenamiento[ index ] )
        if auxIndex == neuronasCEntrada:
            inputArray.append( inputArrayAux )
            inputArrayAux = []
            targetArray.append( datosEntrenamiento[ index + 1] )
            index = index - (neuronasCEntrada - 1)
            auxIndex = 0
        auxIndex+=1
        index+=1

    input = np.array( inputArray )
    inp = input.reshape( len(input), neuronasCEntrada )
    target = np.array( targetArray )
    lenT = len(target)
    tar = target.reshape(lenT, 1);

    net = nl.net.newff([[0, 1]] * neuronasCEntrada,[ nuronasCOculta, 1])

    if algoritmoEntrenamiento == 1:
        net.trainf = nl.train.train_gd
    elif algoritmoEntrenamiento == 2:
        net.trainf = nl.train.train_gdm
    elif algoritmoEntrenamiento == 3:
        net.trainf = nl.train.train_gda
    elif algoritmoEntrenamiento == 4:
        net.trainf = nl.train.train_gdx
    elif algoritmoEntrenamiento == 5:
        net.trainf = nl.train.train_rprop
    elif algoritmoEntrenamiento == 6:
        net.trainf = nl.train.train_wta
    elif algoritmoEntrenamiento == 7:
        net.trainf = nl.train.train_cwta
    elif algoritmoEntrenamiento == 8:
        net.trainf = nl.train.train_bfgs
    elif algoritmoEntrenamiento == 9:
        net.trainf = nl.train.train_cg
    elif algoritmoEntrenamiento == 10:
        net.trainf = nl.train.train_ncg
    elif algoritmoEntrenamiento == 11:
        net.trainf = nl.train.train_lvq
    elif algoritmoEntrenamiento == 12:
        net.trainf = nl.train.train_delta

    error = net.train(inp, tar, epochs=epocas, show=10, goal=0.002)
    return np.amin(error)

#!/usr/local/bin/python
# coding: latin-1
# import ejecutaSimulacionRedNeuronalT1
# import ejecutaSimulacionRedNeuronal
import neurolab as nl
import numpy as np
import Helper

def run(neuronasCentrada, neuronasCOculta, algoritmoEntrenamiento, epocas, numeroPasosPronostico, archivo, numeroTraining):

    datosNormalizados = archivo

    #Del 70 porciento atenrior se toma el 70 porciento para datos de entrenamiento
    datosEntrenamiento = datosNormalizados[:numeroTraining]
    
    #Del 70 porciento atenrior se toma el 70 porciento para datos de validacion en la simulación
    datosValidacion = datosNormalizados[numeroTraining:]

    #Se obtienen datos de entrenamiento con formato y target
    inpEntrenamiento, tarEntrenamiento = Helper.getDataFormatted(datosEntrenamiento, neuronasCentrada, numeroPasosPronostico)

    #Se obtienen datos de validación con formato y target
    inpValidacion, tarValidacion = Helper.getDataFormatted(datosValidacion, neuronasCentrada, numeroPasosPronostico)

    #Se crea la red neuronal
    net = nl.net.newff([[0, 1]] * neuronasCentrada,[ neuronasCOculta, 1])

    #Se asigna el algoritmo de entrenamiento
    if algoritmoEntrenamiento == 0:
        net.trainf = nl.train.train_gd
    elif algoritmoEntrenamiento == 1:
        net.trainf = nl.train.train_gdm
    elif algoritmoEntrenamiento == 2:
        net.trainf = nl.train.train_gda
    elif algoritmoEntrenamiento == 3:
        net.trainf = nl.train.train_gdx
    elif algoritmoEntrenamiento == 4:
        net.trainf = nl.train.train_rprop
    elif algoritmoEntrenamiento == 5:
        net.trainf = nl.train.train_bfgs
    elif algoritmoEntrenamiento == 6:
        net.trainf = nl.train.train_cg
    elif algoritmoEntrenamiento == 7:
        net.trainf = nl.train.train_ncg

    #Se entrena la red neuronal
    error = net.train(inpEntrenamiento, tarEntrenamiento, epochs=epocas, show=100, goal=0.00002)

    #Se realiza una simulación con los datos de validación
    out = net.sim(inpValidacion)

    # Se calcula el MSE - Error cuadrático medio
    fMse = nl.error.MSE()
    mse = fMse(tarValidacion, out)

    # Se retorna el mse y la instancia de la red
    return mse, net
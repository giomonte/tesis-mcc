# Proceso de entrenamiento de red neuronal entreando 3 veces obteniendo el mejor error
import Helper
import redNeuronal
import neurolab as nl
import numpy as np


def entrenamiento(datosArchivos):

    directorio = 'datosExperimentosOriginales/'

    numeroMuestra =  2000
    numeroValidacion = 80
   
    numeroPasosPronostico = 1

    # [nombreArchivo, nE, nO, aE]
    # datosArchivos = np.array([  
    #                         ['wind_20891_10m_complete',16,4,5],
    #                         ['wind_22641_10m_complete',10,2,5], 

    
    #                         ['wind_27947_10m_complete',5,60,4],
    #                         ['wind_29231_10m_complete',25,16,3],
    #                         ['wind_30230_10m_complete',5,21,6],
    #                         ['wind_37099_10m_complete',32,6,1]
    #                     ])

    # datosArchivos = np.array([  
    #                         ['wind_22887_10m_complete',45,41,5]
    #                     ])

    for i in range (0, len(datosArchivos)):

        nb_archivo = datosArchivos[i][0]
        rutaArchivo = directorio + nb_archivo+'.csv' 

        archivo = Helper.obtenerArchivo( rutaArchivo, numeroMuestra)

        archivo = Helper.formatear(archivo)

        numeroMuestra = len(archivo)
        numeroTraining = numeroMuestra - numeroValidacion - int( datosArchivos[i][1] )

        arrayError  = []
        arrayNet    = []

        for j in range(0,3):

            # (nE,nO,aE,numeroPasosPronostico, archivo, numeroTrainig)
            error, net = redNeuronal.run( int( datosArchivos[i][1] ) , int ( datosArchivos[i][2]), int(datosArchivos[i][3]), 3000, numeroPasosPronostico, archivo, numeroTraining)

            print "Error ", (j+1)," : ", error

            arrayError.append(error)
            arrayNet.append(net)

        mejorError = min(arrayError)

        print "Mejor Error: ", mejorError

        posicionMejorNet = np.argmin(arrayError)
        mejorNet =  arrayNet[posicionMejorNet]

        mejorNet.save(('redesExperimentos/MejorN_' + str(numeroPasosPronostico) + '_'+ str(nb_archivo) +'.net'))

        # # Se simula el t + 1
        # ejecutaSimulacionRedNeuronalT1.run(datosArchivos[i][1], datosArchivos[i][2], datosArchivos[i][3], nb_archivo, archivo)

        # # Se simula el t + 24
        # ejecutaSimulacionRedNeuronal.run(datosArchivos[i][1], datosArchivos[i][2], datosArchivos[i][3], nb_archivo, archivo)

datosArchivos1 = np.array([  
    # ['wind_20891_10m_complete',57,24,5],
    # ['wind_22641_10m_complete',59,39,5],
    # ['wind_22887_10m_complete',61,64,5]
    # ['wind_23711_10m_complete',54,35,5]
    # ['wind_24908_10m_complete',62,59,5],
    # ['wind_27947_10m_complete',39,37,5],
    # ['wind_28722_10m_complete',45,47,5],
    # ['wind_29231_10m_complete',41,35,5]
    ['wind_30230_10m_complete',33,25,5],
    ['wind_37099_10m_complete',43,63,5]
])

entrenamiento(datosArchivos1)
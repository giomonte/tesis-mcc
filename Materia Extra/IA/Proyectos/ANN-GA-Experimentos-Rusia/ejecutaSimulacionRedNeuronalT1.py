# Experimentos de t+1
import simulacionRedNeuronalT1
import csv
import matplotlib.pyplot as plt
import numpy as np
import Helper
import math

directorio = 'datosExperimentosOriginales/'

numeroMuestra =  2000
numeroPasosPronostico = 1
t = 1

datosArchivos = np.array([  
    ['wind_20891_10m_complete',57,24,5],
    ['wind_22641_10m_complete',59,39,5],
    ['wind_22887_10m_complete',61,64,5],
    ['wind_23711_10m_complete',54,35,5],
    ['wind_24908_10m_complete',62,59,5],
    ['wind_27947_10m_complete',39,37,5],
    ['wind_28722_10m_complete',45,47,5],
    ['wind_29231_10m_complete',41,35,5],
    ['wind_30230_10m_complete',33,25,5],
    ['wind_37099_10m_complete',43,63,5]
])

# Posicion del archivo a simular
# x = 9

for x in xrange(0,len(datosArchivos)):
    
    neuronasCentrada = int ( datosArchivos[x][1] )
    neuronasCOculta = int  ( datosArchivos[x][2] )
    algoritmoEntrenamiento = int ( datosArchivos[x][3] )

    nb_archivo = datosArchivos[x][0]

    rutaArchivo = directorio + nb_archivo+'.csv' 

    archivo = Helper.obtenerArchivo( rutaArchivo, numeroMuestra)

    archivo = Helper.formatear(archivo)

    numeroMuestra = len(archivo)

    numeroValidacion = 80
    numeroTraining = numeroMuestra - numeroValidacion  - neuronasCentrada

    print "numeroTraining: ", numeroTraining

    numeroMinimo, numeroMaximo = Helper.valoresMinMax(rutaArchivo, numeroMuestra)

    print ""
    print "numeroMaximo: ", numeroMaximo
    print "numeroMinimo: ", numeroMinimo

    out, tar = simulacionRedNeuronalT1.run( neuronasCentrada, neuronasCOculta, algoritmoEntrenamiento, numeroPasosPronostico, nb_archivo, archivo, numeroTraining )

    Helper.imprimirResultado(tar, out, numeroMinimo, numeroMaximo, algoritmoEntrenamiento, nb_archivo, neuronasCentrada, neuronasCOculta, t)

# archivoCSV = "pronosticos_t1/pronostico_t1_"+str(numeroPasosPronostico)+"_"+str(nb_archivo)+".csv"

# with open(archivoCSV, "wb") as ArchivoNuevoCSV:

#     writer = csv.writer(ArchivoNuevoCSV, lineterminator='\n')

#     writer.writerow(['Target', 'Output'])

#     for i in range(0, len(tar)):

#     	tarTemp = str(tar[i])
#         outTemp = str(out[i])

#         aux1 = tarTemp.replace("[", "")
#         tarAdd = aux1.replace("]", "")

#         aux2 = outTemp.replace("[", "")
#         outAdd = aux2.replace("]", "")

#         writer.writerow( (tarAdd, outAdd) )
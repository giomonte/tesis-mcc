# 24 dias
import simulacionRedNeuronal
import csv
import matplotlib.pyplot as plt
import numpy as np
import Helper
import math

directorio = 'datosExperimentosOriginales/'
numeroPasosPronostico = 1
t = 8

datosArchivos = np.array([  
    ['wind_20891_10m_complete',57,24,5],
    ['wind_22641_10m_complete',59,39,5],
    ['wind_22887_10m_complete',61,64,5],
    ['wind_23711_10m_complete',54,35,5],
    ['wind_24908_10m_complete',62,59,5],
    ['wind_27947_10m_complete',39,37,5],
    ['wind_28722_10m_complete',45,47,5],
    ['wind_29231_10m_complete',41,35,5],
    ['wind_30230_10m_complete',33,25,5],
    ['wind_37099_10m_complete',43,63,5]
])

# x = 9

for x in xrange(0, len(datosArchivos)):


    nE = int ( datosArchivos[x][1] ) # Neuronas en la capa de entrada
    nO = int ( datosArchivos[x][2] ) # Neuronas en la capa oculta
    aE = int ( datosArchivos[x][3] )  # Algoritmo de entrenamiento

    nb_archivo = datosArchivos[x][0]

    rutaArchivo = directorio + nb_archivo+'.csv' 

    numeroMuestra =  2000

    archivo = Helper.obtenerArchivo( rutaArchivo, numeroMuestra)

    numeroMinimo, numeroMaximo = Helper.valoresMinMax(rutaArchivo, numeroMuestra)

    print ""
    print "numeroMaximo: ", numeroMaximo
    print "numeroMinimo: ", numeroMinimo

    # Se quitan los -1
    archivo = Helper.formatear(archivo)

    numeroValidacion = 80
    numeroMuestra = len(archivo)
    numeroTraining = numeroMuestra - numeroValidacion

    datosNecesario = 10 * 8 + nE

    datosInicio = len(archivo) - datosNecesario

    # Datos Pronosticados
    pronosticados = []

    for iteracion1 in range(0, 10):

    	datosEntrenamiento = archivo[datosInicio - 1:]
    	ventana = datosEntrenamiento[:nE]

    	for iteracion2 in range(0, 8):

    		pronostico = simulacionRedNeuronal.run( nE, nO, aE, ventana, nb_archivo )
    		pronosticados.append(pronostico)

    		ventana.append(pronostico)
    		ventana = ventana[1:]

    	datosInicio  = datosInicio + 8

    tar = archivo[numeroTraining:]

    out = pronosticados

    Helper.imprimirResultado(tar, out, numeroMinimo, numeroMaximo, aE, nb_archivo, nE, nO, t)

# Crear archivo .csv
# archivoCSV = "pronosticos_t24/pronostico_t24_"+str(nb_archivo)+".csv"

# with open(archivoCSV, "wb") as ArchivoNuevoCSV:

#     writer = csv.writer(ArchivoNuevoCSV, lineterminator='\n')

#     writer.writerow(['Target', 'Output'])

#     for i in range(0, len(tar)):

#     	tarTemp = str(tar[i])
#         outTemp = str(out[i])

#         aux1 = tarTemp.replace("[", "")
#         tarAdd = aux1.replace("]", "")

#         aux2 = outTemp.replace("[", "")
#         outAdd = aux2.replace("]", "")

#         writer.writerow( (tarAdd, outAdd) )
# Experimentos de t+1
import simulacionRedNeuronalT1
import csv
import matplotlib.pyplot as plt
import numpy as np
import Helper
import math

directorio = 'datosExperimentosOriginales/'

numeroMuestra =  1000
numeroValidacion = 240
numeroTraining = numeroMuestra - numeroValidacion
numeroPasosPronostico = 1
t = 1

datosArchivos = np.array([  
                        ['wind_aristeomercado_10m_complete',16,4,5],
                        ['wind_cointzio_10m_complete',10,2,5], 
                        ['wind_corrales_10m_complete',5,60,4],
                        ['wind_elfresno_10m_complete',25,16,3],
                        ['wind_lapalma_10m_complete',5,21,6],
                        ['wind_lapiedad_10m_complete',32,6,1],
                        ['wind_malpais_10m_complete',64,5,1],
                        ['wind_markazuza_10m_complete',53,19,5],
                        ['wind_melchorocampo_10m_complete',39,15,4],
                        ['wind_patzcuaro_10m_complete',29,3,4]
                    ])

# Posicion del archivo a simular
x = 0

neuronasCentrada = int ( datosArchivos[x][1] )
neuronasCOculta = int  ( datosArchivos[x][2] )
algoritmoEntrenamiento = int ( datosArchivos[x][3] )

nb_archivo = datosArchivos[x][0]

rutaArchivo = directorio + nb_archivo+'.csv' 

archivo = Helper.obtenerArchivo( rutaArchivo, numeroMuestra)

numeroMinimo, numeroMaximo = Helper.valoresMinMax(rutaArchivo, numeroMuestra)

print ""
print "numeroMaximo: ", numeroMaximo
print "numeroMinimo: ", numeroMinimo

out, tar = simulacionRedNeuronalT1.run( neuronasCentrada, neuronasCOculta, algoritmoEntrenamiento, numeroPasosPronostico, nb_archivo, archivo )

Helper.imprimirResultado(tar, out, numeroMinimo, numeroMaximo, algoritmoEntrenamiento, nb_archivo, neuronasCentrada, neuronasCOculta, t)

archivoCSV = "pronosticos_t1/pronostico_t1_"+str(numeroPasosPronostico)+"_"+str(nb_archivo)+".csv"

with open(archivoCSV, "wb") as ArchivoNuevoCSV:

    writer = csv.writer(ArchivoNuevoCSV, lineterminator='\n')

    writer.writerow(['Target', 'Output'])

    for i in range(0, len(tar)):

    	tarTemp = str(tar[i])
        outTemp = str(out[i])

        aux1 = tarTemp.replace("[", "")
        tarAdd = aux1.replace("]", "")

        aux2 = outTemp.replace("[", "")
        outAdd = aux2.replace("]", "")

        writer.writerow( (tarAdd, outAdd) )
# Proceso de entrenamiento de red neuronal entreando 3 veces obteniendo el mejor error
import Helper
import redNeuronal
import neurolab as nl
import numpy as np

directorio = 'datosExperimentosOriginales/'

numeroMuestra =  1000
numeroValidacion = 240
numeroTraining = numeroMuestra - numeroValidacion
numeroPasosPronostico = 1

# [nombreArchivo, nE, nO, aE]
datosArchivos = np.array([  
                        ['wind_aristeomercado_10m_complete',16,4,5],
                        ['wind_cointzio_10m_complete',10,2,5], 
                        ['wind_corrales_10m_complete',5,60,4],
                        ['wind_elfresno_10m_complete',25,16,3],
                        ['wind_lapalma_10m_complete',5,21,6],
                        ['wind_lapiedad_10m_complete',32,6,1],
                        ['wind_malpais_10m_complete',64,5,1],
                        ['wind_markazuza_10m_complete',53,19,5],
                        ['wind_melchorocampo_10m_complete',39,15,4],
                        ['wind_patzcuaro_10m_complete',29,3,4]
                    ])

for i in range (0, len(datosArchivos)):

    nb_archivo = datosArchivos[i][0]
    rutaArchivo = directorio + nb_archivo+'.csv' 

    archivo = Helper.obtenerArchivo( rutaArchivo, numeroMuestra)

    arrayError  = []
    arrayNet    = []

    for j in range(0,3):

        # (nE,nO,aE,numeroPasosPronostico, archivo, numeroTrainig)
        error, net = redNeuronal.run( int( datosArchivos[i][1] ) , int ( datosArchivos[i][2]), int(datosArchivos[i][3]), 2000, numeroPasosPronostico, archivo, numeroTraining)

        print "Error ", (j+1)," : ", error

        arrayError.append(error)
        arrayNet.append(net)

    mejorError = min(arrayError)

    print "Mejor Error: ", mejorError

    posicionMejorNet = np.argmin(arrayError)
    mejorNet =  arrayNet[posicionMejorNet]

    mejorNet.save(('redesExperimentos/MejorN_' + str(numeroPasosPronostico) + '_'+ str(nb_archivo) +'.net'))

    # # Se simula el t + 1
    # ejecutaSimulacionRedNeuronalT1.run(datosArchivos[i][1], datosArchivos[i][2], datosArchivos[i][3], nb_archivo, archivo)

    # # Se simula el t + 24
    # ejecutaSimulacionRedNeuronal.run(datosArchivos[i][1], datosArchivos[i][2], datosArchivos[i][3], nb_archivo, archivo, numeroTraining)

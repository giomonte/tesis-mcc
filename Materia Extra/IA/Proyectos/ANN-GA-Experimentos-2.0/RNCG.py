__author__ = 'igor'

import redNeuronal
import Helper
import csv
import numpy as np

from random import random
from random import uniform

class Solution(object):
    """
    A solution for the given problem, it is composed of a binary value and its fitness value
    """
    def __init__(self, value):
        self.value = value
        self.fitness = 0

    def calculate_fitness(self, fitness_function):
        self.fitness = fitness_function(self.value)

def generate_candidate(vector):
    """
    Generates a new candidate solution based on the probability vector
    """
    arregloBinario = []

    for p in vector:
        arregloBinario.append( 1 if random() < p else 0 )

    return Solution(arregloBinario)

def generate_vector(size):
    """
    Initializes a probability vector with given size
    """
    return [0.5] * size


def compete(a, b):
    """
    Returns a tuple with the winner solution
    """
    if a.fitness > b.fitness:
        return a, b
    else:
        return b, a


def update_vector(vector, winner, loser, population_size):
    for i in xrange(len(vector)):
        if winner[i] != loser[i]:
            if winner[i] == '1':
                vector[i] += 1.0 / float(population_size)
            else:
                vector[i] -= 1.0 / float(population_size)

def run(generations, size, population_size, fitness_function):
    # this is the probability for each solution bit be 1
    vector = generate_vector(size)
    best = None

    # I stop by the number of generations but you can define any stop param
    for i in xrange(generations):
        # generate two candidate solutions, it is like the selection on a conventional GA
        s1 = generate_candidate(vector)
        s2 = generate_candidate(vector)

        # calculate fitness for each
        s1.calculate_fitness(fitness_function)
        s2.calculate_fitness(fitness_function)

        # let them compete, so we can know who is the best of the pair
        winner, loser = compete(s1, s2)


        if best:
            if winner.fitness > best.fitness:
                best = winner
        else:
            best = winner

        #print "Best:", best
        # updates the probability vector based on the success of each bit
        update_vector(vector, winner.value, loser.value, population_size)

        print "generation: %d best value: %s best fitness: %f" % (i + 1, best.value, float(best.fitness))

def convertirABinario(x):
    if x>=0.5:
        return 1
    return 0
def convertirBinarioADecimal(arreglo):
    res=0
    aux=0
    for i in range(len(arreglo)-1,-1,-1):
        if arreglo[i]>0:
            res=res+2**aux
        aux+=1
    return res

def TipoFuncionEntrenamiento(num):

    rango = int( num * 12 + 1 )

    print "Numero de entrenamiento: ", num
    print "Rango:", rango

    return num

def functionEvaluationI(x):
    global mejorConf
    global mejorE
    global mejorC
    global mejorN
    global numeroPasosPronostico
    global numeroTraining

    #Numero de Neuronas de entrada
    Ne = 0
    #Numero de Neuronas en la capa oculta
    Nn = 0

    #Ne = map(convertirABinario,x[:4])
    Ne = x[:6]
    Ne = convertirBinarioADecimal(Ne) + 1 #para evitar el valor 0 entradas

    #Nn = map(convertirABinario, x[4:8])
    Nn = x[6:12]
    Nn = convertirBinarioADecimal(Nn) + 1 #para evitar el valor 0 entradas

    #print "Neuronas de entrada:", Ne
    #print "Neuronas ocultaas", Nn

    #NumFunc = TipoFuncionEntrenamiento(x[8:12])
    NumFunc = x[12:15]
    NumFunc = convertirBinarioADecimal(NumFunc)

    # print "Ne: ", Ne
    # print "Nn: ", Nn

    if( Ne > 0 and Nn > 0):
        error, net = redNeuronal.run(Ne, Nn, NumFunc, 150, numeroPasosPronostico, archivo, numeroTraining)

        errorMin = error
    else:
        errorMin = 10000

    #print "Error minimo: ", errorMin

    if errorMin < mejorE:
            mejorConf = "Ne:", Ne, "Nn:", Nn, "NumFunc:", NumFunc
            mejorE = errorMin
            mejorC = x
            mejorN = net
            print mejorConf, " ", mejorE

    return (errorMin * -1)

if __name__ == '__main__':
    global mejorConf
    global mejorE
    global mejorC
    global mejorN
    global archivo
    global numeroPasosPronostico
    global numeroTraining

    numeroPasosPronostico = 1
    directorio = 'datosVientoPruebas/'

    datos = np.array([  
                        'wind_cointzio', 
                        'wind_cointzio_2',
                        'wind_corrales',
                        'wind_elfresno',
                        'wind_lapalma',
                        'wind_lapiedad',
                        'wind_malpais',
                        'wind_markazuza',
                        'wind_melchorocampo',
                        'wind_patzcuaro'
                    ])
    # datosMuestra = np.array([1000,1300])

    numeroMuestra =  1300
    numeroValidacion = 240
    numeroTraining = numeroMuestra - numeroValidacion

    topologia =  np.array([])

    for i in range (0, len(datos)):

        # numeroTraining = datosMuestra[i] - numeroValidacion

        nb_archivo = datos[i]
        rutaArchivo = directorio + nb_archivo+'.csv' 

        archivo = Helper.obtenerArchivo( rutaArchivo, numeroMuestra)
        # archivo = Helper.obtenerArchivo( rutaArchivo, datosMuestra[i])

        # print archivo
        mejorE = 100000

        #def run(generations, size, population_size, fitness_function):
        f = lambda x: functionEvaluationI(x)

        run(100, 15, 100, f)

        print "Archivo: ", rutaArchivo
        print "Topologia:"
        print "Mejor resultado", mejorE
        print "Mejor individiuo", mejorC
        print mejorConf
        print mejorN

        topologia = np.append(topologia, str(mejorConf) ) 

        mejorN.save(('pronosticosPruebas/MejorN' + str(numeroPasosPronostico) + '_'+ str(nb_archivo) +'.net'))

    # Se guarda en un archivo cvs las topologias

    archivoTopologias = "archivoTopologias.csv"

    print topologia

    with open(archivoTopologias, "wb") as ArchivoNuevoCSV:

        writer = csv.writer(ArchivoNuevoCSV, lineterminator='\n')

        writer.writerow(['Archivo', 'Topologia'])

        tam = len(datos)
        i = 0

        while i < tam:

            tarTemp = str(datos[i])
            outTemp = str(topologia[i])

            aux1 = tarTemp.replace("[", "")
            datosAdd = aux1.replace("]", "")

            aux2 = outTemp.replace("[", "")
            topologiaAdd = aux2.replace("]", "")

            writer.writerow( (datosAdd, topologiaAdd) )
            i += 1
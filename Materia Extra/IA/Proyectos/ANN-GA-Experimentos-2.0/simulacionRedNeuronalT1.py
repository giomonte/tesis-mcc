# Experimentos de  T + 1
import neurolab as nl
import numpy as np
import Helper

def run( neuronasCEntrada, neuronasCOculta, algoritmoEntrenamiento, numeroPasosPronostico, nb_archivo, archivo ):

    datos = archivo

    datosEntrenamiento = datos[760:]

    inp, tar = Helper.getDataFormatted(datosEntrenamiento, neuronasCEntrada, numeroPasosPronostico)

    net = nl.load("redesExperimentos/MejorN_"+str(numeroPasosPronostico)+"_"+str(nb_archivo)+".net")

    out = net.sim(inp)

    return out, tar
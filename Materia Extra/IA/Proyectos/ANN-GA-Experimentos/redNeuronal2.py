#!/usr/local/bin/python
# coding: latin-1
# Red Neuronal validacion en 4 partes
import neurolab as nl
import numpy as np
import Helper

def run(neuronasCentrada, nuronasCOculta, algoritmoEntrenamiento, epocas, numeroPasosPronostico, archivo):

    datosNormalizados = archivo

    #Se toma el 70 porciento del data set original ya normalizado y se deja un 30 porciento para validaciones
    datosParciales = datosNormalizados[:700]
    folds = 4

    tamanioFold = len(datosParciales) / folds

    arrayError  = []
    arrayNet    = []

    for i in range(0, folds):

        datosEntrenamiento =  list(datosParciales)
        datosValidacion = datosEntrenamiento[i * tamanioFold:  tamanioFold * (i + 1)]
        del datosEntrenamiento[i * tamanioFold:  tamanioFold * (i + 1)]

        #Se obtienen datos de entrenamiento con formato y target
        inpEntrenamiento, tarEntrenamiento = Helper.getDataFormatted(datosEntrenamiento, neuronasCentrada, numeroPasosPronostico)

        #Se obtienen datos de validación con formato y target
        inpValidacion, tarValidacion = Helper.getDataFormatted(datosValidacion, neuronasCentrada, numeroPasosPronostico)

        #Se crea la red neuronal
        net = nl.net.newff([[0, 1]] * neuronasCentrada,[ nuronasCOculta, 1])

        #Se asigna el algoritmo de entrenamiento
        if algoritmoEntrenamiento == 0:
            net.trainf = nl.train.train_gd
        elif algoritmoEntrenamiento == 1:
            net.trainf = nl.train.train_gdm
        elif algoritmoEntrenamiento == 2:
            net.trainf = nl.train.train_gda
        elif algoritmoEntrenamiento == 3:
            net.trainf = nl.train.train_gdx
        elif algoritmoEntrenamiento == 4:
            net.trainf = nl.train.train_rprop
        elif algoritmoEntrenamiento == 5:
            net.trainf = nl.train.train_bfgs
        elif algoritmoEntrenamiento == 6:
            net.trainf = nl.train.train_cg
        elif algoritmoEntrenamiento == 7:
            net.trainf = nl.train.train_ncg

        #Se entrena la red neuronal
        error = net.train(inpEntrenamiento, tarEntrenamiento, epochs=epocas, show=100, goal=0.00002)

        #Se realiza una simulación con los datos de validación
        out = net.sim(inpValidacion)

        #Se calcula el MSE - Error cuadrático medio
        fMse = nl.error.MSE()
        mse = fMse(tarValidacion, out)

        arrayError.append(mse)
        arrayNet.append(net)

    mejorError = min(arrayError)
    posicionMejorNet = np.argmin(arrayError)
    mejorNet =  arrayNet[posicionMejorNet]

    #Se retorna el mse y la instancia de la red
    return mejorError, mejorNet
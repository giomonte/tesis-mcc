import simulacionRedNeuronal
import csv
import matplotlib.pyplot as plt
import numpy as np
import Helper

directorio = 'datosExperimentosPruebas/'
nb_archivo = 'wind_aristeomercado_10m'

nE = 16 # Neuronas en la capa de entrada
nO = 4 # Neuronas en la capa oculta
aE = 5  # Algoritmo de entrenamiento

rutaArchivo = directorio + nb_archivo+'.csv' 

numeroMuestra =  1000
numeroValidacion = 240
numeroTraining = numeroMuestra - numeroValidacion

archivo = Helper.obtenerArchivo( rutaArchivo, 0)

datosNecesario = 10 * 24 + nE

datosInicio = len(archivo) - datosNecesario

# Datos Pronosticados
pronosticados = []

for iteracion1 in range(0, 10):

	# print "ITERACION ", iteracion1 + 1

	datosEntrenamiento = archivo[datosInicio - 1:]
	ventana = datosEntrenamiento[:nE]

	for iteracion2 in range(0, 24):

		pronostico = simulacionRedNeuronal.run( nE, nO, aE, ventana, nb_archivo )
		pronosticados.append(pronostico)

		ventana.append(pronostico)
		ventana = ventana[1:]

		# print iteracion2 + 1, " - PRONOSTICO", pronostico

	datosInicio  = datosInicio + 24

# print pronosticados

tar = archivo[numeroTraining:]

out = pronosticados

# Crear archivo .csv
arrayTarget = []
arrayOutput = []

archivoCSV = "datosExperimentosPruebas/pronostico2_"+str(nb_archivo)+".csv"

with open(archivoCSV, "wb") as ArchivoNuevoCSV:

    writer = csv.writer(ArchivoNuevoCSV, lineterminator='\n')

    writer.writerow(['Target', 'Output'])

    tam = len(tar)
    i = 0

    while i < tam:

    	tarTemp = str(tar[i])
        outTemp = str(out[i])

        aux1 = tarTemp.replace("[", "")
        tarAdd = aux1.replace("]", "")

        aux2 = outTemp.replace("[", "")
        outAdd = aux2.replace("]", "")

        arrayTarget.append(tarAdd)
        arrayOutput.append(outAdd)

        writer.writerow( (tarAdd, outAdd) )
        i += 1

# GRAFICA
plt.plot(arrayTarget, label='Target')
plt.plot(arrayOutput, label='Output')

plt.title("Grafica de prediccion")
plt.legend()
plt.show()
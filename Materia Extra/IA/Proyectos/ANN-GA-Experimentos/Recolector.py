import numpy as np
import csv

datos = np.array([	'wind_cointzio_10auxm', 
					# 'wind_cointzio_10m', 
					'wind_corrales_10m',
					'wind_elfresno_10m',
					'wind_lapalma_10m',
					'wind_lapiedad_10m',
					'wind_malpais_10m',
					'wind_markazuza_10m',
					'wind_melchorocampo_10m',
					'wind_patzcuaro_10m'])

topologia =  np.array([])

# ('Ne:', 41, 'Nn:', 38, 'NumFunc:', 5)

for i in range (0, len(datos)):

	print datos[i]


topologia = np.append(topologia, "('Ne:', 41, 'Nn:', 38, 'NumFunc:', 5)" )
# topologia = np.append(topologia, "('Ne:', 42, 'Nn:', 38, 'NumFunc:', 5)" )
topologia = np.append(topologia, "('Ne:', 43, 'Nn:', 38, 'NumFunc:', 5)" )
topologia = np.append(topologia, "('Ne:', 44, 'Nn:', 38, 'NumFunc:', 5)" )
topologia = np.append(topologia, "('Ne:', 45, 'Nn:', 38, 'NumFunc:', 5)" )
topologia = np.append(topologia, "('Ne:', 46, 'Nn:', 38, 'NumFunc:', 5)" )
topologia = np.append(topologia, "('Ne:', 47, 'Nn:', 38, 'NumFunc:', 5)" )
topologia = np.append(topologia, "('Ne:', 48, 'Nn:', 38, 'NumFunc:', 5)" )
topologia = np.append(topologia, "('Ne:', 49, 'Nn:', 38, 'NumFunc:', 5)" )
topologia = np.append(topologia, "('Ne:', 50, 'Nn:', 38, 'NumFunc:', 5)" )

print topologia


archivoTopologias = "archivoTopologias.csv"

with open(archivoTopologias, "wb") as ArchivoNuevoCSV:

    writer = csv.writer(ArchivoNuevoCSV, lineterminator='\n')

    writer.writerow(['Archivo', 'Topologia'])

    tam = len(datos)
    i = 0

    while i < tam:

    	tarTemp = str(datos[i])
        outTemp = str(topologia[i])

        aux1 = tarTemp.replace("[", "")
        datosAdd = aux1.replace("]", "")

        aux2 = outTemp.replace("[", "")
        topologiaAdd = aux2.replace("]", "")

        writer.writerow( (datosAdd, topologiaAdd) )
        i += 1
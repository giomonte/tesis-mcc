# Experimentos de 24 dias
import experimento
import csv
import matplotlib.pyplot as plt
import numpy as np
import Helper

directorio = 'datosExperimentosPruebas/'
nb_archivo = 'wind_aristeomercado_10m'

rutaArchivo = directorio + nb_archivo+'.csv' 

archivo = Helper.obtenerArchivo( rutaArchivo, 0)

print "archivo, ", archivo

numeroPasosPronostico = 1

out, tar = experimento.run( 16, 4, 5, numeroPasosPronostico, archivo )


arrayTarget = []
arrayOutput = []

archivoCSV = "datosExperimentosPruebas/experimento2_"+str(numeroPasosPronostico)+"_wind_aristeomercado_10m.csv"

with open(archivoCSV, "wb") as ArchivoNuevoCSV:

    writer = csv.writer(ArchivoNuevoCSV, lineterminator='\n')

    writer.writerow(['Target', 'Output'])

    tam = len(tar)
    i = 0

    while i < tam:

    	tarTemp = str(tar[i])
        outTemp = str(out[i])

        aux1 = tarTemp.replace("[", "")
        tarAdd = aux1.replace("]", "")

        aux2 = outTemp.replace("[", "")
        outAdd = aux2.replace("]", "")

        arrayTarget.append(tarAdd)
        arrayOutput.append(outAdd)

        writer.writerow( (tarAdd, outAdd) )
        i += 1

# GRAFICA
plt.plot(arrayTarget, label='Target')
plt.plot(arrayOutput, label='Output')

plt.title("Grafica de prediccion T+"+ str(numeroPasosPronostico) +" wind_lapalma_10m")
# plt.title("Grafica de prediccion T+"+ str(numeroPasosPronostico) +" SVC" )

plt.legend()
plt.show()
# Experimentos de 24 dias

import neurolab as nl
import numpy as np
import Helper


def run( neuronasCEntrada, nuronasCOculta, algoritmoEntrenamiento, numeroPasosPronostico, archivo ):

    datos = archivo

    continuar = True
    index = 0
    auxIndex = 1
    datosEntrenamiento = datos[760:]

    # print "Datos de entrenamiento", datosEntrenamiento

    inp, tar = Helper.getDataFormatted(datosEntrenamiento, neuronasCEntrada, numeroPasosPronostico)

    print "TARGET : ",tar

    net = nl.load("datosExperimentosPruebas/Mejor2N"+str(numeroPasosPronostico)+"_wind_aristeomercado_10m.net")

    out = net.sim(inp)

    print "OUTPUT : ",out

    return out, tar
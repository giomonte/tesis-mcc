import neurolab as nl
import numpy as np

def run( neuronasCEntrada, nuronasCOculta, algoritmoEntrenamiento, epocas ):

    data = np.genfromtxt('aplicacion/IMECA_1eros_MIL_DATOS.csv',delimiter=',')
    numeroMaximo = np.amax(data)
    numeroMinimo = np.amin(data)

    datos=map( lambda x: (( x - numeroMinimo ) / ( numeroMaximo - numeroMinimo)), data)
    continuar = True
    index = 0
    auxIndex = 1
    datosEntrenamiento = datos[700:]

    inputArray = []
    targetArray = []
    for i in range (0, ( len(datosEntrenamiento) - neuronasCEntrada - 1)):
        inputArray.append(datosEntrenamiento[ i: i + neuronasCEntrada])
        targetArray.append(datosEntrenamiento[ i + neuronasCEntrada])

    input = np.array( inputArray )
    inp = input.reshape( len(input), neuronasCEntrada )

    target = np.array( targetArray )
    lenT = len(target)
    tar = target.reshape(lenT, 1);

    net = nl.load('aplicacion/mejorN.net')

    out = net.sim(inp)

    return out, tar

NET <- function( Pattern , Weights ){ # Cross product
	cat( " Net ", sum( Pattern * Weights ) )
	return( sum( Pattern * Weights ) )
}
ActivationFunction <- function( net ){
	if( net > 0)
		return( 1 )
	return( 0 )
}
Learning <- function( x , t , z , c ){ # wights increment
	cat( " Increment ", c * ( t - z ) * x ,"\n")
	return( c * ( t - z ) * x )
}
Perceptron <- function( Patterns , Targets , Weights = -1 , LearningRate = 1, ErrorRate = 0 , MaxIter = 10 ){
	if( dim(Patterns)[ 1 ] == length( Targets ) ){#Number of patterns must be equal to number of targets
		if( length( Weights ) == 1 )
			Weights <- runif( dim(Patterns)[ 2 ] , 0 , 1 )# Generates the weigths
		Weights_Old <- Weights - 10000
		OutPuts <- array( 10000000 , length( Targets ) )# Generates the outputs
		Iterations <- 0
		while( Iterations < MaxIter && length( which( ( abs( OutPuts - Targets ) > ErrorRate ) == T , F ) ) > 0 ){
			cat("Iteration  ",(Iterations+1)," -----------------------\n")
			OutPuts_Old <- OutPuts #Saves the outputs
			for( i in 1 : dim( Patterns )[ 1 ] ){
				cat("Pattern ",Patterns[ i , ]," Target ",Targets[ i ]," Weights ",Weights)
				OutPuts[ i ] <- ActivationFunction( NET( Patterns[ i , ] , Weights ) )
				cat(" Output ",OutPuts[ i ])
				Weights <- Weights + Learning( Patterns[ i , ] , Targets[ i ] , OutPuts[ i ] , LearningRate )
				
			}
			cat("              Evaluation %%%%%%%%%%%%%%%%%%%%\n")
			cat("              Targets ",Targets,"OutPuts",OutPuts,"Difference",abs(OutPuts-Targets),"\n")
			cat("              %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n")
			
			
			Iterations <- Iterations + 1
		}
		if( Iterations == MaxIter && length( which( ( abs( OutPuts - Targets ) > ErrorRate ) == T , F ) ) > 0 )
			cat("\n\nPerceptron Convergence Theorem failed.\nMaybe", Iterations,"iterations aren't enougth to training the the perceptron. Try a higger iteration\nOr maybe the activation function used is not suitable. Try another activation function\nEtc.")
		else if(length( which( ( abs( OutPuts - Targets ) > ErrorRate ) == T , F ) ) == 0 && Iterations <= MaxIter )
			cat("\n\nThe weigths are ",Weights)	
		
	}else{
		print("Number of patterns must be equal to number of targets")
	}
	
}

Patterns <- rbind( c(0,0,0,0),c(0,0,0,0),c(0,0,0,0),c(0,0,0,0) )
Targets <- c(0,1,1,1,5)
Weights <- c(0,0,0,0)



Perceptron( Patterns , Targets , Weights = 1 , LearningRate = 1, ErrorRate = 0 , MaxIter = 10 )
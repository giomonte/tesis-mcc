import neurolab as nl
import numpy as np

#numpy es para trabajar con vectores y matrcices
from numpy import genfromtxt
my_data = genfromtxt('datos1-20.csv', delimiter=',')
#print my_data

# Create train samples
# lleno el vector de la posicion 1 al 15
datos80 = my_data[:15]

index = 0
auxIndex = 1
#asigna los valores del 16 al 20 a un vector
datos2 = my_data[16:]

inputArrayAux = []
inputArray = []
targetArray = []

while index < len(datos80) - 1:
    inputArrayAux.append( datos80[ index ] )
    if auxIndex == 3:
        inputArray.append( inputArrayAux )
        inputArrayAux = []
        targetArray.append( datos80[ index + 1] )
        index = index - 2
        auxIndex = 0
    auxIndex+=1
    index+=1

    #Entradas a la Red   
inp = np.array( inputArray )
input1 = inp.reshape(len(inp),3)
#print input1

#print ""
target = np.array( targetArray )
#print target

#Hace reshape para reordenar los datos en filas y columnas
lenT = len(target)
tar = target.reshape(lenT, 1);
#print tar
 

# Create network with 2 layers and random initialized
# las entradas van conforme al numero de entradas que tengo en la serie de tiempo
# para cada entrada se define un rango
"""
# create neural net with 2 inputs
# input range for each input is [-0.5, 0.5]
# 3 neurons for hidden layer, 1 neuron for output
# 2 layers including hidden layer and output layer
>>> net = newff([[-0.5, 0.5], [-0.5, 0.5]], [3, 1])
# if auxIndex == 3: linea 24 define el tamaño del rango [,]
# cada [,] viene definido por el numero de componentes de cada renglon del vector
# [3,1] el primero son el numero de elmentos y el 1 el numero de salidas
"""
net = nl.net.newff([[1,20], [1,20],[1,20]],[100,1])

# Train network
# (inp, tar, epochs=15000, show=100, goal=0.00002)
# (entradas, salida deseada, epocas maximas, muestra cada 100, error objetivo)
error = net.train(input1, tar, epochs=15000, show=100, goal=0.02)


# Simulate network
out = net.sim(input1)
#print ""
print out
"""
a = []
b = []

for i in range(len(out)):
    a.append(out[i,0])
    b.append(tar[i,0])

# Plot result
import pylab as pl
pl.subplot(211)
pl.plot(error)
pl.xlabel('Epoch number')
pl.ylabel('error (default SSE)')

#y2 = net.sim(inp)
#y3 = out.reshape(len(inp))

#print ""
#print a
pl.subplot(212)
pl.plot(a, b)
pl.legend(['train target', 'net output'])
pl.show()
"""
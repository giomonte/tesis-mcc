# -*- coding: utf-8 -*-
"""
Created on Fri Oct 28 10:32:11 2016

@author: auriarte
"""

import random as random
from deap import algorithms, base, creator, tools

mejorCalificacion=100000
i=0
maxE = 16
maxN = 16
NS = 1
def convertirABinario(x):
    if x>=0.5:
        return 1
    return 0
def convertirBinarioADecimal(arreglo):
    res=0
    aux=0
    for i in range(len(arreglo)-1,-1,-1):
        if arreglo[i]>0:
            res=res+2**aux
        aux+=1
    return res    
def convertirFloatAUnRango(x):
    res=(x*5.9)+1
    print "res",res
    return int(res)
def evalOneMax(individuo):
    global mejorCalificacion
    global mejorIndividuo
    print "Individuo", individuo
    NE=map(convertirABinario,individuo[:4])
    print "NE ",NE
    NE=convertirBinarioADecimal(NE) + 1#para evitar el valor 0 entradas
    print "NE ",NE
    NN=map(convertirABinario,individuo[4:8])
    print "NN ",NN
    NN=convertirBinarioADecimal(NN) + 1#para evitar el valor 0 entradas
    print "NN ",NN
    FA = convertirFloatAUnRango(individuo[8])
    print "FA ",FA
    #Obtener del individuo NE,NN
    calificacion=10#Realizar la evaluacion y obtener el error
    if calificacion<mejorCalificacion:
        mejorIndividuo=individuo
        mejorCalificacion=calificacion
    return (calificacion,)

#maximizar la adaptabilidad
creator.create("FitnessMax", base.Fitness, weights=(-1.0,))

# crea los individuos
creator.create("Individual", list, fitness=creator.FitnessMax)
toolbox = base.Toolbox()
#el individuos tendra valores float entre cero y uno
toolbox.register("attr_float", random.random)

#estas indicando el tamaño del cromosoma con 15 genes
toolbox.register("individual", tools.initRepeat, creator.Individual,
                 toolbox.attr_float, n=9) #  tamanio del individuo

#colonia                  
toolbox.register("population", tools.initRepeat, list, toolbox.individual)

#funcion de evaluacion, le indicamos como se hará 
toolbox.register("evaluate", evalOneMax)
toolbox.register("mate", tools.cxTwoPoint)

#mutate es el comando, mutFlipBit es mutacion poe individuo, indpb = probabbilidad de mutacion
toolbox.register("mutate", tools.mutFlipBit, indpb=0.005)

#tools.selTournament es tipo de seleccion y tournsize=4 es numero de veces
toolbox.register("select", tools.selTournament, tournsize=4)

#pop= toolbox.population(n=10) es la poblacion
pop = toolbox.population(n=10)

#result = algorithms.eaSimple(pop, toolbox, cxpb=0.7, mutpb=0.65 , ngen=1, verbose=True)
# (poblacion,toolbox , probabilidad de crossover, probabilidad de mutacion, numero de generaciones)
result = algorithms.eaSimple(pop, toolbox, cxpb=0.7, mutpb=0.65 , ngen=1, verbose=True)

print "resultado ", mejorIndividuo
    
import cPickle
from keras.models import Sequential
from keras.layers import *
from sklearn.model_selection import train_test_split
from keras import backend as K
from sklearn.neighbors import KNeighborsClassifier
import csv
import numpy as np
import math
import random
import operator
import cv2

def convolucional(imagesData, labelData):

	X = np.asarray(imagesData)
	y = np.asarray(labelData)

	model = Sequential()
	model.add(Conv1D(64,3, activation="relu", input_shape=(X.shape[1],X.shape[2])))
	model.add(MaxPool1D(3))
	model.add(Conv1D(64,3, activation="relu"))
	model.add(MaxPool1D(3))
	model.add(Conv1D(64,3, activation="relu"))
	model.add(MaxPool1D(3))
	flat = Flatten()
	model.add(flat)
	model.compile("adam", "sparse_categorical_crossentropy", metrics=['accuracy'])

	flat_f = K.function(model.inputs, [flat.output])

	data = flat_f([X])

	newData = []

	for index,d in enumerate(data[0]):

		newData.append([d, y[index]])

	return newData


def knn(features):

	predictions=[]
	k = 5
	for x in range(len(features)):

		neighbors = getNeighbors(features[x], k)
		result = getResponse(neighbors)
		predictions.append(result)
	#accuracy = getAccuracy(testY, predictions)
	#print('Accuracy: ' + repr(accuracy) + '%')

	return predictions

def euclideanDistance(instance1, instance2, length):
	distance = 0
	for x in range(length):
		distance += pow((instance1[x] - instance2[x]), 2) #formula de distancia euclidiana
	return math.sqrt(distance)

def getNeighbors(testX, k):

	distances = []
	length = len(testX)-1
	#calcula la distancia euclidiana, la agrega a distances y las ordena
	for x in range(len(trainingSet)):
		dist = euclideanDistance(testX, trainingSet[x], length)
		distances.append((trainY[x], dist))

	distances.sort(key=operator.itemgetter(1))

	neighbors = []
	#extrar las n vecinas
	for x in range(k):
		neighbors.append(distances[x][0])

	return neighbors

def getResponse(neighbors):
	return modaLista(neighbors)

def getAccuracy(testY, predictions):
	correct = 0
	for x in range(len(testY)):
		if testY[x] == predictions[x]:
			correct += 1
	return (correct/float(len(testX))) * 100.0

def modaLista(lista):
    aux = 0
    cont = 0
    moda = -1
    lista.sort(cmp=None, key=None, reverse=False)
    for i in range(0,len(lista)-1):
        if (lista[i] == lista[i+1]):
            cont = cont + 1
            if cont >= aux:
                aux = cont
                moda = lista[i]
        else:
            cont=0

    return moda

def obtenerSentimiento():
	global trainingSet
	global testX
	global trainY
	global testY

	data = cPickle.load(open("KNNConvolutional.data","rb"))

	newX = []
	newY = []

	for index,d in enumerate(data):
		newX.append(d[0])
		newY.append(d[1])

	trainingSet, testX, trainY, testY = train_test_split(newX, newY, test_size=.2)

	print("trainingSetLen", len(trainingSet))

	#trainingSet = newX
	#trainY = newY

	#cap = cv2.VideoCapture(0)

	emociones = ["Aburrimiento", "Enganchado", "Emocionado", "Frustrado"]

	#capture frame-by-frame
	#ret, frame = cap.read()

	# Our operations on the frame come here

	print("leyendo imagen")
	frame = cv2.imread('4.jpg')

	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	gray = cv2.resize(gray, (150,150))

	print("convolucion")
	features = convolucional([gray], [5])

	print("clasificacion KNN")
	predictions = knn([features[0][0]])

	indiceEmocion = predictions[0]

	emocion = emociones[indiceEmocion]
	print("Indice:%s Emocion: %s" % (indiceEmocion, emocion))

    # Display the resulting frame
    #frame = cv2.putText(frame, emocion, (0,100), cv2.FONT_HERSHEY_SIMPLEX, 5, (255,255,255))
    #cv2.imshow('frame',frame)


	# When everything done, release the capture
	#cap.release()


if __name__ == '__main__':
	global trainingSet
	global testX
	global trainY
	global testY

	X = cPickle.load(open("X3.x","rb"))
	y = cPickle.load(open("y3.y","rb"))

	# data = convolucional(X, y)

	# cPickle.dump(data,open("KNNConvolutional.data","wb"))

	obtenerSentimiento()

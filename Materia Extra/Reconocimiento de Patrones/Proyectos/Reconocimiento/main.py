import cPickle
from keras.models import Sequential
from keras.layers import *
from sklearn.model_selection import train_test_split
from keras import backend as K
from sklearn.neighbors import KNeighborsClassifier
import csv 
import numpy as np
import IrisKnn as iris

X = cPickle.load(open("X3.x","rb"))
y = cPickle.load(open("y3.y","rb"))

X = np.asarray(X)
y = np.asarray(y)

model = Sequential()
model.add(Conv1D(64,3, activation="relu", input_shape=(X.shape[1],X.shape[2])))
model.add(MaxPool1D(3))
model.add(Conv1D(64,3, activation="relu"))
model.add(MaxPool1D(3))
model.add(Conv1D(64,3, activation="relu"))
model.add(MaxPool1D(3))
flat = Flatten()
model.add(flat)
model.compile("adam", "sparse_categorical_crossentropy", metrics=['accuracy'])

flat_f = K.function(model.inputs, [flat.output])

data = flat_f([X])
"""
print(data)
with open("output.csv", "wb") as f:
	writer = csv.writer(f)
	writer.writerows(data)

datos = np.asarray(data)
np.savetxt("output2.csv",   # Archivo de salida
           datos.T,        # Trasponemos los datos
           delimiter=",")  # Para que sea un CSV de verdad
"""
	   
newX = []
newY = []

for index,d in enumerate(data[0]):
    newX.append(d)
    newY.append(y[index])

trainX, testX, trainY, testY = train_test_split(newX, newY, test_size=.1)
print('TrainX', len(trainX), '			', 'testX', len(testX))
print('')
print('TrainY', len(trainY), '			', 'testY', len(testY))


"""	
clasificador = KNeighborsClassifier()
clasificador.fit(trainX, trainY)

print(clasificador.score(testX, testY))

print(clasificador.get_params())
"""
predictions=[]
k = 5
for x in range(len(testX)):
	neighbors = iris.getNeighbors(trainX, testX[x], k)
	result = iris.getResponse(neighbors)
	predictions.append(result)
	print('> predicted=' + repr(result) + ', actual=' + repr(testX[x][-1]))
accuracy = getAccuracy(testX, predictions)
print('Accuracy: ' + repr(accuracy) + '%')
import cPickle
from keras.models import Sequential
from keras.layers import *
from sklearn.model_selection import train_test_split
from keras import backend as K
from sklearn.neighbors import KNeighborsClassifier

X = cPickle.load(open("X3.x","rb"))
y = cPickle.load(open("y3.y","rb"))

X = np.asarray(X)
y = np.asarray(y)

model = Sequential()
model.add(Conv1D(64,3, activation="relu", input_shape=(X.shape[1],X.shape[2])))
model.add(MaxPool1D(3))
model.add(Conv1D(64,3, activation="relu"))
model.add(MaxPool1D(3))
model.add(Conv1D(64,3, activation="relu"))
model.add(MaxPool1D(3))
flat = Flatten()
model.add(flat)
model.compile("adam", "sparse_categorical_crossentropy", metrics=['accuracy'])

flat_f = K.function(model.inputs, [flat.output])

data = flat_f([X])

newX = []
newY = []

for index,d in enumerate(data[0]):
    newX.append(d)
    newY.append(y[index])

trainX, testX, trainY, testY = train_test_split(newX, newY, test_size=.1)

clasificador = KNeighborsClassifier()
clasificador.fit(trainX, trainY)

print(clasificador.score(testX, testY))

print(clasificador.get_params())
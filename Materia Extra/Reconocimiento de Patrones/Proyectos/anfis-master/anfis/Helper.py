import numpy as np
import copy
import math

def obtenerArchivo(archivo, numeroDatos):
    
    return funcionLeerArchivo( archivo, numeroDatos)

def funcionLeerArchivo(archivo, numeroDatos):
    valorIncorrecto = -1
    datosCrudos = np.genfromtxt(archivo ,delimiter=',')

    #Tamano de datos lectura n datos
    datosCrudos =  datosCrudos[len(datosCrudos) - numeroDatos:]

    return normalizaDatos(datosCrudos, valorIncorrecto)
    # return datosCrudos

def getDataFormatted(datos, neuronasCentrada, numeroPasosPronostico):

    inputArray = []
    targetArray = [] 

    temporal = (len(datos) - neuronasCentrada - numeroPasosPronostico + 1)

    for i in range (0, temporal):

        input = datos[ i: i + neuronasCentrada]
        target = datos[ i + neuronasCentrada + (numeroPasosPronostico - 1) ]

        if "NA" not in input and target != "NA":
            inputArray.append(input)
            targetArray.append(target)

    input = np.array(inputArray)
    inp = input.reshape(len(input), neuronasCentrada)

    target = np.array(targetArray)
    lenT = len(target)
    tar = target.reshape(lenT, 1)
    
    return inp, target

def normalizaDatos(datosCrudos, valorIncorrecto):

    datosCrudosSinValorIncorrecto = copy.copy(datosCrudos)

    datosCrudosSinValorIncorrecto = filter( lambda x: x != -1, datosCrudosSinValorIncorrecto )

    #Normalizacion de los datos
    numeroMaximo = np.amax( datosCrudosSinValorIncorrecto )
    # print "numeroMaximo: ", numeroMaximo
    numeroMinimo = np.amin( datosCrudosSinValorIncorrecto )
    # print "numeroMinimo: ", numeroMinimo
    return map(lambda x : "NA" if x == -1 else (( x - numeroMinimo ) / ( numeroMaximo - numeroMinimo)), datosCrudos)

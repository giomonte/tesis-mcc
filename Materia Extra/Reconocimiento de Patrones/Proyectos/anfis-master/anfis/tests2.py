import anfis
import membership #import membershipfunction, mfDerivs
import numpy
import Helper


directorio = 'datos/'

nb_archivo = 'wind_aristeomercado_10m_complete'
rutaArchivo = directorio + nb_archivo+'.csv' 

numeroMuestra =  1300
numeroValidacion = 240
numeroTraining = numeroMuestra - numeroValidacion

archivo = Helper.obtenerArchivo( rutaArchivo, numeroMuestra)

neuronasCentrada = 2

datosNormalizados = archivo

#Del 70 porciento atenrior se toma el 70 porciento para datos de entrenamiento
datosEntrenamiento = datosNormalizados[:numeroTraining]

#Se obtienen datos de entrenamiento con formato y target
X, Y = Helper.getDataFormatted(datosEntrenamiento, neuronasCentrada, 1)

print "X: ", X
print "Y: ", Y

mf = [[['gaussmf',{'mean':-11.,'sigma':5.}],['gaussmf',{'mean':-8.,'sigma':5.}],['gaussmf',{'mean':-14.,'sigma':20.}],['gaussmf',{'mean':-7.,'sigma':7.}]],
            [['gaussmf',{'mean':-10.,'sigma':20.}],['gaussmf',{'mean':-20.,'sigma':11.}],['gaussmf',{'mean':-9.,'sigma':30.}],['gaussmf',{'mean':-10.5,'sigma':5.}]]]

mfc = membership.membershipfunction.MemFuncs(mf)


anf = anfis.ANFIS(X, Y, mfc)

anf.trainHybridJangOffLine(epochs=200)

print round(anf.consequents[-1][0],6)
print round(anf.consequents[-2][0],6)
print round(anf.fittedValues[9][0],6)


print "anf.fittedValues: "
print anf.fittedValues

print "anf.Y:"
print anf.Y

print "anf.residuals:"
print anf.residuals

# anf.plotErrors()
anf.plotResults()